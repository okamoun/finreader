#import <Foundation/Foundation.h>
#import "XBRLObject.h"
#import "value.h"
#import "report.h"
#import "PropertiesGetter.h"
#import "FileManager.h"
#import "ErrorManager.h"

@interface section : NSObject<XBRLObject> {
	NSString *xbrlId;
	NSString *title;
	NSString *definition;
	NSString *unit;
	NSString *stype;
	NSNumber *multiplicator;
	
	NSNumber *istotal;//BOOL : is that row a total row ?
	NSNumber *isbegin;//BOOL : is that row a begin row ?
	NSNumber *HTMLValuesLoaded;//BOOL
	
	NSMutableArray *values;
	NSMutableArray *subSections;
	
	///Common dates with parents (if parent have values), brothers, children, etc., but not with cousins if parents haven't value
	///Every one has the same dates, but not necessary a value corresponding to that date !
	NSArray **dates;
	NSArray *rdates;//real dates, at thoses dates, there are values for that sections
	
	report *myReport;
	section *parentSection;
	
	NSString *dimensionsStyleSheet;
	
	//tmp objects
	NSMutableArray *footnotes;
	NSMutableArray *graphValues;
	NSMutableArray *currentGraphValues;	
	
	BOOL hasSeveralDurations;
}

@property (nonatomic, retain) NSString *xbrlId;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *definition;
@property (nonatomic, retain) NSString *unit;
@property (nonatomic, retain) NSString *stype;
@property (nonatomic, retain) NSNumber *multiplicator; 
@property (nonatomic, retain) NSNumber *istotal;
@property (nonatomic, retain) NSNumber *isbegin;
@property (nonatomic, retain) NSNumber *HTMLValuesLoaded;
@property (nonatomic, retain) NSMutableArray *values;
@property (nonatomic, retain) NSMutableArray *subSections;
@property (nonatomic, assign) NSArray **dates;
@property (nonatomic, retain) NSArray *rdates;
@property (nonatomic, retain) report *myReport;
@property (nonatomic, retain) section *parentSection;
@property (nonatomic, assign) NSString *dimensionsStyleSheet;
@property (nonatomic, assign) NSMutableArray *footnotes;
@property (nonatomic, assign) NSMutableArray *graphValues;
@property (nonatomic, assign) NSMutableArray *currentGraphValues;

//xml parsing methods
- (void) addValue:(value *)v;
- (void) addSubSection:(section *)s;

//accessors
- (int) getNbLevel;
- (BOOL) hasMultiValue;

//
- (void) setDates:(NSArray **)d;
- (NSString *) html;
- (NSString *) htmlUnit;
- (NSArray *) getAllDates:(BOOL)setDates;
- (NSString*) getValueForDate:(int)did;
- (BOOL) hasFootNoteForDate:(int)did;
//add a footnote and return the numberof that note
- (int) addFootNote:(NSString *)note;
- (NSString *) getHTMLFootNotes;
- (NSString *) getSVGGraphWithConditions:(NSArray *)c;
- (NSString *) svgLegend;
- (BOOL) isXML;
- (BOOL) isHTML;
- (BOOL) hasOtherValues;
- (NSMutableArray *) matchingSections:(NSString *)search;
- (void) setReport:(report *)r andParent:(section *)p;
- (NSError *) loadHTMLValues;
- (NSString *) multiplicatorStr;
- (void) checkMultiplicator;
- (NSString *) unit2Str:(NSString *)u;
- (BOOL) hasOnlyOneValuePerRow;

- (NSString *) getRawCsvWithSeparator:(NSString *)sep;
- (NSString*)getRawCsvWithOffset:(int)offset andNbLevel:(int)nbl andSeparator:(NSString *)sep;

- (NSArray *) sectionsCorrespondingToId:(ObjID *)objid;

- (BOOL) valuesAreNumbers;

@end
