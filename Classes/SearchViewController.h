#import <UIKit/UIKit.h>
#import "TinUITableViewController.h"
#import "ReportCell.h"
#import "report.h"
#import "reportGroup.h"
#import "CompleteReportViewController.h"
#import "PropertiesGetter.h"
#import "FileManager.h"

@interface SearchViewController : TinUITableViewController <UISearchBarDelegate> {
	NSArray *sections;
	NSMutableArray *search;
}

@property (nonatomic, retain) NSArray *sections;
@property (nonatomic, retain) NSMutableArray *search;

- (id) initWithSections:(NSArray *)subSections;

@end
