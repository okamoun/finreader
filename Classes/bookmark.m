#import "bookmark.h"

@implementation bookmark

@synthesize name;
@synthesize complement;
@synthesize directory;

-(id) initWithName:(NSString *)n complement:(NSString *)c andDirectory:(NSString *)d {
	if(self = [super init]) {
		[self setName:n];
		[self setComplement:c];
		[self setDirectory:d];
	}
	return self;
}

- (void) dealloc {
	[name release];
	[complement release];
	[directory release];
	[super dealloc];
}

- (id) initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setName:[coder decodeObject]];
		[self setComplement:[coder decodeObject]];
		[self setDirectory:[coder decodeObject]];
	}
	return self;
}

- (void) encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: name];
    [coder encodeObject: complement];
	[coder encodeObject: directory];
}

@end
