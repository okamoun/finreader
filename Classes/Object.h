#import <UIKit/UIKit.h>
#import "ObjID.h"

@protocol Object

- (ObjID *) getId;
- (BOOL) correspondsToId:(ObjID *)i;
- (BOOL) isEqual:(id)anObject;

@end
