#import "ReportGroupViewController.h"

@implementation ReportPartViewController

@synthesize parent;
@synthesize tv;
@synthesize sec;
@synthesize companyName;
@synthesize header;

- (id) initWithSection:(section *)nsec
			 andParent:(TinUIViewController *)p {	
	if(self=[super initWithNibName:@"ReportPartViewController" bundle:nil]) {
		//[self setTv:nil];
		self.tv = nil;
		[self setParent:p];
		[self setSec:nsec];
		[[self sec] getAllDates:YES];
		[self setTitle:[sec getTitle]];
		[self setCompanyName:[[sec myReport] getCompanyName]];
		//[self setHeader:nil];
		self.header = nil;
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
}

- (void)dealloc {
	[parent release], parent = nil;
	[tv release], tv = nil;
	[sec release], sec = nil;
	[companyName release], companyName = nil;
	[header release], header = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/%@", [parent getStrUrl], [sec getTitle]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	if(header == nil) {
		header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 45.0)];
		[header setAutoresizesSubviews:YES];
		[header setBackgroundColor:[PropertiesGetter getColorForKey:@"header_flow_color"]];
		UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 310.0, 45.0)];
		[headerLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
		[headerLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
		[headerLabel setMinimumScaleFactor:4.0/[UIFont labelFontSize]];//TODO : check size ; setMinimumFontSize:10
		[headerLabel setAdjustsFontSizeToFitWidth:NO];
		[headerLabel setNumberOfLines:2];
		[headerLabel setTextAlignment:NSTextAlignmentCenter];
		NSArray *a = [self.title componentsSeparatedByString:@"-"];
		if([a count] > 2) {
			NSString *s = [NSString stringWithFormat:@"%@\n%@ - %@", [a objectAtIndex:2], [a objectAtIndex:0], [a objectAtIndex:1]];
			[headerLabel setText:s];
		} else {
			[headerLabel setText:self.title];
		}
		[header addSubview:headerLabel];
		[headerLabel release];
	}
	return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[sec subSections] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"c";
	SecondLevelCell *cell = (SecondLevelCell *) [tv dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		UIViewController *c = [[UIViewController alloc] initWithNibName:@"SecondLevelCell" bundle:nil];
		cell = (SecondLevelCell *)c.view;
		[c release];
	}	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
	
	[cell setSection:[[sec subSections] objectAtIndex:indexPath.row]];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if([[sec subSections] count] == 1) {
		HTMLViewController *anotherViewController = [[HTMLViewController alloc] initWithSection:[[sec subSections] objectAtIndex:0]];
		[parent.navigationController pushViewController:anotherViewController animated:YES];
		[anotherViewController release];
	}
	else {
		ReportViewController *anotherViewController = [[ReportViewController alloc] initWithSection:sec gotoRow:[NSNumber numberWithInt:indexPath.row]];
		[parent.navigationController pushViewController:anotherViewController animated:YES];
		[anotherViewController release];
	}
}

- (void) didSelectHeader {
	if([[sec subSections] count] == 1) {
		HTMLViewController *anotherViewController = [[HTMLViewController alloc] initWithSection:[[sec subSections] objectAtIndex:0]];
		[parent.navigationController pushViewController:anotherViewController animated:YES];
		[anotherViewController release];
	}
	else if([[sec subSections] count] > 1) {
		ReportViewController *anotherViewController = [[ReportViewController alloc] initWithSection:sec gotoRow:nil];
		[parent.navigationController pushViewController:anotherViewController animated:YES];
		[anotherViewController release];
	}
}

- (float) sectionHeaderHeight {
	return [tv sectionHeaderHeight];
}

- (UITableView *) getTableView {
	return tv;
}

@end

