@protocol SelectableHeaderDelegate
- (void) didSelectHeader;
- (float) sectionHeaderHeight;
- (UITableView *) getTableView;
@end
