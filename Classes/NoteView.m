#import "NoteView.h"

@implementation NoteView

- (void)dealloc {
    [super dealloc];
}

- (void) setColorsAndFont {
	[self setTextColor:[UIColor colorWithRed:.38 green:.14 blue:.13 alpha:1]];
	[self setBackgroundColor:[UIColor colorWithRed:.98 green:.99 blue:.76 alpha:1]];
	[self setFont:[UIFont fontWithName:@"Marker Felt" size:18.0f]];
	[self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	[super drawRect:rect];
	NSString *alpha = @"ABCD";
    
    CGRect textRect = [alpha boundingRectWithSize:self.contentSize
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:self.font}
                                         context:nil];
	CGSize textSize = textRect.size;
	NSUInteger height = textSize.height;
    textRect = [self.text boundingRectWithSize:self.contentSize
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:@{NSFontAttributeName:self.font}
                                   context:nil];
	textSize = textRect.size;
	NSUInteger contentHeight = (rect.size.height > textSize.height) ? (NSUInteger)rect.size.height : textSize.height;
	NSUInteger offset = 6 + height;
	contentHeight += offset; 
	CGContextSetRGBStrokeColor(ctx, .8, .8, .8, 1); 
	for(int i=offset; i < contentHeight+200;i+=height) { 
		CGPoint lpoints[2] = { CGPointMake(0, i), CGPointMake(rect.size.width, i) }; 
		CGContextStrokeLineSegments(ctx, lpoints, 2);	
	}
	
	CGContextSetRGBStrokeColor(ctx, .38, .14, .13, 1); 
	{
		CGPoint lpoints[2] = { CGPointMake(6, -200), CGPointMake(6, contentHeight+200) }; 
		CGContextStrokeLineSegments(ctx, lpoints, 2);
	}
	{
		CGPoint lpoints[2] = { CGPointMake(8, -200), CGPointMake(8, contentHeight+200) }; 
		CGContextStrokeLineSegments(ctx, lpoints, 2);
	}
}

@end
