#import <UIKit/UIKit.h>

@interface EditableCell : UITableViewCell {
	UITextField *field;
}

@property (nonatomic, retain) IBOutlet UITextField *field;

@end
