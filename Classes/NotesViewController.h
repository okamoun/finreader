#import <UIKit/UIKit.h>
#import "TinUITableViewController.h"
#import "FileManager.h"
#import "NoteCell.h"
#import "MultiWayViewController.h"
#import "CompleteReportViewController.h"
#import "CompanyViewController.h"

@interface NotesViewController : TinUITableViewController {
	NSMutableArray *notes;
}

@property (nonatomic, retain) NSArray *notes;

@end

