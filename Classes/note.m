#import "note.h"

@implementation note

@synthesize name;
@synthesize nbId;
@synthesize parentId;

- (id) initWithName:(NSString *)n parentId:(ObjID *)pi andNbId:(NSNumber *)ni {
	if(self = [super init]) {
		[self setName:n];
		[self setNbId:ni];
		[self setParentId:pi];
	}
	return self;
}

- (void) dealloc {
	[name release], name = nil;
	[nbId release], nbId = nil;
	[parentId release], parentId = nil;
	[super dealloc];
}

-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setName:[coder decodeObject]];
		[self setNbId:[coder decodeObject]];
		[self setParentId:[coder decodeObject]];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: name];
    [coder encodeObject: nbId];
	[coder encodeObject: parentId];
}

- (NSString *) getFileName {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	if([parentId reportId] == nil) return [NSString stringWithFormat:@"%@/notes/%@;%@", [paths objectAtIndex:0], [parentId compId], [self nbId]];
	if([parentId xbrlId] == nil) return [NSString stringWithFormat:@"%@/notes/%@;%@", [paths objectAtIndex:0], [parentId reportId], [self nbId]];
	return [NSString stringWithFormat:@"%@/notes/%@;%@;%@", [paths objectAtIndex:0], [parentId reportId], [parentId xbrlId], [self nbId]];
}

- (NSString *) getContent {
	NSError *error= nil;
	NSString *ret = [[NSString stringWithContentsOfFile:[self getFileName] encoding:NSUTF8StringEncoding error:&error] retain];
	if(error != nil) {
		ERROR_WE(NOTIFY, error, @"Cannot get the note");
	}
	return [ret autorelease];
}

- (void) setContent:(NSString *)content {
	NSError *error = nil;
	[content writeToFile:[self getFileName] atomically:YES encoding:NSUTF8StringEncoding error:&error];
	if(error != nil) {
		ERROR_WE(NOTIFY, error, @"Cannot write the note on the disk");
	}
}

- (void) remove {
	NSError *error = nil;
	NSFileManager *fm = [NSFileManager defaultManager];
	[fm removeItemAtPath:[self getFileName] error:&error];
	if(error != nil){
		ERROR_WE(NOTIFY, error, @"Cannot remove the note file from the disk");
	}
}

/// ==== OBJ FUNCTIONS ===

- (ObjID *) getId {
	return [ObjID ObjIdWithObjId:parentId andNb:[self nbId]];
}

- (BOOL) correspondsToId:(ObjID *)i {
	return [[i reportId] isEqualToString:[parentId reportId]] 
	&& [[i xbrlId] isEqualToString:[parentId xbrlId]]
	&& [[i nbId] isEqual:nbId];

}

- (BOOL) isEqual:(id)object {
	if([object isKindOfClass:[note class]]) {
		return [self correspondsToId:[object getId]];
	}
	return NO;
}

- (NSString *) getSuggestedName {
	report *r = [FileManager getReportForId:parentId];
	return [NSString stringWithFormat:@"%@ - %@ - %@ - Note %@", [r getCompanyName], [r getReportType], [r getDate], [self nbId]];
}


@end
