#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "FileManager.h"
#import "ReportCell.h"
#import "report.h"
#import "ReportDownloader.h"
#import "section.h"
#import "CompleteReportViewController.h"

@interface RecentViewController : TinUIViewController {
	NSArray *keys;
	BOOL updating;
	UITableView *tv;
}

@property (nonatomic, retain) NSArray *keys;
@property (nonatomic, retain) IBOutlet UITableView *tv;

@end
