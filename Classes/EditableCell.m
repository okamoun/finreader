#import "EditableCell.h"

@implementation EditableCell

@synthesize field;

- (void)dealloc {
	[field release];
    [super dealloc];
}

@end
