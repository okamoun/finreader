#import "FileManager.h"
#import "XMLToCompaniesParser.h"
#import "AppDelegate.h"
#import "report.h"
#import "note.h"
#import "ZipArchive.h"
#import "mach/mach.h"
#import "mach/mach_time.h"

@implementation FileManager

static NSArray *alpha;
static NSFileManager *fm;
static NSString *documentsDirectory;
static NSDateFormatter *dateFormat;

static NSString *filenameCompanies;
static NSArray *companies;

static NSString *filenameReportsDesc;
static NSDictionary *reportsDesc;

static NSString *filenameLocalReports;
static NSMutableDictionary *localReports;

static NSString *filenameNotes;
static NSMutableDictionary *notes;

static NSString *filenameBookmarks;
static NSMutableDictionary *bookmarks;

static NSString *filenameInfos;

static NSMutableDictionary *recentReports;

static NSString *filenameLastupdate;
static NSDate *lastUpdate;

static BOOL updating;

NSComparisonResult dateSort(NSDate *d1, NSDate *d2, void *context) {
    return [d2 compare:d1];
}

+ (void) checkVersionCompatibility {
	
	NSMutableDictionary *versions = [[NSMutableDictionary alloc] initWithCapacity:6];
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"companies"];
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"reports_desc.plist"];	
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"local_reports"];
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"usernotes"];	
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"bookmarks"];
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"infos.html"];
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"notes"];
	[versions setObject:[NSNumber numberWithInt:2] forKey:@"reports"];
	
	NSString *versionCompatibilityFile = [[documentsDirectory stringByAppendingPathComponent:@"versionCompatibility"] retain];
	if(![fm fileExistsAtPath:versionCompatibilityFile])  {
		//so, remove everything
		NSDirectoryEnumerator *dirEnum = [fm enumeratorAtPath:documentsDirectory];
		NSString *file;
		while ((file = [documentsDirectory stringByAppendingPathComponent:[dirEnum nextObject]])) {
			if([file isEqualToString:documentsDirectory]) break; 
			NSError *err = nil;
			[fm removeItemAtPath:file error:&err];
			if(err != nil) {
				ERROR_WE(NOTIFY_AS_INTERNAL_ERROR, err, @"Cannot remove file %@ while installing a new version", file);
			}
		}
		[versions writeToFile:versionCompatibilityFile atomically:YES];
	}
	/* else {
	   //TODO : manage here each thing, one by one
       [versions writeToFile:versionCompatibilityFile atomically:YES];
	 } */
	[versions release];
	[versionCompatibilityFile release];
}

+ (void) initialize {
	alpha = [[NSArray alloc] initWithObjects: @"0-9", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", 
			 @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];

	fm = [NSFileManager defaultManager];
	documentsDirectory = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] retain];
	filenameCompanies = [[documentsDirectory stringByAppendingPathComponent:@"companies"] retain];
	filenameReportsDesc = [[documentsDirectory stringByAppendingPathComponent:@"reports_desc.plist"] retain];
	filenameLocalReports = [[documentsDirectory stringByAppendingPathComponent:@"local_reports"] retain];	
	filenameNotes = [[documentsDirectory stringByAppendingPathComponent:@"usernotes"] retain];	
	filenameBookmarks = [[documentsDirectory stringByAppendingPathComponent:@"bookmarks"] retain];	
	filenameInfos = [[documentsDirectory stringByAppendingPathComponent:@"infos.html"] retain];
	filenameLastupdate = [[documentsDirectory stringByAppendingPathComponent:@"lastUpdate"] retain];
	
	[self checkVersionCompatibility];
	
	dateFormat = [[NSDateFormatter  alloc] init];
	[dateFormat setDateFormat:(NSString *)[PropertiesGetter getParamForKey:@"date_format"]];

	
	/* --- COMPANIES --- */

	if ([fm fileExistsAtPath:filenameCompanies]) {
		companies = [[NSKeyedUnarchiver unarchiveObjectWithFile:filenameCompanies] retain];
	}
	else {
		NSString *bFilenameCompanies = [[NSBundle mainBundle] pathForResource:[PropertiesGetter getStringForKey:@"companies_filename"] ofType:@""];
		if ([fm fileExistsAtPath:bFilenameCompanies]) {
			companies = [[NSKeyedUnarchiver unarchiveObjectWithFile:bFilenameCompanies] retain];
		} else {
			FATAL(NOTIFY_AS_INTERNAL_ERROR, @"The default companies file (%@) does not exist !", bFilenameCompanies);
			[self reload:self];
			exit(1);
		}
	}
	
	/* --- REPORT DESCRIPTION --- */
	if ([fm fileExistsAtPath:filenameReportsDesc]) {
		reportsDesc = [[NSMutableDictionary alloc] initWithContentsOfFile:filenameReportsDesc];
	}
	else 
	{
		NSString *bFilenameReportsDesc = [[NSBundle mainBundle] pathForResource:@"reports_desc" ofType:@"plist"];
		if ([fm fileExistsAtPath:bFilenameReportsDesc]) {
			reportsDesc = [[NSMutableDictionary alloc] initWithContentsOfFile:bFilenameReportsDesc];
		} else {
			ERROR(NOTIFY_AS_INTERNAL_ERROR, @"The default report desc file (%@) does not exist !", bFilenameReportsDesc);
			reportsDesc = [[NSMutableDictionary alloc] initWithCapacity:0];
		}
	}

	/* --- LOCAL REPORTS --- */	
	if ([fm fileExistsAtPath:filenameLocalReports])
		localReports = [[NSKeyedUnarchiver unarchiveObjectWithFile:filenameLocalReports] retain];
	else 
		localReports = [[NSMutableDictionary alloc] initWithCapacity:0];

	/* --- NOTES --- */	
	NSLog(@"Notes filename : %@", filenameNotes);
	
	if ([fm fileExistsAtPath:filenameNotes]) {
		NSLog(@"filename exists");
		notes = [[NSKeyedUnarchiver unarchiveObjectWithFile:filenameNotes] retain];
	}
	else {
		notes = [[NSMutableDictionary alloc] initWithCapacity:0];
	}
	
	/* --- BOOKMARKS --- */	
	if ([fm fileExistsAtPath:filenameBookmarks])
		bookmarks = [[NSKeyedUnarchiver unarchiveObjectWithFile:filenameBookmarks] retain];
	else {
		bookmarks = [[NSMutableDictionary alloc] initWithCapacity:0];
	}

	/* --- INFOS --- */
	if (![fm fileExistsAtPath:filenameInfos]) {
		
		if([fm fileExistsAtPath:[[NSBundle mainBundle] pathForResource:@"infos" ofType:@"html"]]) 
		{
			NSData *content = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"infos" ofType:@"html"]];
			if(![fm createFileAtPath:filenameInfos contents:content attributes:nil]) {
				ERROR(NOTIFY_AS_INTERNAL_ERROR , @"Cannot copy file %@ to %@ !", [[NSBundle mainBundle] pathForResource:@"infos" ofType:@"html"], filenameInfos);
				filenameInfos = [[NSBundle mainBundle] pathForResource:@"infos" ofType:@"html"];
			}
		}
		else 
		{
			ERROR(NOTIFY_AS_INTERNAL_ERROR, @"The default infos file does not exist !");
		}
	}
	
	/* --- RECENT REPORTS --- */
	recentReports = nil;
	
	/* --- LAST UPDATE --- */
	if([fm fileExistsAtPath:filenameLastupdate]) {
		lastUpdate = [[NSKeyedUnarchiver unarchiveObjectWithFile:filenameLastupdate] retain];
	} else {
		lastUpdate = [[PropertiesGetter getReleaseDate] retain];
	}

	updating = NO;
}

+ (NSArray *) getAlpha {
	return alpha;
}

+ (NSArray *) getCompanies {
	@synchronized(companies) {
		return companies;
	}
}

+ (NSDictionary *) getReportsDesc {
	@synchronized (reportsDesc) {
		return reportsDesc;
	}
}

+ (NSDictionary *) getLocalReports {
	return localReports;
}

+ (NSDictionary *) getNotes {
	return notes;
}

+ (NSDictionary *) getBookmarks {
	return bookmarks;
}

+ (NSString *) getInfosFile {
	return filenameInfos;
}

+ (NSDate *) getLastUpdate {
	return lastUpdate;
}

+ (NSDictionary *) getRecentReports {
	@synchronized(recentReports) {
		if(recentReports == nil) {
			recentReports = [[NSMutableDictionary alloc] initWithCapacity:0];
			NSDate *now = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
			NSCalendar *sysCalendar = [NSCalendar currentCalendar];
			unsigned int unitFlags = NSDayCalendarUnit;
			@synchronized(companies) {
				if([companies count] >= 1) {
					for(int i = 0; i < [companies count] - 1; i++)
					{
						NSArray *currentArray = [companies objectAtIndex:i];
						for(int j = 0; j < [currentArray count] ; j++)
						{
							company *c = [currentArray objectAtIndex:j];
							for(int k = 0 ; k < [[c reportGroups] count]; k++)
							{
								reportGroup *rg = [[c reportGroups] objectAtIndex:k];
								for(int l = 0; l < [[rg reports] count]; l++)
								{
									report *r = [[rg reports] objectAtIndex:l];
									if(![r isAggregate]) {
										NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:[r date] toDate:now  options:0];
										NSNumber *nb_day = [NSNumber numberWithInt:[breakdownInfo day]];
										if([nb_day intValue] < [PropertiesGetter getIntForKey:@"nb_days_recent_reports"]) {
											NSMutableArray *a = [recentReports objectForKey:nb_day];
											if(a == nil) {
												a = [[NSMutableArray alloc] initWithObjects:r, nil];
												[recentReports setObject:a forKey:nb_day];
												[a release];
											}
											else {
												[a addObject:r];
											}
											[c loadCompany];
										}
									}
								}
							}
						}
					}
				}
				[now release];
			}
		}
		return recentReports;
	}
}

+ (company *) getCompanyForId:(ObjID *)rid {
	for(int i = 0; i < [companies count] ; i++) {//We should pick the right company group ...
		for(company *c in [companies objectAtIndex:i]) {
			if([[c.name  stringByReplacingOccurrencesOfString:@"/" withString:@"_"] isEqualToString:[rid compId]]) {
				return c;
			}
		}
	}
	return nil;
}

+ (report *) getReportForId:(ObjID *)rid {
	for(int i = 0; i < [companies count] ; i++) {//We should pick the right company group ...
		for(company *c in [companies objectAtIndex:i]) {
			if([[c.name stringByReplacingOccurrencesOfString:@"/" withString:@"_"] isEqualToString:[rid compId]]) {
				return [c getReport:rid];
			}
		}
	}
	return nil;
}

+ (void) removeOldReports {
	while ([localReports count] > [PropertiesGetter getIntForKey:@"nb_reports_in_cache"])
	{
		NSDate *key = [[[localReports allKeys] sortedArrayUsingFunction:dateSort context:nil] objectAtIndex:0];
		ObjID *reportId = [localReports objectForKey:key];
		[localReports removeObjectForKey:key];
		NSString *file = [report getFileNameForId:reportId];
		NSString *partfile = [report getPartFileNameForId:reportId];
		NSString *dir = [report getDirNameForId:reportId];
		if([fm fileExistsAtPath:file]) {
			NSError *err = nil;
			[fm removeItemAtPath:file error:&err];
			WARNING(DO_NOT_NOTIFY, @"Cannot remove file %@", file);
		}
		if([fm fileExistsAtPath:partfile]) {
			NSError *err = nil;
			[fm removeItemAtPath:partfile error:&err];
			WARNING(DO_NOT_NOTIFY, @"Cannot remove file %@", partfile);
		}
		if([fm fileExistsAtPath:dir]) {
			NSError *err = nil;
			[fm removeItemAtPath:dir error:&err];
			WARNING(DO_NOT_NOTIFY, @"Cannot remove file %@", dir);
		}
	}
	[[NSKeyedArchiver archivedDataWithRootObject:localReports] writeToFile:filenameLocalReports atomically:YES];
}

+ (void) addLocalReport:(report *)r {
	[localReports setObject:[r getId] forKey:[NSDate date]];
	[self removeOldReports];
}

+ (void) upLocalReport:(report *)r {
	ObjID *rid = [r getId];
	NSArray * keys = [localReports allKeys];
	for(int i = 0; i < [keys count] ; i++)
	{
		ObjID *cid = [localReports objectForKey:[keys objectAtIndex:i]];
		if([[rid reportId] isEqualToString:[cid reportId]])
		{
			[localReports removeObjectForKey:[keys objectAtIndex:i]];
			break;
		}
	}
	[localReports setObject:rid forKey:[NSDate date]];
	[[NSKeyedArchiver archivedDataWithRootObject:localReports] writeToFile:filenameLocalReports atomically:YES];
}

+ (void) removeLocalReport:(NSDate *)key {
	NSFileManager *fm = [NSFileManager defaultManager];
	ObjID *rid = [localReports objectForKey:key];
	NSString *file = [report getFileNameForId:rid];
	NSString *partfile = [report getPartFileNameForId:rid];
	NSString *dir = [report getDirNameForId:rid];
	if([fm fileExistsAtPath:file]) {
		NSError *err = nil;
		[fm removeItemAtPath:file error:&err];
		WARNING(DO_NOT_NOTIFY, @"Cannot remove file %@", file);
	}
	if([fm fileExistsAtPath:partfile]) {
		NSError *err = nil;
		[fm removeItemAtPath:partfile error:&err];
		WARNING(DO_NOT_NOTIFY, @"Cannot remove file %@", partfile);
	}
	if([fm fileExistsAtPath:dir]) {
		NSError *err = nil;
		[fm removeItemAtPath:dir error:&err];
		WARNING(DO_NOT_NOTIFY, @"Cannot remove file %@", dir);
	}
	[localReports removeObjectForKey:key];
	[[NSKeyedArchiver archivedDataWithRootObject:localReports] writeToFile:filenameLocalReports atomically:YES];
}

+ (BOOL) isBookmarked:(NSObject<XBRLObject> *)obj {
	ObjID *oid = [obj getId];
	bookmark *b = [bookmarks objectForKey:oid];	
	return (b != nil);
}

+ (void) addBookmark:(bookmark *)b forObject:(NSObject<XBRLObject> *)obj {
	ObjID *oid = [obj getId];
	[bookmarks setObject:b forKey:oid];
	
	[[NSKeyedArchiver archivedDataWithRootObject:bookmarks] writeToFile:filenameBookmarks atomically:YES];
}

+ (void) unbookmark:(NSObject<XBRLObject> *)obj {
	[self unbookmarkId:[obj getId]];
}

+ (void) unbookmarkId:(ObjID *)objid {
	[bookmarks removeObjectForKey:objid];
	[[NSKeyedArchiver archivedDataWithRootObject:bookmarks] writeToFile:filenameBookmarks atomically:YES];
}

+ (bookmark *) getBookmarkForObj:(ObjID *)obj {
	return [bookmarks objectForKey:obj];
}

+ (BOOL) hasNote:(NSObject<XBRLObject> *)obj {
	return ([notes objectForKey:[obj getId]] != nil);
}

+ (NSArray*) getNotesForObj:(NSObject<XBRLObject> *)obj {
	return [notes objectForKey:[obj getId]];
}

+ (note *) addNoteNamed:(NSString *)name forObj:(NSObject<XBRLObject> *)obj {
	NSMutableArray *nfo = [notes objectForKey:[obj getId]];
	int newId = 0;
	if(nfo == nil) {
		nfo = [[NSMutableArray alloc] initWithCapacity:1];
		[notes setObject:nfo forKey:[obj getId]];
		[nfo release];
	}
	else {
		for(int i = 0; i < [nfo count]; i++) { //TODO : no, we can do that better, bu getting the last and adding one
			if(newId == [[[[nfo objectAtIndex:i] getId] nbId] intValue]) {
				newId++;
				i = -1;
				continue;
			}
		}	
	}
	note *ret = [[note alloc] initWithName:name parentId:[obj getId] andNbId:[NSNumber numberWithInt:newId]];
	[nfo addObject:ret];
	[[NSKeyedArchiver archivedDataWithRootObject:notes] writeToFile:filenameNotes atomically:YES];
	return [ret autorelease];
}

+ (void) updateNote:(note *)n {
	NSMutableArray *nfo = [notes objectForKey:[n parentId]];
	NSUInteger idx = [nfo indexOfObject:n];
	[nfo replaceObjectAtIndex:idx withObject:n];
	[[NSKeyedArchiver archivedDataWithRootObject:notes] writeToFile:filenameNotes atomically:YES];
}

+ (void) removeNote:(note *)n {
	NSMutableArray *nfo = [notes objectForKey:[n parentId]];
	for(int i = 0; i < [nfo count]; i++) {
		if([[[[nfo objectAtIndex:i] getId] nbId] intValue] == [[[n getId] nbId] intValue]) {
			[nfo removeObjectAtIndex:i];
			break;
		}
	}
	[[NSKeyedArchiver archivedDataWithRootObject:notes] writeToFile:filenameNotes atomically:YES];
}

+ (void) moveNote:(note *)n to:(int)position {
	/*NSMutableArray *nfo = [notes objectForKey:[n parentId]];
	for(int i = 0; i < [nfo count]; i++) {
		if([[[[nfo objectAtIndex:i] getId] nbId] intValue] == [[[n getId] nbId] intValue]) {
			note *n = [[nfo objectAtIndex:i] retain];
			[nfo removeObjectAtIndex:i];
			if(position > i) position--;
			[nfo insertObject:n atIndex:position];
			[n release];
			break;
		}
	}*/
}

+ (note *) getNoteForId:(ObjID *)nid {
	
	ObjID *pid = [ObjID ObjIdWithCompany:[nid compId] report:[nid reportId] item:[nid xbrlId] andNb:nil];
	
	NSArray *notesOfObj = [notes objectForKey:pid];
	
	for(int i = 0; i < [notesOfObj count]; i++) {
		
		if([[[[notesOfObj objectAtIndex:i] getId] nbId] isEqual:[nid nbId]])
			return [notesOfObj objectAtIndex:i];
	}
	return nil;
}

+ (void) createNeededDirs {
	NSString *reportsDir = [NSString stringWithFormat:@"%@/reports", documentsDirectory];
	[fm createDirectoryAtPath:reportsDir withIntermediateDirectories:NO attributes:nil error:nil];
	NSString *notesDir = [NSString stringWithFormat:@"%@/notes", documentsDirectory];
	[fm createDirectoryAtPath:notesDir withIntermediateDirectories:NO attributes:nil error:nil];
}

+ (void) removePartAndUnknownFiles {
	//reports
	NSString *reportsDir = [NSString stringWithFormat:@"%@/reports", documentsDirectory];
	NSArray *reportsOnDisk = [fm contentsOfDirectoryAtPath:reportsDir error:nil];
	for(int i = 0; i < [reportsOnDisk count] ; i++)
	{
		NSString *filename = [reportsDir stringByAppendingFormat:@"/%@", [reportsOnDisk objectAtIndex:i]];
		NSString *toCheck;
		BOOL isDir;
		[fm fileExistsAtPath: filename isDirectory:&isDir];
		if(isDir) toCheck = [filename substringToIndex:([filename length]-2)];
		else toCheck = filename;
		NSString *filenamePart = [filename stringByAppendingString:@".part"];
		[fm removeItemAtPath:filenamePart error:nil];
		BOOL isUnknown = YES;		
		for (int j = 0; j < [localReports count] ; j++)
		{
			NSDate *key = [[localReports allKeys] objectAtIndex:j];
			ObjID *rid = [localReports objectForKey:key];
			if([[report getFileNameForId:rid] isEqualToString:toCheck] || [[report getPartFileNameForId:rid] isEqualToString:toCheck]) {
				isUnknown = NO;
				break;
			}
		}
		if(isUnknown) {
			[fm removeItemAtPath:filename error:nil];	
		}
	}
}

+ (void) downloadNewVersions:(int)update {
	  NSURL *urlVersions = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@&id=%@",
	 													[PropertiesGetter getBaseURL],
														[PropertiesGetter getStringForKey:@"versions_file_uri"],
														[[UIDevice currentDevice] identifierForVendor]]];
    // seesms lie UIDevice id breqk the URL
    // NSURL *urlVersions = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@&id=%@",
	//													[PropertiesGetter getBaseURL],
	//													[PropertiesGetter getStringForKey:@"versions_file_uri"],
	//													@"ID1"]];

    
     NSDictionary *serverVersions = [[NSDictionary alloc] initWithContentsOfURL:urlVersions];
	if (serverVersions == nil) {
		
		ERROR(DO_NOT_NOTIFY, @"Cannot get server versions, perhaps no connection ! URLis : %@", urlVersions);
		[urlVersions release];
		[serverVersions release];
		return;
	}
	
	NSLog(@"sv : %@", serverVersions);
	
	[urlVersions release];
	NSString *filenameVersions = [[documentsDirectory stringByAppendingPathComponent:@"versions.plist"] retain];
	NSMutableDictionary *localVersions;
	if ([fm fileExistsAtPath:filenameVersions]) {
		localVersions = [[NSMutableDictionary alloc] initWithContentsOfFile:filenameVersions];
	}
	else {
		localVersions = [[NSMutableDictionary alloc] initWithCapacity:[serverVersions count]];
		NSArray *keys = [serverVersions allKeys];
		for(int i =0; i < [keys count]; i++)
		{
			[localVersions setObject:[NSNumber numberWithInt:0] forKey:[keys objectAtIndex:i]];
		}
	}
	
	NSLog(@"lv : %@", localVersions);
		
	/* --- REPORTS DESC -- */
	if(update & upReportDesc){
		NSLog(@"UPDATE REPORT DESC");
		NSLog(@"up report desc");
		NSString *key = @"reportsdesc";
		NSNumber *sv = [serverVersions objectForKey:key];
		NSNumber *lv = [localVersions objectForKey:key];
		if([lv intValue] != [sv intValue]) {
			NSURL *urlReportsDesc = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@&id=%@", 
																   [PropertiesGetter getBaseURL], 
																   [PropertiesGetter getStringForKey:@"reports_desc_file_uri"], 
																   [[UIDevice currentDevice] identifierForVendor]]];
			
			NSDictionary *newReportsDesc = [[NSDictionary alloc] initWithContentsOfURL:urlReportsDesc];
			[urlReportsDesc release];
			
			if(newReportsDesc != nil) {
				if([newReportsDesc writeToFile:filenameReportsDesc atomically:YES])
					[localVersions setObject:sv forKey:key];
			}
			[newReportsDesc release];
		}
	}

	/* --- COMPANIES --- */
	if(update & upCompanies) {
		NSLog(@"UPDATE COMPANIES");
		NSLog(@"up companies");
		NSString *key = @"reports";
		NSNumber *sv = [serverVersions objectForKey:key];
		NSNumber *lv = [localVersions objectForKey:key];
		NSLog(@"companies versions : %d : %d", [sv intValue], [lv intValue]);
		if(true || [lv intValue] != [sv intValue] /*TODO : remove true */) {
			NSURL *urlCompanies;
			XMLToCompaniesParser *parser;
				urlCompanies = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@.zip&id=%@", 
															  [PropertiesGetter getBaseURL], 
															  [PropertiesGetter getStringForKey:@"reports_file_uri"], 
															  [[UIDevice currentDevice] identifierForVendor]]];
			
			NSLog(@"url : %@", urlCompanies);
				NSData *zipContent = [NSData dataWithContentsOfURL:urlCompanies];
				NSString *tmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat: @"%.0f.%@", [NSDate timeIntervalSinceReferenceDate] * 1000.0, @"zip"]]; 
				[zipContent writeToFile:tmpFile atomically:NO];
				ZipArchive *za = [[ZipArchive alloc] init];
				[za UnzipOpenFile:tmpFile];
				[za UnzipFileTo:NSTemporaryDirectory() overWrite:YES];
				[za UnzipCloseFile];
				NSString *contentTmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[[[PropertiesGetter getStringForKey:@"reports_file_uri"] componentsSeparatedByString:@"/"] lastObject]];
				NSLog(@"contentTmpFile : %@", contentTmpFile);
				NSData *content = [[NSData dataWithContentsOfFile:contentTmpFile] retain];
				parser = [[XMLToCompaniesParser alloc] initWithData:content];
				[content release];
				[za release];
				[fm removeItemAtPath:tmpFile error:nil];
				[fm removeItemAtPath:contentTmpFile error:nil];
			[urlCompanies release];

			if([parser parseError] == nil) {
				NSArray *newCompanies = [parser companies];
				if([NSKeyedArchiver archiveRootObject:newCompanies toFile:filenameCompanies]) {
					[localVersions setObject:sv forKey:key];
					[lastUpdate release], lastUpdate = [[NSDate alloc] initWithTimeIntervalSinceNow:0];
					[[NSKeyedArchiver archivedDataWithRootObject:lastUpdate] writeToFile:filenameLastupdate atomically:YES];
				} else
					NSLog(@"error writing...");
			} else {
				NSLog(@"error : %@", [parser parseError]);
			}
			
			[parser release];
		}
	}
		
	/* --- INFOS --- */
	if(update & upInfos) {
		NSLog(@"UPDATE INFOS");
		NSLog(@"up infos");
		NSString *key = @"infos";
		NSNumber *sv = [serverVersions objectForKey:key];
		NSNumber *lv = [localVersions objectForKey:key];
		if([lv intValue] != [sv intValue]) {
			NSURL *urlInfos = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@&id=%@", 
															 [PropertiesGetter getBaseURL], 
															 [PropertiesGetter getStringForKey:@"infos_file_uri"], 
															 [[UIDevice currentDevice] identifierForVendor]]];
			NSData *infos = [NSData dataWithContentsOfURL:urlInfos];
			[urlInfos release];
			[infos writeToFile:filenameInfos atomically:YES];
		}
	}
	
	[localVersions writeToFile:filenameVersions atomically:YES];
	[filenameVersions release];
	[serverVersions release];
	[localVersions release];

}

+ (void) downloadNewVersions {
	[self downloadNewVersions:(upCompanies|upInfos|upReportDesc)];
}

+ (void) resetRecentReports {
	@synchronized(recentReports) {
		[recentReports release], recentReports = nil;
	}
}

//TODO : call that method only if there is a new file
+ (void) updateNewVersions:(int)update {
	NSLog(@"update new version...");
	if(update & upReportDesc) {
		NSLog(@"... reports desc");
		@synchronized(reportsDesc) {
			[reportsDesc release];
			reportsDesc = [[NSMutableDictionary alloc] initWithContentsOfFile:filenameReportsDesc];
		}
	}
	if(update & upCompanies) {
		NSLog(@"... companies");
		@synchronized(companies) {
			[companies release];
			companies = [[NSKeyedUnarchiver unarchiveObjectWithFile:filenameCompanies] retain];
		}
	}
}

+ (void) upCompanies:(id)sender {
	NSLog(@"UPDATING YES");
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	id caller = [sender objectAtIndex:0];
	SEL callback = NSSelectorFromString([sender objectAtIndex:1]);
	
	[self downloadNewVersions:upCompanies];
	[self updateNewVersions:upCompanies];
	[caller performSelectorOnMainThread:callback
							 withObject:nil 
						  waitUntilDone:NO];
	NSLog(@"UPDATING NO");
	updating = NO;
	[pool release];
}

+ (void) updateCompaniesWithCallbackTarget:(id)caller andSelector:(NSString *)selectorString {
	//TODO : what append if no connection ?
	[self performSelector:@selector(upCompanies:) withObject:[NSArray arrayWithObjects:caller, selectorString, nil] afterDelay:1];
	/*[NSThread detachNewThreadSelector:@selector(upCompanies:)
							 toTarget:self 
						   withObject:[NSArray arrayWithObjects:caller, selectorString, nil]];*/
}

+ (void) ResetRecentReportsAndUpdateCompaniesWithCallbackTarget:(id)caller andSelector:(NSString *)selectorString  {
	[self performSelector:@selector(resetRecentReports) withObject:nil afterDelay:1];
	[self performSelector:@selector(upCompanies:) withObject:[NSArray arrayWithObjects:caller, selectorString, nil] afterDelay:2];
}

+(void) start {
	[self createNeededDirs];
}

+(void) reload:(id)sender {
	[NSThread setThreadPriority:0.0];
	[AppDelegate addConnection];
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[self removePartAndUnknownFiles];
	[self downloadNewVersions:upReportDesc|upInfos];
	[AppDelegate releaseConnection];
	[pool release];
}

+ (void) setUpdating {
	updating = YES;
}

+ (void) setNotUpdating {
	updating = NO;
}

+ (BOOL) isUpdating {
		return updating;
}

@end
