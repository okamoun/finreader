#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "MoreOptionsController.h"
#import "MoreOptionsController_ipad.h"

@interface WithMoreOptions : TinUIViewController<WithMoreOptionsDelegate> {
}

- (void) removePlusButton;
- (void) addPlusButton;
- (void) displayNote:(ObjID *)nid;

@end
