#import "CompanyViewController.h"

@implementation CompanyViewController

@synthesize comp;
@synthesize subViews;
@synthesize noteToBeOpened;
@synthesize reportToGo;
@synthesize firstToGo;

- (id) initWithCCompany:(company *)ncompany {
	if(self=[super initWithNibName:@"CompanyViewController" bundle:nil]) {
		[self setComp:ncompany];
		[comp loadCompany];
		[self setTitle:[comp name]];
        self.subViews = nil;
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.subViews = [[[NSMutableDictionary alloc] initWithCapacity:[[[self comp] reportGroups] count]] autorelease];
	UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
	[[self navigationItem] setBackBarButtonItem:back];
	[back release];
	[self redraw:nil];
}

- (void)viewDidUnload {
	[subViews release], subViews = nil;
	[super viewDidUnload];
}

- (void)dealloc {
	[comp release], comp = nil;
 	[subViews release], subViews = nil;
	[noteToBeOpened release], noteToBeOpened = nil;
	[reportToGo release], reportToGo = nil;
	[super dealloc];
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self redraw:self];
}

- (void) redraw:(id)sender {
	FlowView *fv = (FlowView *)self.view;
	[fv initDraw];
}

- (void) push:(NSTimer *)timer {
	[[self navigationController] pushViewController:[[timer userInfo] objectAtIndex:0] animated:YES];
	[[self view] setUserInteractionEnabled:YES];
}

- (void) performSearch:(NSTimer *)timer {
		
	for(int i = 0; i < [[comp reportGroups] count]; i++) {
		if([[[reportToGo myGroup] type_] isEqualToString:[[[comp reportGroups] objectAtIndex:i] type_]]) {
			
			FlowView *fv = (FlowView *)self.view;
			[fv setMid:i];
			[fv draw];
			
			CompleteReportViewController *anotherViewController = [[CompleteReportViewController alloc] initWithReport:reportToGo];
			[NSTimer scheduledTimerWithTimeInterval:0.4 
											 target:self 
										   selector:@selector(push:) 
										   userInfo:[NSArray arrayWithObject:anotherViewController]
											repeats:NO];
			[anotherViewController release];
			break;
		}
	}
	[reportToGo release], reportToGo = nil;
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(didRotate:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:nil];

	if(reportToGo != nil)
	{
		[[self view] setUserInteractionEnabled:NO];
		NSTimeInterval ti;
		if(firstToGo) { ti = 0.8; firstToGo = NO ; }
		else ti = 0.4;
		[NSTimer scheduledTimerWithTimeInterval:ti 
										 target:self 
									   selector:@selector(performSearch:) 
									   userInfo:nil
										repeats:NO];
	} else if(noteToBeOpened != nil) {
		[super displayNote:noteToBeOpened];
		[noteToBeOpened release], noteToBeOpened = nil;
	}
	
}

- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	return YES;
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (int)flowNumberOfViews:(FlowView *)view
{
	return [[[self comp] reportGroups] count];
}

- (void)didRotate:(NSNotification  *)notification {
	FlowView *fv = (FlowView *)self.view;
	[fv draw];
}

- (UIView *)flow:(FlowView *)view number:(int)i
{
	ReportGroupViewController *c = [subViews objectForKey:[NSNumber numberWithInt:i]];
	if(c == nil)
	{
		c = [[ReportGroupViewController alloc] initWithReportGroup:[[comp reportGroups] objectAtIndex:i] 
														 andParent:self];
		[subViews setObject:c forKey:[NSNumber numberWithInt:i]];
		[c release];
	}
	return [[subViews objectForKey:[NSNumber numberWithInt:i]] view];
}

- (UIViewController<SelectableHeaderDelegate> *)flow:(FlowView *)view controllerAtIdx:(int)i {
	ReportGroupViewController *c = [subViews objectForKey:[NSNumber numberWithInt:i]];
	if(c == nil)
	{
		c = [[ReportGroupViewController alloc] initWithReportGroup:[[comp reportGroups]objectAtIndex:i] 
														 andParent:self];
		[subViews setObject:c forKey:[NSNumber numberWithInt:i]];
		[c release];
	}
	return [subViews objectForKey:[NSNumber numberWithInt:i]];	
}

- (NSObject<XBRLObject> *) getXbrlObject {
	return (NSObject<XBRLObject> *)comp;
}

- (void) search:(id)sender {
	CompanySearchViewController *anotherViewController = [[CompanySearchViewController alloc] initWithSCompany:comp];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:YES];
	[[self navigationController] pushViewController:anotherViewController animated:NO];
	[UIView commitAnimations];
	[anotherViewController release];
}

@end
