#import <Foundation/Foundation.h>
#import "company.h"

@class CompaniesViewController;

@interface XMLToCompaniesParser : NSObject<NSXMLParserDelegate> {
	
	NSMutableArray *companies;	
	NSError *parseError;
	
	/* temp objects */
	NSMutableArray *currentCompanyGroup;
	company *currentCompany;
	
	NSString *currentType;
	NSString *currentCompanyName;
	
	report *currentReport;
	//NSString *currentNodeName;
	NSMutableString *currentNodeContent;
	int groupCounter;
}

@property (nonatomic, retain) NSError *parseError;
@property (nonatomic, retain) NSMutableArray *companies;

- (id) initWithContentsOfURL:(NSURL *)url;
- (id) initWithData:(NSData *)data;
	
@end
	
