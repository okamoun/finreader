//
//  RootViewController.m
//  test
//
//  Created by Olivier Kamoun on 3/2/10.
//  Copyright Apple Inc 2010. All rights reserved.
//

#import "DownloadedViewController.h"

@implementation DownloadedViewController

@synthesize url;
@synthesize reports;
@synthesize selectedReport;
@synthesize searchReports;
@synthesize selectedURL;
@synthesize tv;

- (void)dealloc {
	[reports release], reports = nil;
	[selectedReport release], selectedReport = nil;
	[selectedURL release], selectedURL = nil;
	[searchReports release], searchReports = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return @"Downloaded";
}

- (UITableView*) getTableView {
	return (UITableView*)[self view];
}

- (void)viewDidLoad {

	[super viewDidLoad];
	
	NSString * lurl = [NSString stringWithFormat:@"%@&id=%@", 
					   [PropertiesGetter getParamForKey:@"reports_file_uri"], 
					   [[UIDevice currentDevice] identifierForVendor]];
	
	url = [[NSURL alloc] initWithString:lurl
       					  relativeToURL:[PropertiesGetter getBaseURL]];
	//------------------------------------------------------------------------------------------------------------	
	reports = [FileManager getLocalReports];
	//------------------------------------------------------------------------------------------------------------	
	UISearchBar *searchBar = [[UISearchBar alloc] init];
	[searchBar setDelegate:self];
	[searchBar sizeToFit];
	[searchBar setTintColor:[PropertiesGetter getColorForKey:@"search_bar_color"]];
	[searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
	[[self getTableView] setTableHeaderView:searchBar];
	[searchBar release];
	//------------------------------------------------------------------------------------------------------------	
	[[self navigationItem] setRightBarButtonItem:[self editButtonItem]];
	//------------------------------------------------------------------------------------------------------------
	searchReports = [[NSMutableArray alloc] initWithCapacity:0];
	//------------------------------------------------------------------------------------------------------------
	[self setTitle:@"Downloaded Reports"];
	[[[self navigationController]navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[[self view] setAutoresizesSubviews:YES];
	[[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
}

- (void) viewWillAppear:(BOOL)animated
{
    [[self getTableView] reloadData];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];	
}

- (void) viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if(searching) return [searchReports count];
    return [reports count]; 
}

- (report *) getReportForIndex:(NSIndexPath *) indexPath {
	if(searching) return [searchReports objectAtIndex:indexPath.row];
	return [FileManager getReportForId:[reports objectForKey:[[[reports allKeys] sortedArrayUsingFunction:dateSort context:nil] objectAtIndex:indexPath.row]]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"reportcell";
    ReportCell *cell = (ReportCell *) [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
    	UIViewController *c = [[UIViewController alloc] initWithNibName:@"ReportCell"  bundle:nil];
		cell = (ReportCell *)c.view;
		[c release];
		cell.autoresizingMask = YES;
	}
	

    report *currentReport = [self getReportForIndex:indexPath];
	
	[cell setReport:currentReport];
	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
	
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	report *currentReport = [self getReportForIndex:indexPath];
	if(![currentReport isAvailable]) 
	{
		UIAlertView *unaivalableAlert = [[UIAlertView alloc] initWithTitle:[PropertiesGetter getStringForKey:@"unavailable_title"]  
																   message:[PropertiesGetter getStringForKey:@"unavailable_message"]
																  delegate:self 
														 cancelButtonTitle:@"Cancel"
														 otherButtonTitles:@"Buy", nil];
		[unaivalableAlert show];
		[unaivalableAlert release];
		return nil;
	}
	return indexPath;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1)
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=376803429&mt=8"]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	report *currentReport = [self getReportForIndex:indexPath];
		
	CompleteReportViewController *anotherViewController = [[CompleteReportViewController alloc] initWithReport:currentReport];
	
	[self.navigationController pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
	
	[pool release];
}

/* --------- editing ------------ */

- (void) tableView:(UITableView *)tableView 
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
 forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		NSDate *key = [[[reports allKeys] sortedArrayUsingFunction:dateSort context:nil] objectAtIndex:indexPath.row];
		[FileManager removeLocalReport:key];
		[tableView reloadData];
	}
}

/* ---------- search --------------*/

- (void) searchTableView:(NSString *)searchText; {
	for (int i=0; i < [[reports allKeys] count]; i++) {
		report *r = [reports objectForKey:[[reports allKeys] objectAtIndex:i]];
		
		NSString *comp = [r getCompanyName];
		NSString *type = [r getReportType];
		NSRange resultsRange = [comp rangeOfString:searchText options:NSCaseInsensitiveSearch];
			
		if(resultsRange.length > 0)
			[searchReports addObject:r];
		else {
			resultsRange = [type rangeOfString:searchText options:NSCaseInsensitiveSearch];
			if(resultsRange.length > 0)
			[searchReports addObject:r];
		}
	}
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
	presearch = YES;
	UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																			target:self 
																			action:@selector(cancelSearch:)];
	[[self navigationItem] setLeftBarButtonItem:cancel];
	[cancel release];
	[[self navigationItem] setRightBarButtonItem:nil];
	
	if([[theSearchBar text] length] > 0) {
		searching = YES;
		[searchReports removeAllObjects];
		[self searchTableView:[theSearchBar text]];
	}	
	[[self getTableView] reloadData];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
	searching = YES;
	[searchReports removeAllObjects];
	if([searchText length] > 0) {
		[self searchTableView:[theSearchBar text]];
	}
	[[self getTableView] reloadData];
}

- (void) cancelSearch:(id)sender {
	searching = NO;
	presearch = NO;	
	[[[self getTableView] tableHeaderView] resignFirstResponder];
	[[self navigationItem] setLeftBarButtonItem:nil];
	[[self navigationItem] setRightBarButtonItem:[self editButtonItem]];
	[[self getTableView] reloadData];
}

- (void) keyboardShown:(id) sender {	
	NSDictionary* info = [sender userInfo];
	
	//TODO : no
#ifdef __IPHONE_3_2
	CGRect keyboardScreenFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	UIScrollView *scrollView = (UIScrollView *) self.view;
    CGRect viewFrame = scrollView.frame;
    CGRect keyboardFrame = [scrollView.superview convertRect:keyboardScreenFrame fromView:nil];
    hiddenRect = CGRectIntersection(viewFrame, keyboardFrame);
	
    CGRect remainder, slice;
    CGRectDivide(viewFrame, &slice, &remainder, CGRectGetHeight(hiddenRect), CGRectMaxYEdge);
    scrollView.frame = remainder;
#else
    NSValue *boundsValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
    CGRect keyboardBounds = [boundsValue CGRectValue];
	keyboardHeight = keyboardBounds.size.height-49;
	CGRect frame = self.view.frame;
	frame.size.height -= keyboardHeight;
	self.view.frame = frame;	
#endif
	keyboardShown = YES;	
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    if (keyboardShown)
        return;
	
	[NSTimer scheduledTimerWithTimeInterval:0.3 
									 target:self 
								   selector:@selector(keyboardShown:) 
								   userInfo:[aNotification userInfo]
									repeats:NO];
	
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
	if (!keyboardShown)
        return;
	
	//TODO : no
#ifdef __IPHONE_3_2
	UIScrollView *scrollView = (UIScrollView *) self.view;
    CGRect viewFrame = [scrollView frame];
    scrollView.frame = CGRectUnion(viewFrame, hiddenRect);
#else	
   	CGRect frame = self.view.frame;
	frame.size.height += keyboardHeight;
	self.view.frame = frame;	
#endif
	keyboardShown = NO;
}

@end
