#import <Foundation/Foundation.h>

@interface directory : NSObject {
	NSString *title;
}

@property (nonatomic, retain) NSString *title;

- (id) initWithTitle:(NSString *)ititle;

@end
