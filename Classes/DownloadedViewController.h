#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "ReportCell.h"
#import "report.h"
#import "reportGroup.h"
#import "CompleteReportViewController.h"
#import "PropertiesGetter.h"
#import "FileManager.h"
#import "ReportDownloader.h"

@interface DownloadedViewController : TinUITableViewController <UISearchBarDelegate> {
	NSDictionary *reports;
	NSURL *url;
	NSURL *selectedURL;
	NSArray *selectedReport;
	
	UITableView *tv;
	
	NSMutableArray *searchReports;
	BOOL searching;
	BOOL presearch;
	BOOL reloading;
	
	BOOL keyboardShown;
#ifdef __IPHONE_3_2
	CGRect hiddenRect;
#else	
	float keyboardHeight;	
#endif
}

@property (nonatomic, assign) NSDictionary *reports;
@property (nonatomic, retain) NSURL *url;
@property (nonatomic, retain) NSURL *selectedURL;
@property (nonatomic, retain) NSArray *selectedReport;
@property (nonatomic, retain) NSMutableArray *searchReports;

@property (nonatomic, retain) IBOutlet UITableView *tv;

@end
