#import "PropertiesGetter.h"
#import "ErrorManager.h"

@implementation PropertiesGetter

static NSDictionary * properties;

+ (void) initialize {
	NSString *errorDesc = nil;
	NSPropertyListFormat format;
#ifndef LITEVERSION
	NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"parameters" ofType:@"plist"];
#else
	NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"parameters_lite" ofType:@"plist"];
#endif
	NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
	properties = (NSDictionary *)[[NSPropertyListSerialization propertyListFromData:plistXML
																   mutabilityOption:NSPropertyListMutableContainersAndLeaves
																			 format:&format
																	errorDescription:&errorDesc] retain];
}
	
+ (NSObject *) getParamForKey:(NSString *)key {
	return [properties objectForKey:key];
}

+ (NSString *) getStringForKey:(NSString *)key {
	return [properties objectForKey:key];
}

+ (int) getIntForKey:(NSString *)key {
	return [[properties objectForKey:key] intValue];
}

+ (float) getFloatForKey:(NSString *)key {
	return [[properties objectForKey:key] floatValue];
}

+ (BOOL) getBOOLForKey:(NSString *)key {
	return [[properties objectForKey:key] boolValue];
}

+ (UIColor *) getColorForKey:(NSString *)key {
	NSArray * t = [properties objectForKey:key];
	if ([t count] == 4)
		return [UIColor colorWithRed:[[t objectAtIndex:0] floatValue] green:[[t objectAtIndex:1] floatValue] blue:[[t objectAtIndex:2] floatValue] alpha:[[t objectAtIndex:3] floatValue] ];
	ERROR(NOTIFY_DEBUG, @"The color parameter '%@' must be composed with 4 floats", key);
	return [UIColor colorWithRed:.5 green:.5 blue:.5 alpha:1];
}

+ (NSURL *) getBaseURL {
	return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@@%@", 
								 [properties objectForKey:@"user"], 
								 [properties objectForKey:@"password"],
								 [properties objectForKey:@"base_uri"]]];
}

+ (NSDate *) getReleaseDate {
	NSString *compileDate = [NSString stringWithUTF8String:__DATE__];
	NSLog(@"%@", compileDate);
	NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
	[df setDateFormat:@"MMM d yyyy"];
	NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
	[df setLocale:usLocale];
	[usLocale release];
	return [df dateFromString:compileDate]; 
}

@end

