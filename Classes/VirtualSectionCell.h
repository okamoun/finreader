#import "VirtualXBRLObjCell.h"
#import "section.h"

@interface VirtualSectionCell : VirtualXBRLObjCell {
	IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *valueLabel;
	IBOutlet UILabel *valueLabel2;
	IBOutlet UILabel *valueLabel3;
	IBOutlet UIImageView *web;
}

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *valueLabel;
@property (nonatomic, retain) IBOutlet UILabel *valueLabel2;
@property (nonatomic, retain) IBOutlet UILabel *valueLabel3;
@property (nonatomic, retain) IBOutlet UIImageView *web;

- (void) setSection:(section *)s;

@end
