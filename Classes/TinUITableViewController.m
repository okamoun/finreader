#import "TinUITableViewController.h"

@implementation TinUITableViewController

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[ErrorManager setCurrentController:self];
	[ErrorManager setURL:[self getStrUrl]];
}


- (NSString *) getStrUrl {
	ERROR(NOTIFY_DEBUG, @"You must implement the getStrUrl method in derivate class");
	return nil;
}

- (void)dealloc {
    [super dealloc];
}

@end

