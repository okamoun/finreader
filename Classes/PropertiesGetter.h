#import <Foundation/Foundation.h>

@interface PropertiesGetter : NSObject {
}

+ (NSObject *) getParamForKey:(NSString *)key;
+ (NSString *) getStringForKey:(NSString *)key;
+ (int) getIntForKey:(NSString *)key;
+ (float) getFloatForKey:(NSString *)key;
+ (BOOL) getBOOLForKey:(NSString *)key;
+ (UIColor *) getColorForKey:(NSString *)key;
+ (NSURL *) getBaseURL;
+ (NSDate *) getReleaseDate;

@end
