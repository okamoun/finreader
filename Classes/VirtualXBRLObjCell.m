#import "VirtualXBRLObjCell.h"

@implementation VirtualXBRLObjCell

@synthesize favoriteIcon;
@synthesize noteIcon;
@synthesize fontColor;

- (void)dealloc {
	[favoriteIcon release];
	[noteIcon release];
	[fontColor release];
    [super dealloc];
}

- (void) setObj:(NSObject<XBRLObject> *)obj {
	if([FileManager isBookmarked:obj]) {
		[favoriteIcon setHidden:NO];
	} else {
		[favoriteIcon setHidden:YES];
	}
	if([FileManager hasNote:obj]) {
		[noteIcon setHidden:NO];
	} else {
		[noteIcon setHidden:YES];
	}
}

@end
