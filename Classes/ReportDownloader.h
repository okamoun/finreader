#import <Foundation/Foundation.h>
#import "XMLToSectionsParser.h"
#import "report.h"
#import "section.h"
#import "ErrorManager.h"

@interface ReportDownloader : NSObject {
}

+ (void) loadReport:(report *)selectedReport 
 withCallbackTarget:(id)caller 
		andSelector:(NSString *)selectorString 
   andErrorSelector:(NSString *)errorSelectorString
   andStateSelector:(NSString *)stateSelector;

@end
