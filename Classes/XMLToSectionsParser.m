#import "XMLToSectionsParser.h"


@implementation XMLToSectionsParser

@synthesize url;
@synthesize rdata;
@synthesize items;
@synthesize thread;
@synthesize parseError;
@synthesize sectionsFilo;

- (id) initWithURL:(NSURL *)nurl
			thread:(NSThread *)loadingThread {
	if(self = [super init]) {
		url = nurl;
		rdata = nil;
		thread = loadingThread;		
		items = [[NSMutableArray alloc] initWithCapacity:0];
		sectionsFilo = [[NSMutableArray alloc] initWithCapacity:0];
		parseError = nil;
	}
	return self;		
}

- (id) initWithData:(NSData *)data
			 thread:(NSThread *)loadingThread {
	if(self = [super init]) {
		url = nil;
		rdata = data;
		thread = loadingThread;		
		items = [[NSMutableArray alloc] initWithCapacity:0];
		sectionsFilo = [[NSMutableArray alloc] initWithCapacity:0];
		parseError = nil;
	}
	return self;		
}

- (void)dealloc
{
	[rdata release];
	[items release];
	[sectionsFilo release];
	[super dealloc];
}


- (void) parse {
	NSURLRequest* request = [NSURLRequest requestWithURL:url 
											 cachePolicy:NSURLRequestReturnCacheDataElseLoad 
										 timeoutInterval:(NSTimeInterval)[PropertiesGetter getFloatForKey:@"timeout"]];
	if(rdata == nil) {
		NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&parseError]; 
		if(parseError == nil)
		{
			NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
			[parser setDelegate:self];
			[parser parse];
			parseError = [parser parserError];
			[parser release];
		}
	} else {
		NSXMLParser *parser = [[NSXMLParser alloc] initWithData:rdata];
		[parser setDelegate:self];
		[parser parse];
		parseError = [parser parserError];
		[parser release];	
	}

}

- (void) parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
	 attributes:(NSDictionary *)attributeDict {
	
	if(thread != nil) { 
		if ([thread isCancelled]) [parser abortParsing];
	}
	
	/* --- */
	
	if([elementName isEqualToString:@"section"]) {
		section *s = [[section alloc] init];
		[sectionsFilo addObject:s];
		[s release];
		[s setXbrlId:[attributeDict objectForKey:@"id"]];
		
	}
	else if([elementName isEqualToString:@"value"]) {
		currentVal = [[value alloc] init];
	}
	else if ([sectionsFilo count] > 0) {
		currentNodeName = [elementName copy];
	    currentNodeContent = [[NSMutableString alloc] init];
	}
}

- (void) parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{  
	if(currentNodeContent != nil)
		[currentNodeContent appendString:string];
}
 
- (void) parser:(NSXMLParser *)parser
  didEndElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName {
	if([elementName isEqualToString:@"section"]) {
		section *s = [[sectionsFilo lastObject] retain];
		[sectionsFilo removeLastObject];
		if([sectionsFilo count] > 0)
		{
			section *s2 = [sectionsFilo lastObject]; 
			[s2 addSubSection:s];
		} else {
			[items addObject:s];
		}
		[s release];
	} else if ([elementName isEqualToString:@"value"]) { 
		section *s = [sectionsFilo lastObject];
		[s addValue:currentVal];
		[currentVal release], currentVal = nil;
	} else if([elementName isEqualToString:currentNodeName]) {
		if(currentVal != nil) {
			if(![elementName isEqualToString:@"context"]) {
				if([elementName isEqualToString:@"name"])
				{
					[currentVal addContextName:[currentNodeContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
				}
				else if([elementName isEqualToString:@"cval"])
				{
					[currentVal addContextValue:[currentNodeContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
				}
				else
				{
					[currentVal setValue:[currentNodeContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:elementName];
				}
			}
		} else {
			section *s = [sectionsFilo lastObject];
			[s setValue:[currentNodeContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:elementName];
		}
		[currentNodeContent release], currentNodeContent = nil;
		[currentNodeName release], currentNodeName = nil;
   }
}
 
@end
