#import "company.h"
#import "report.h"

@implementation company

@synthesize name;
@synthesize reportGroups;

-(id) init {
	if(self = [super init])	{
		reportGroups = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setName:[coder decodeObject]];
		[self setReportGroups:[coder decodeObject]];
	}
	return self;
}

- (void) dealloc {
	[name release];
	[reportGroups release];
	[super dealloc];
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: name];
    [coder encodeObject: reportGroups];
}

- (void) addReport:(report *)r ofType:(NSString *)reportType {
	NSArray *typeArray = [reportType componentsSeparatedByString:@"/"];
	NSString *simpleType = [typeArray objectAtIndex:0];
	BOOL isAmend = NO;
	BOOL isAggregate = NO;
	for(int i = 1 ; i < [typeArray count]; i++) {
		if([[typeArray objectAtIndex:i] isEqualToString:@"A"]) {
			isAmend = YES;
		} else if ([[typeArray objectAtIndex:i] isEqualToString:@"AG"]) {
			isAggregate = YES;
		} 
	}
	[r setAmend:[NSNumber numberWithBool:isAmend]];
	[r setAggregate:[NSNumber numberWithBool:isAggregate]];
	
	for(int i = 0; i < [reportGroups count]; i++)
	{
		if ([[[reportGroups objectAtIndex:i] type_] isEqualToString:simpleType])
		{
			[[reportGroups objectAtIndex:i] addReport:r];
			return;
		}
	}
	reportGroup *newrg= [[reportGroup alloc] init];
	[newrg setType_:simpleType];
	[newrg addReport:r];
	[reportGroups addObject:newrg];
	[newrg release];
}

- (void) loadCompany {
	
	for(int i = 0; i < [reportGroups count] ;i++) {
		reportGroup *rg = [reportGroups objectAtIndex:i];
		for(int j=0; j < [[rg reports] count] ; j++) {
			[[[rg reports] objectAtIndex:j] setMyCompany:self];
			[[[rg reports] objectAtIndex:j] setMyGroup:rg];
		}
	}
}

- (int) getNbReports {
	int ret = 0;
	for(reportGroup *rg in reportGroups) {
		ret += [[rg reports] count];
	}
	return ret;
}

- (report *) getReport:(ObjID *)rid {
	for(reportGroup *rg in reportGroups) {
		for(report *r in [rg reports]) {
			if([r correspondsToId:rid]) {
				[r setMyCompany:self];
				[r setMyGroup:rg];
				return r;
			}
		}
	}
	return nil;
}

/// ==== XBRL OBJ FUNCTIONS ===

- (ObjID *) getId {
	return [ObjID ObjIdWithCompany:[name stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

- (BOOL) correspondsToId:(ObjID *)i {
	return [[i compId] isEqualToString:[name stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

- (BOOL) isEqual:(id)object {
	if([object isKindOfClass:[company class]]) {
		return [[object name] isEqualToString:name];
	}
	return NO;
}

- (NSString *) getSuggestedName {
	return name;
}

- (NSString *) getSuggestedMailObject {
	return [NSString stringWithFormat:@"%@ - Reports list", name];
}

- (NSString *) getSuggestedCsvFileName {
	return [NSString stringWithFormat:@"%@_reports_list.csv", name];
}

- (NSString *) getSuggestedXlsFileName {
	return [NSString stringWithFormat:@"%@_reports_list.xls", name];
}

- (NSString *) getTitle {
	return name;
}

- (NSString *) getDescription {
	return @"";
}

- (NSArray *) getBreadCrumb {
	return [NSArray arrayWithObject:self];
}

- (NSString *) getRawCsvWithSeparator:(NSString *)sep {
	NSMutableString *content = [NSMutableString stringWithCapacity:0];
	[content setString:[NSString stringWithFormat:@"\"Report type\"%@\"Date\"\n", sep]];
	for(int i = 0 ; i < [reportGroups count] ; i++) {
		reportGroup *rg = [reportGroups objectAtIndex:i];
		for(int j = 0; j < [[rg reports] count] ; j++) {
			[content appendFormat:@"\"%@\"%@\"%@\"\n", [rg type_], sep, [[[rg reports] objectAtIndex:j] date]];
		}
	}
	return content;	
}

- (NSString *) getCsvAsString {
	return [self getRawCsvWithSeparator:@","];
}

- (NSData *) getCsv {
	return [[self getCsvAsString] dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *) getXlsAsString {
	return [self getRawCsvWithSeparator:@"\t"];
}

- (NSData *) getXls {
	return [[self getXlsAsString] dataUsingEncoding:NSUTF8StringEncoding];
}

@end
