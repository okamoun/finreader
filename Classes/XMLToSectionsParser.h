#import <Foundation/Foundation.h>
#import "section.h"
#import "value.h"

@interface XMLToSectionsParser : NSObject<NSXMLParserDelegate> {
	
	NSURL *url;
	NSData *rdata;
	NSMutableArray *items;
	
	NSMutableArray *sectionsFilo;
	value *currentVal;
	NSString *currentNodeName;
	NSMutableString *currentNodeContent;
	
	NSThread *thread;
	NSError *parseError;
	
}

- (id) initWithURL:(NSURL *)url
			thread:(NSThread *)loadingThread;
- (id) initWithData:(NSData *)data
			thread:(NSThread *)loadingThread;
- (void) parse;

@property (assign) NSURL *url;
@property (nonatomic, retain) NSData *rdata;
@property (assign) NSThread *thread;
@property (nonatomic, assign) NSError *parseError;
@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, retain) NSMutableArray *sectionsFilo;

@end
