#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SelectableHeader.h"

@protocol FlowViewDelegate;

@interface FlowView : UIScrollView
{
	IBOutlet id<FlowViewDelegate, UIScrollViewDelegate> delegate;
	BOOL isinit;
	int mid;
	int max;
	int nbViewAtInit;
	
	//touch
	int direction;//0: unknown, 1:up, 2:down
	BOOL touchFlag;//the user move the flow view or just touch it ?
	double startPos;
	double lastPos;
	
	
	//for pinch
	CGFloat initialDistance;
	BOOL expanded;
	
	//to clean
	NSLock *cleanForwardLock, *cleanBackwardLock;
}

@property (nonatomic, assign) id<FlowViewDelegate, UIScrollViewDelegate> delegate;
@property int mid;
@property BOOL isinit;
@property (nonatomic, retain) NSLock *cleanForwardLock;
@property (nonatomic, retain) NSLock *cleanBackwardLock;

- (void)initDraw;
- (void)draw;
- (void) expand;
- (void) unexpand;

@end

/*--------------------------------------------*/

@protocol FlowViewDelegate
- (int)flowNumberOfViews:(FlowView *)view;
- (UIView *)flow:(FlowView *)view number:(int)i;
- (UIViewController<SelectableHeaderDelegate> *)flow:(FlowView *)view controllerAtIdx:(int)i;
@end
