#import "NoteCell.h"

@implementation NoteCell

@synthesize label;

- (void)dealloc {
	[label release];
    [super dealloc];
}

- (void) setNote:(note *)n {
	[label setTextColor:[UIColor colorWithRed:0.38 green:0.14 blue:0.13 alpha:1]];
	[label setText:[n name]];
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
	backgroundView.backgroundColor = [UIColor colorWithRed:.98 green:.99 blue:0.76 alpha:1];
	super.backgroundView = backgroundView;
	[backgroundView release];
}

@end
