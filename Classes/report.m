#import "report.h"

@implementation report

@synthesize date;
@synthesize link;
@synthesize available;
@synthesize amend;
@synthesize aggregate;
@synthesize sections;

@synthesize myCompany;
@synthesize myGroup;

static NSDateFormatter *dateFormatIn;
static NSDateFormatter *dateFormatOut;
static NSDateFormatter *dateFormatOutShort;

+ (void) initialize {
	dateFormatIn= [[NSDateFormatter  alloc] init];
	[dateFormatIn setDateFormat:@"yyyy-MM-dd"];
	dateFormatOut = [[NSDateFormatter  alloc] init];
	[dateFormatOut setDateFormat:(NSString *)[PropertiesGetter getParamForKey:@"date_format"]];
	dateFormatOutShort = [[NSDateFormatter  alloc] init];
	[dateFormatOutShort setDateFormat:@"yyyyMM"];

}

-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setDate:[coder decodeObject]];
		[self setLink:[coder decodeObject]];
		[self setAvailable:[coder decodeObject]];
		[self setAmend:[coder decodeObject]];
		[self setAggregate:[coder decodeObject]];
		self.sections = nil;
	}
	return self;
}

- (void) dealloc {
	[date release];
	[link release];
	[available release];
	[amend release];
	[aggregate release];
	[sections release];
	[myCompany release];
	[myGroup release];
	[super dealloc];
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject:date];
	[coder encodeObject:link];
	[coder encodeObject:available];
	[coder encodeObject:amend];
	[coder encodeObject:aggregate];
}

- (void) setDateFromXML:(NSString *)dstr {
	date = [[dateFormatIn dateFromString:dstr] retain];
}

- (void) setAvailableFromXML:(NSString *)bstr {
	available = [[NSNumber alloc] initWithBool:[bstr boolValue]];
}

- (BOOL) isAvailable{
	return [available boolValue];
}

- (BOOL) isAmend{
	return [amend boolValue];
}

- (BOOL) isAggregate {
	return [aggregate boolValue];
}

- (NSString *) getCompanyName {
	return [myCompany name];
}

- (NSString *) getReportType {
	return [myGroup type_];
}

- (NSString *) getDate {
	return [dateFormatOut stringFromDate:date];
}

- (NSString *) getShortDate {
	return [dateFormatOutShort stringFromDate:date];
}

- (NSString *) getFileName {
	return [report getFileNameForId:[self getId]];
}

- (NSString *) getPartFileName {
	return [report getPartFileNameForId:[self getId]];
}

- (NSString *) getDirName {
		return [report getDirNameForId:[self getId]];
}

+ (NSString *) getFileNameForId:(ObjID *)objId {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [NSString stringWithFormat:@"%@/reports/%@", [paths objectAtIndex:0], [objId reportId]] ;
}

+ (NSString *) getPartFileNameForId:(ObjID *)objId {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [NSString stringWithFormat:@"%@/reports/%@.part", [paths objectAtIndex:0], [objId reportId]];

}

+ (NSString *) getDirNameForId:(ObjID *)objId {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [NSString stringWithFormat:@"%@/reports/%@_d", [paths objectAtIndex:0], [objId reportId]];

}

- (NSArray *) sectionsCorrespondingToId:(ObjID *)objid {
	NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
	for(section *s in sections) {
		[ret addObjectsFromArray:[s sectionsCorrespondingToId:objid]];
	}
	return ret;
}

/// ==== XBRL OBJ FUNCTIONS ===

- (ObjID *) getId {
	return [ObjID ObjIdWithObjId:[myCompany getId] andReport:[link stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

- (BOOL) correspondsToId:(ObjID *)i {
	return [[i reportId] isEqualToString:[link stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

- (NSString *) getSuggestedName {
	return [NSString stringWithFormat:@"%@ - %@ - %@", [self getCompanyName], [self getReportType], [dateFormatOutShort stringFromDate:date]];
}

- (NSString *) getSuggestedMailObject {
	return [NSString stringWithFormat:@"Report %@ - %@ - %@", [self getCompanyName], [self getReportType], [dateFormatOutShort stringFromDate:date]];
}

- (NSString *) getSuggestedCsvFileName {
	return [NSString stringWithFormat:@"report_%@_%@_%@.csv", [self getCompanyName], [self getReportType], [dateFormatOutShort stringFromDate:date]];
}

- (NSString *) getSuggestedXlsFileName {
	return [NSString stringWithFormat:@"report_%@_%@_%@.xls", [self getCompanyName], [self getReportType], [dateFormatOutShort stringFromDate:date]];
}

- (NSString *) getTitle {
	return [NSString stringWithFormat:@"%@ - %@", [self getReportType], [dateFormatOut stringFromDate:date]];
}

- (NSString *) getDescription {
	return [[FileManager getReportsDesc] objectForKey:[self getReportType]];
}

- (NSArray *) getBreadCrumb {
	return [[myCompany getBreadCrumb] arrayByAddingObject:self];
}

- (NSString *) getRawCsvWithSeparator:(NSString *)sep {
	NSMutableString *content = [NSMutableString stringWithCapacity:0];
	for(int i = 0; i < [sections count]; i++) {
		section *currentSection = [sections objectAtIndex:i];
		int nbLevel = [currentSection getNbLevel];
		[content appendFormat:@"\"%@\"", [currentSection getTitle]];
		for(int j = 0; j < nbLevel; j++) {
			[content appendString:sep];
		}
		NSArray *currentDates = [currentSection getAllDates:NO];
		for(int j = 0; j < [currentDates count];j++) {
			[content appendFormat:@"\"%@\"%@", [currentDates objectAtIndex:j], sep];
		}
		[content appendString:@"\n"];
		[content appendString:[currentSection getRawCsvWithOffset:0 andNbLevel:[currentSection getNbLevel] andSeparator:sep]];
		[content appendString:@"\n\n"];		
	}
	return content;
	
}

- (NSString *) getCsvAsString {
	return [self getRawCsvWithSeparator:@","];
}

- (NSData *) getCsv {
	return [[self getCsvAsString] dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *) getXlsAsString {
	return [self getRawCsvWithSeparator:@"\t"];
}

- (NSData *) getXls {
	return [[self getXlsAsString] dataUsingEncoding:NSUTF8StringEncoding];
}

//TODO : nein
- (void)setValue:(id)value forUndefinedKey:(NSString *)key {}

- (BOOL) isEqual:(id)object {
	if([object isKindOfClass:[report class]]) {
		return [[object link] isEqualToString:link];
	}
	return NO;
}

- (Downloaded) getDownloaded {
	NSFileManager *fm = [NSFileManager defaultManager];
	if([fm fileExistsAtPath:[self getFileName]]) {
		return totally_downloaded;
	} else if ([fm fileExistsAtPath:[self getPartFileName]]) {
		return partially_downloaded;	
	} else {
		return not_downloaded;	
	}
}

- (void) unload {
	[sections release], sections = nil;
}
	
@end
