#import "NoteController.h"


@implementation NoteController

@synthesize deleteButton;
@synthesize obj;
@synthesize nameCell;
@synthesize contentCell;
@synthesize n;
@synthesize topbar;

- (id) initWithObject:(NSObject<XBRLObject> *)o
			  andNote:(note *)newnote {
	if(self = [super initWithNibName:@"NoteController" bundle:nil]) {
		[self setObj:o];
		[self setN:newnote];
		{
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"EditableCell"  bundle:nil];
			nameCell = [(EditableCell *)c.view retain];
			[nameCell setBackgroundColor:[UIColor colorWithRed:.38 green:.14 blue:.13 alpha:1]];
			[[nameCell field] setTextColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
			[[nameCell field] setFont:[UIFont fontWithName:@"Marker Felt" size:14]];
			[c release];
			if(n != nil) {
				[[nameCell field] setText:[n name]];
			} else {
				int nbNotes = [[FileManager getNotesForObj:o] count];
				[[nameCell field] setText:[NSString stringWithFormat:@"%@ - Note %d", [o getSuggestedName], nbNotes+1]];
			}
			[[nameCell field] setPlaceholder:@"Title"];
		}
		{
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"NoteContentCell"  bundle:nil];
			contentCell = [(NoteContentCell *)c.view retain];
			[[contentCell content] setColorsAndFont];
			[c release];
			if(n != nil) [[contentCell content] setText:[n getContent]];
		}
		
	}
	return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)dealloc {
	[deleteButton release], deleteButton = nil;
	[obj release], obj = nil;
	[nameCell release], nameCell= nil;
	[contentCell release], contentCell = nil;
	[n release], n = nil;
	[topbar release], topbar = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/note", [[obj getId] description]];
}

- (void) viewDidLoad {
	[super viewDidLoad];
	[[self topbar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
																				  target:self 
																				  action:@selector(cancel:)];
	UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																				  target:self 
																				  action:nil];
	deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
																				  target:self 
																				  action:@selector(deleteNote:)];
	[deleteButton setStyle:UIBarButtonItemStyleBordered];
	if(n == nil) { 
		[deleteButton setEnabled:NO];
		[[contentCell content] becomeFirstResponder];
	}
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
																				  target:self 
																				  action:@selector(save:)];
	
	[[self topbar] setItems:[NSArray arrayWithObjects:cancelButton, flexButton, deleteButton, flexButton, saveButton, nil] animated:NO];
	[cancelButton release];
	[flexButton release];
	[deleteButton release], deleteButton = nil;
	[saveButton release];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row == 0) return 44.0f;
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	if(orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) return 70.0f;
	return 150.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row == 0) return nameCell;
	return contentCell;
}

- (void) cancel:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) save:(id)sender {
	if(n == nil) {
		[self setN:[FileManager addNoteNamed:nameCell.field.text forObj:obj]];
	} else {
		[n setName:nameCell.field.text];
		[FileManager updateNote:n];
	}
	[n setContent:[[contentCell content] text]];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1) {
		[n remove];
		[FileManager removeNote:n];
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void) deleteNote:(id)sender {
	UIAlertView *confirm = [[UIAlertView alloc] initWithTitle:@"Are you sure ?" message:@"You will not be able to get your note back !" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
	[confirm show];
	[confirm release];
}


@end
