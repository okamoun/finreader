#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "EditableCell.h"
#import "XBRLObject.h"
#import "bookmark.h"
#import "FileManager.h"
#import "NoteContentCell.h"
#import "note.h"
#import <UIKit/UIKit.h>


@interface NoteController : TinUIViewController {
	UIBarButtonItem *deleteButton;
	
	NSObject<XBRLObject> *obj;
	EditableCell *nameCell;
	NoteContentCell *contentCell;
	note *n;
	
	IBOutlet UIToolbar *topbar;
}

@property (nonatomic, retain) IBOutlet UIBarButtonItem *deleteButton;
@property (nonatomic, retain) NSObject<XBRLObject> *obj;
@property (nonatomic, retain) EditableCell *nameCell;
@property (nonatomic, retain) NoteContentCell *contentCell;
@property (nonatomic, retain) note *n;
@property (nonatomic, retain) IBOutlet UIToolbar *topbar;

- (id) initWithObject:(NSObject<XBRLObject> *)o
			  andNote:(note *)newnote;

- (void) cancel:(id)sender;
- (void) save:(id)sender;
- (void) deleteNote:(id)sender;

@end
