#import "BookmarkController.h"

@implementation BookmarkController

@synthesize obj;
@synthesize nameCell;
@synthesize topbar;
@synthesize bm;
@synthesize deleteButton;
@synthesize parentViewController;

- (id) initWithObject:(NSObject<XBRLObject> *)o
		  andBookmark:(bookmark *)b {
	if(self = [super initWithNibName:@"BookmarkController" bundle:nil]) {
		[self setObj:o];
		[self setBm:b];
		{
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"EditableCell"  bundle:nil];
			nameCell = [(EditableCell *)c.view retain];
			[c release];
			if(bm != nil) {
				[[nameCell field] setText:[bm name]];
			} else {
				[[nameCell field] setText:[obj getSuggestedName]];
			}
			[[nameCell field] setPlaceholder:@"Title"];
		}
		parentViewController = nil;
	}
	return self;
}

- (void)dealloc {
	[obj release], obj = nil;
	[nameCell release], nameCell = nil;
	[topbar release], topbar = nil;
	[bm release], bm = nil;
	[deleteButton release], deleteButton = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/bookmark", [[obj getId] description]];
}

- (void) viewDidLoad {
	[super viewDidLoad];
	[[self topbar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
																				  target:self 
																				  action:@selector(cancel:)];
	UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
																				target:self 
																				action:nil];
	deleteButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
																 target:self 
																 action:@selector(delete:)];
	[deleteButton setStyle:UIBarButtonItemStyleBordered];
	if(bm == nil) [deleteButton setEnabled:NO];
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
																				target:self 
																				action:@selector(save:)];
	
	[[self topbar] setItems:[NSArray arrayWithObjects:cancelButton, flexButton, deleteButton, flexButton, saveButton, nil] animated:NO];
	[cancelButton release];
	[flexButton release];
	[deleteButton release], deleteButton = nil;
	[saveButton release];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	if(indexPath.row == 0)
	{
		return nameCell;
    }

	static NSString *CellIdentifier = @"c";
	DirectoryCell *cell = (DirectoryCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		UIViewController *c = [[UIViewController alloc] initWithNibName:@"DirectoryCell"  bundle:nil];
		cell = (DirectoryCell *)c.view;
		[c release];
	}
	[[cell label] setText:@"Bookmarks"];
	return cell;
}

- (void) cancel:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) save:(id)sender {
	if(bm != nil) {
		[bm setName:[[nameCell field] text]];
	} else {
		bm = [[bookmark alloc] initWithName:[[nameCell field] text] complement:@"todo" andDirectory:@"Bookmarks"];
	}
	[FileManager addBookmark:bm forObject:obj];	
	if(parentViewController !=nil) {
		[parentViewController completeReload];
	}
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1) {
		[FileManager unbookmark:obj];
		if(parentViewController !=nil) {
			[parentViewController completeReload];
		}
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void) delete:(id)sender {
	UIAlertView *confirm = [[UIAlertView alloc] initWithTitle:@"Are you sure ?" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
	[confirm show];
	[confirm release];
}

/*- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	/*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 [detailViewController release];
	 */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

