#import "NotesViewController.h"

@implementation NotesViewController

@synthesize notes;


- (NSString *) getStrUrl {
	return @"Notes";
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    [[self navigationItem] setRightBarButtonItem:[self editButtonItem]];
	
	[self setTitle:@"Notes"];
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[[self view] setAutoresizesSubviews:YES];
	[[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
}

- (void)viewWillAppear:(BOOL)animated {
	[[self tableView] reloadData];
    [super viewWillAppear:animated];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	[notes release];
	notes = [[NSMutableArray alloc] initWithCapacity:0];
	NSDictionary *notesDico = [FileManager getNotes];
	for(int i = 0; i < [[notesDico allKeys] count]; i++) {
		[notes addObjectsFromArray:[notesDico objectForKey:[[notesDico allKeys] objectAtIndex:i]]];
	}
	return [notes count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"cell";
    NoteCell *cell = (NoteCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
    	UIViewController *c = [[UIViewController alloc] initWithNibName:@"NoteCell"  bundle:nil];
		cell = (NoteCell *)c.view;
		[c release];
	}
	
	note *n = [notes objectAtIndex:indexPath.row];
	//[[cell label] setText:[n name]];
	[cell setNote:n];
	
	/*UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];*/
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        note *n = [notes objectAtIndex:indexPath.row];
		[n remove];
		[FileManager removeNote:n];
		[notes removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    /*else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    } */  
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {

}



/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	note *n = [notes objectAtIndex:indexPath.row];
	ObjID *key = [n getId];
	UIViewController<CanContainsNote> *anotherViewController;
	
	if([key xbrlId] != nil) { //item in a report
		report *selectedReport = [FileManager getReportForId:key];
		anotherViewController = [[MultiWayViewController alloc] initWithReport:selectedReport andTargetId:key];
		[anotherViewController setNoteToBeOpened:key];
	}
	else if([key reportId] != nil) { // a report
		report *selectedReport = [FileManager getReportForId:key];
		anotherViewController = [[CompleteReportViewController alloc] initWithReport:selectedReport];
		[anotherViewController setNoteToBeOpened:key];	
	} else { // a company
		company *selectedCompany = [FileManager getCompanyForId:key];
		anotherViewController = [[CompanyViewController alloc] initWithCCompany:selectedCompany];
		[anotherViewController setNoteToBeOpened:key];	
	}
	[[self navigationController] pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end

