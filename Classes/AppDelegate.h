#import <UIKit/UIKit.h>
#import "TabBarController.h"
#import "FileManager.h"

@interface AppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	TabBarController *tabBarController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet TabBarController *tabBarController;

+ (void) addConnection;
+ (void) releaseConnection;
+ (UIDeviceOrientation) getDeviceOrientation;
+ (void) hideTabs;
+ (void) displayTabs;

@end

