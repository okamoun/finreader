#import "ObjID.h"

@implementation ObjID

@synthesize compId;
@synthesize reportId;
@synthesize xbrlId;
@synthesize nbId;

+ (id) ObjIdWithCompany:(NSString *)comp 
				report:(NSString *)rep 
				  item:(NSString *)item 
				 andNb:(NSNumber *)nb {
	ObjID * ret = [[ObjID alloc] initWithCompany:comp report:rep item:item andNb:nb];
	return [ret autorelease];
}

+ (id) ObjIdWithCompany:(NSString *)comp {
	ObjID * ret = [[ObjID alloc] initWithCompany:comp];
	return [ret autorelease];
}

+ (id) ObjIdWithObjId:(ObjID *)i andReport:(NSString *)rep {
	ObjID * ret = [[ObjID alloc] initWithObjId:i andReport:rep];
	return [ret autorelease];
}

+ (id) ObjIdWithObjId:(ObjID *)i andItem:(NSString *)item {
	ObjID * ret = [[ObjID alloc] initWithObjId:i andItem:item];
	return [ret autorelease];
}

+ (id) ObjIdWithObjId:(ObjID *)i andNb:(NSNumber *)nb {
	ObjID * ret = [[ObjID alloc] initWithObjId:i andNb:nb];
	return [ret autorelease];
}

- (id) initWithCompany:(NSString *)comp 
				report:(NSString *)rep 
				  item:(NSString *)item 
				 andNb:(NSNumber *)nb {
	if(self=[super init]) {
		[self setCompId:comp];
		[self setReportId:rep];
		[self setXbrlId:item];
		[self setNbId:nb];
	}
	return self;
}

- (id) initWithCompany:(NSString *)comp {
	if(self=[super init]) {
		[self setCompId:comp];
		[self setReportId:nil];
		[self setXbrlId:nil];
		[self setNbId:nil];
	}
	return self;
}

- (id) initWithObjId:(ObjID *)i andReport:(NSString *)rep {
	if(self=[super init]) {
		[self setCompId:[i compId]];
		[self setReportId:rep];
		[self setXbrlId:nil];
		[self setNbId:nil];
	}
	return self;
}

- (id) initWithObjId:(ObjID *)i andItem:(NSString *)item {
	if(self=[super init]) {
		[self setCompId:[i compId]];
		[self setReportId:[i reportId]];
		[self setXbrlId:item];
		[self setNbId:nil];
	}
	return self;
}

- (id) initWithObjId:(ObjID *)i andNb:(NSNumber *)nb {
	if(self=[super init]) {
		[self setCompId:[i compId]];
		[self setReportId:[i reportId]];
		[self setXbrlId:[i xbrlId]];
		[self setNbId:nb];
	}
	return self;
}

- (void) dealloc {
	[compId release];
	[reportId release];
	[xbrlId release];
	[nbId release];
	[super dealloc];
}

-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setCompId:[coder decodeObject]];
		[self setReportId:[coder decodeObject]];
		[self setXbrlId:[coder decodeObject]];
		[self setNbId:[coder decodeObject]];		
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: compId];
    [coder encodeObject: reportId];
    [coder encodeObject: xbrlId];
    [coder encodeObject: nbId];	
}

-(id)copyWithZone:(NSZone*)zone {
    ObjID *copy = [[[self class] allocWithZone:zone] init];
	[copy setCompId:compId];
	[copy setReportId:reportId];
	[copy setXbrlId:xbrlId];
	[copy setNbId:nbId];	
	return copy;
}

- (BOOL) isEqual:(id)anObject {
	if([anObject isKindOfClass:[self class]]) {
		if([[anObject compId] isEqualToString:self.compId]) {
			if([anObject reportId] == nil && self.reportId == nil) return YES;
			if([[anObject reportId] isEqualToString:self.reportId]) {
				if([anObject xbrlId] == nil && self.xbrlId == nil) return YES;
				if([[anObject xbrlId] isEqualToString:self.xbrlId]) {
					if([anObject nbId] == nil && self.nbId == nil) return YES;
					return [[anObject nbId] isEqual:self.nbId];
				}
			}
		}
	}
	return NO;
}

- (NSUInteger) hash {
	return [[self description] hash];
}
			 
- (NSString *) description
{
    return [NSString stringWithFormat:@"%@/%@/%@/%@", compId, reportId, xbrlId, nbId];
}


@end
