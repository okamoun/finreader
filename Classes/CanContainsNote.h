#import <UIKit/UIKit.h>
#import "note.h"

@protocol CanContainsNote

- (void) setNoteToBeOpened:(ObjID *)n;

@end
