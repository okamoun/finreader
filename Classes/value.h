#import <Foundation/Foundation.h>


@interface value : NSObject {
	NSString *date;//Not NSDate because date can be an interval
	NSString *val;
	NSString *footnote;
	NSMutableArray *contextNames;
	NSMutableArray *contextValues;
}

@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *val;
@property (nonatomic, retain) NSString *footnote;
@property (nonatomic, retain) NSMutableArray *contextNames;
@property (nonatomic, retain) NSMutableArray *contextValues;

//xml parsing methods
- (void) addContextName:(NSString *)n;
- (void) addContextValue:(NSString *)v;

- (NSString *) getDate;
- (NSString *) getLastDate;
- (BOOL) matchSingleCondition:(NSString *)condition;
- (BOOL) matchConditions:(NSArray *)conditions;
- (int) getDuration;//in monthes
- (void) multValue:(int)m;

@end
