#import <UIKit/UIKit.h>
#import "PropertiesGetter.h"
#import "FileManager.h"

@interface InfosView : UIView {
	UILabel *titleLabel;
	UILabel *versionLabel;
	UIWebView *contentView;
}

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *versionLabel;
@property (nonatomic, retain) IBOutlet UIWebView *contentView;

@end
