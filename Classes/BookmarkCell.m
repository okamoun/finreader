#import "BookmarkCell.h"

@implementation BookmarkCell

@synthesize label;

- (void)dealloc {
	[label release];
    [super dealloc];
}

- (void) setBookmark:(bookmark *)b {
	[label setText:[b name]];
}


@end
