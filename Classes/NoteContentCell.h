#import <UIKit/UIKit.h>
#import "NoteView.h"

@interface NoteContentCell : UITableViewCell {
	NoteView *content;
}

@property (nonatomic, retain) IBOutlet NoteView *content;

@end
