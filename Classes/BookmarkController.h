#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "EditableCell.h"
#import "XBRLObject.h"
#import "bookmark.h"
#import "FileManager.h"
#import "DirectoryCell.h"
#import "CanBeReload.h"

@interface BookmarkController : TinUIViewController {
	NSObject<XBRLObject> *obj;
	EditableCell *nameCell;
	
	UIToolbar *topbar;
	
	bookmark *bm;
	UIBarButtonItem *deleteButton;
	
	TinUIViewController<CanBeReload> *parentViewController;
}

@property (nonatomic, retain) NSObject<XBRLObject> *obj;
@property (nonatomic, retain) EditableCell *nameCell;
@property (nonatomic, retain) IBOutlet UIToolbar *topbar;

@property (nonatomic, retain) bookmark *bm;

@property (nonatomic, retain) 	IBOutlet UIBarButtonItem *deleteButton;

@property (nonatomic, assign) TinUIViewController<CanBeReload> *parentViewController;

- (id) initWithObject:(NSObject<XBRLObject> *)o
		  andBookmark:(bookmark *)b;

- (void) cancel:(id)sender;
- (void) save:(id)sender;
- (void) delete:(id)sender;

@end
