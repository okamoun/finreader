#import <UIKit/UIKit.h>
#import "FlowView.h"
#import "ReportPartViewController.h"
#import "SearchViewController.h"
#import "company.h"
#import "WithMoreOptionsDelegate.h"
#import "WithMoreOptions.h"
#import "ReportDownloader.h"
#import "CanContainsNote.h"
#import "LoadReportStates.h"

@interface CompleteReportViewController : WithMoreOptions <FlowViewDelegate, WithMoreOptionsDelegate, CanContainsNote>
{
	int loadingState;
	report *rep;
	NSArray *sections;
	NSMutableDictionary *subViews;
	section *sectionToGo;
	BOOL firstToGo;
	ObjID *noteToBeOpened;
	
	UILabel *loadingLabel;
	UIActivityIndicatorView *activityView;
}

@property (nonatomic, retain) report *rep;
@property (nonatomic, retain) NSArray *sections;
@property (nonatomic, retain) NSMutableDictionary *subViews;
@property (nonatomic, retain) section *sectionToGo;
@property (nonatomic) BOOL firstToGo;
@property (nonatomic, retain) ObjID *noteToBeOpened;

@property (nonatomic, retain) IBOutlet UILabel *loadingLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;

- (id) initWithReport:(report *)selectedReport;
- (void) reportDownloaded:(id)sender;
- (void) reportDownloadError:(id)sender;
- (void) search:(id)sender;

@end

