#import "MultiWayViewController.h"

@implementation MultiWayViewController

@synthesize myReport;
@synthesize correspondingObjects;
@synthesize tv;
@synthesize noteToBeOpened;

@synthesize loadingLabel;
@synthesize activityView;

- (id) initWithReport:(report *)selectedReport andTargetId:(ObjID *)objid {
	if(self = [super initWithNibName:@"MultiWayViewController" bundle:nil]) {
		myReport = [selectedReport retain];
		targetId = [objid retain];
		correspondingObjects = nil;
	}
	return self;
}

- (void)dealloc {
	[myReport release], myReport = nil;
	[correspondingObjects release], correspondingObjects = nil;
	[noteToBeOpened release], noteToBeOpened = nil;
	[tv release], tv = nil;
	[loadingLabel release], loadingLabel = nil;
	[activityView release], activityView = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/multiWay", [targetId description]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@""];
	[[self tv] setHidden:YES];
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[[self view] setAutoresizesSubviews:YES];
	[[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight ];
	[ReportDownloader loadReport:myReport withCallbackTarget:self andSelector:@"reportDownloaded:" andErrorSelector:@"reportDownloadError:" andStateSelector:@"setLoadingState:"];
}

- (void) reportDownloadError:(id)sender {
	[activityView stopAnimating];
	[loadingLabel setText:@"Error"];
	ERROR(NOTIFY, @"%@", sender);
}

- (void) reportDownloaded:(id)sender {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[self setCorrespondingObjects:[myReport sectionsCorrespondingToId:targetId]];
	
	if([correspondingObjects count] == 1) {
		section *s = [correspondingObjects objectAtIndex:0];
		CompleteReportViewController *anotherViewController = [[CompleteReportViewController alloc] initWithReport:[s myReport]];
		if(noteToBeOpened != nil) {
			[anotherViewController setNoteToBeOpened:noteToBeOpened];
			[noteToBeOpened release], noteToBeOpened = nil;
		}
		[anotherViewController setSectionToGo:s];
		[[self navigationController] pushViewController:anotherViewController animated:NO];
		[anotherViewController release];
		NSMutableArray *newViewControllers = [[NSMutableArray alloc] initWithArray:[self.navigationController viewControllers]];
		[newViewControllers removeObjectAtIndex:1];
		[[self navigationController] setViewControllers:newViewControllers];
		[newViewControllers release];
	} else {
		[self setTitle:@"Choose where you want to go"];
		[[self tv] setHidden:NO];
		[[self tv] reloadData];
	}
	[pool release];
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	if(correspondingObjects != nil) return [correspondingObjects count];
	return 0;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	if(correspondingObjects == nil) return nil;
	static NSString *CellIdentifier = @"cell";
    WayCell *cell = (WayCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
    	UIViewController *c = [[UIViewController alloc] initWithNibName:@"WayCell"  bundle:nil];
		cell = (WayCell *)c.view;
		[c release];
	}
	
	[cell setObj:[correspondingObjects objectAtIndex:indexPath.row]];
	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    } */  
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {

}



/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	section *s = [correspondingObjects objectAtIndex:indexPath.row];
	CompleteReportViewController *anotherViewController = [[CompleteReportViewController alloc] initWithReport:[s myReport]];
	[anotherViewController setSectionToGo:s];
	if(noteToBeOpened != nil) {
		[anotherViewController setNoteToBeOpened:noteToBeOpened];
		[noteToBeOpened release], noteToBeOpened = nil;
	}
	[self.navigationController pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void) setLoadingState:(NSNumber *) newState {
	//TODO
}

@end

