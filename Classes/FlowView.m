#import "FlowView.h"

#define MAXPAGESOVER		4		
#define MAXPAGESUNDER		2
#define DECALAGE_BOTTOM		20.0
#define DECALAGE_TOP_MIN	0.0
#define DECALAGE_TOP_MAX	42.0
#define MAXSPEED			3.0
#define TOUCHLIMIT			100.0
#define SELECTLIMIT			10.0
#define SPEEDFACTOR         0.25
#define WIDTHDECAL			3.0
#define FIRSTY				-13.0

#define MIN_MAX(min, i, max) MIN(MAX(min,i), max)

@implementation FlowView

@synthesize delegate;
@synthesize mid;
@synthesize isinit;
@synthesize cleanForwardLock;
@synthesize cleanBackwardLock;

- (id)initWithFrame:(CGRect)frame 
{
    if (self = [super initWithFrame:frame]) {
		mid = 0;
		isinit = NO;
		expanded = NO;
		self.scrollEnabled = NO;
		self.bounces = YES;
		[self setContentSize:CGSizeMake(self.frame.size.width, self.frame.size.height)];
		[self setContentOffset:CGPointMake(0,0) animated:NO];
		initialDistance = 0;
		cleanForwardLock = [[NSLock alloc] init];
		cleanBackwardLock = [[NSLock alloc] init];
    }
    return self;
}

- (void)dealloc 
{
	[cleanForwardLock release];
	[cleanBackwardLock release];
	[super dealloc];
}

/************************************************************************/
/*																		*/
/*	Delegate Calls														*/
/*																		*/
/************************************************************************/

- (int)numberOfViews
{
	if (delegate) {
		return [delegate flowNumberOfViews:self];
	} else {
		return 0;
	}
}

- (UIViewController<SelectableHeaderDelegate> *)viewControllerAtIdx:(int)i
{
	if (delegate) {
		return [delegate flow:self controllerAtIdx:i];
	} else {
		return nil;
	}
}

- (UIView *)viewAtIdx:(int)i
{
	if (delegate) {
		return [delegate flow:self number:i];
	} else {
		return nil;
	}
}

/************************************************************************/
/*																		*/
/*	Tile Management														*/
/*																		*/
/************************************************************************/

- (UIView *)getViewAtIndex:(int)index
{
	UIView *v = [self viewAtIdx:index];		
	return v;
}

/************************************************************************/
/*																		*/
/*	Drawing																*/
/*																		*/
/************************************************************************/

- (void) reframe:(UIView *)v withX:(float)x andY:(float)y {
	[v setFrame:CGRectMake(x, y, self.frame.size.width-2*x, self.frame.size.height-FIRSTY)];
}

- (void)initDraw {
	if(isinit) return;
	isinit = YES;
	max = [self numberOfViews] - 1;
	[self setBackgroundColor:[UIColor blackColor]];
	[self setScrollEnabled:NO];
	[self setBounces:YES];
	[self setContentSize:CGSizeMake(self.frame.size.width, self.frame.size.height)];
	[self setContentOffset:CGPointMake(0,0) animated:NO];
	
	nbViewAtInit = [[self subviews] count];
	int i, I = 0;
	int NBI = MIN(max, mid+MAXPAGESOVER)-mid;
	float x=WIDTHDECAL*NBI;
	float y = FIRSTY;
	
	for(i = MIN(max, mid+MAXPAGESOVER); i > mid ; i--) 
	{
		x -= WIDTHDECAL;
		y += (DECALAGE_TOP_MAX*I+ (NBI-I)*DECALAGE_TOP_MIN)/NBI;
		UIView *v = [self getViewAtIndex:i];
		[self reframe:v withX:x andY:y];
		[self addSubview:v];
		I++;
	}
	
	{
		if(I > 0) y += DECALAGE_TOP_MAX;
		UIView *v = [self getViewAtIndex:mid];
		[self reframe:v withX:0.0f andY:y];
		[self addSubview:v];
	}
}

- (void)draw
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	
	int i, I = 0;
	int NBI = MIN(max, mid+MAXPAGESOVER)-mid;
	float x=WIDTHDECAL*(MIN(max, mid+MAXPAGESOVER)-mid);
	float y=FIRSTY;
		
	//hidden over
	for(i = mid+MAXPAGESOVER+1 ;i <= max ; i++) {
		UIView *v = [self getViewAtIndex:i];
		if([v isDescendantOfView:self])	[self reframe:v withX:x andY:y];
		else							break;
	}
		
	//over mid	
	for(i = MIN(max, mid+MAXPAGESOVER) ;i > mid; i--) {
		x -= WIDTHDECAL;
		y += (DECALAGE_TOP_MAX*I + (NBI-I)*DECALAGE_TOP_MIN)/NBI;
		UIView *v = [self getViewAtIndex:i];
		[self reframe:v withX:x andY:y];
		I++;
	}
	//mid
	{
		if(I > 0) y += DECALAGE_TOP_MAX;
		UIView *v = [self getViewAtIndex:mid];
		[self reframe:v withX:0.0f andY:y];
	}
	//under mid
	int nbUnder = MIN(mid, MAXPAGESUNDER);
	y = self.bounds.size.height - (float)(nbUnder*DECALAGE_BOTTOM);
	for(i = mid-1; i >= mid-nbUnder ; i--)
	{
		UIView *v = [self getViewAtIndex:i];
		[self reframe:v withX:0.0f andY:y];
		y += DECALAGE_BOTTOM;
	}
	
	//hidden under
	for(; i >= 0 ; i--) {
		UIView *v = [self getViewAtIndex:i];
		if([v isDescendantOfView:self])	[self reframe:v withX:0.0f andY:y];
		else							break;
	}
	[UIView commitAnimations];
}

- (void)redrawMid
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	int NBI = MIN(max, mid+MAXPAGESOVER)-mid;
	float y=FIRSTY+((NBI==0)?0.0f:(NBI+1)*((DECALAGE_TOP_MAX+DECALAGE_TOP_MIN)/2.0f));
	UIView *v = [self getViewAtIndex:mid];
	[self reframe:v withX:0.0f andY:y];
	[UIView commitAnimations];
}

- (void)redrawPageUnderMid
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
	int nbUnder = MIN(mid, MAXPAGESUNDER);
	float y = self.bounds.size.height - (float)(nbUnder*DECALAGE_BOTTOM);
	UIView *v = [self getViewAtIndex:mid-1];
	[self reframe:v withX:0.0f andY:y];
	[UIView commitAnimations];
}
	
- (void) forward:(uint)n {
	mid = MIN(max, mid+(int)n);
	//insert new subviews
	{
		[cleanBackwardLock tryLock];
		float y=FIRSTY;
		float x=WIDTHDECAL*(MIN(max, mid+MAXPAGESOVER)-mid);
		for(int i = mid+MAXPAGESOVER+1-n ; i <= MIN(max, mid+MAXPAGESOVER) ; i++) {
			UIView *v = [self getViewAtIndex:i];
			[self reframe:v withX:x andY:y];
			if(![v isDescendantOfView:self]) [self insertSubview:v atIndex:nbViewAtInit];	
		}
	}
	//move all views
	[self draw];
	//delete old views...
	{
		[cleanForwardLock unlock];
		[NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(cleanForward) userInfo:nil repeats:NO];
	}
}

- (void) cleanForward {//remove views after a foreward
	for (int i=0; i < mid-MAXPAGESUNDER-1; i++) {
		if([cleanForwardLock tryLock]) {
			UIView *v = [self getViewAtIndex:i];
			[v removeFromSuperview];
			[cleanForwardLock unlock];
		} else {
			return;
		}
	}
}

- (void) backward:(uint)n {
	mid = MAX((mid-(int)n), 0);	
	//insert new subviews
	{
		[cleanForwardLock tryLock];
		float y=self.bounds.size.height;
		for(int i = mid+n-MAXPAGESUNDER-1 ; i >= MAX(0, mid+(int)n-MAXPAGESUNDER) ; i--) {
			UIView *v = [self getViewAtIndex:i];
			[self reframe:v withX:0.0f andY:y];
			if(![v isDescendantOfView:self]) [self addSubview:v];
		}		
	}
	//move all views
	[self draw];
	//delete old views
	{
		[cleanBackwardLock unlock];
		[NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(cleanBackward) userInfo:nil repeats:NO];
	}
}

- (void) cleanBackward { //remove views after a backward
	for (int i=max; i > mid+MAXPAGESOVER ; i--) {
		if([cleanBackwardLock tryLock]) {
			UIView *v = [self getViewAtIndex:i];
			[v removeFromSuperview];
			[cleanBackwardLock unlock];
		} else {
			return;
		}

	}
}

/************************************************************************/
/*																		*/
/*	Touch																*/
/*																		*/
/************************************************************************/

- (CGFloat)distanceBetweenTwoPoints:(CGPoint)fromPoint toPoint:(CGPoint)toPoint {
	float x = toPoint.x - fromPoint.x;
	float y = toPoint.y - fromPoint.y;
	return sqrt(x * x + y * y);
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	NSSet *allTouches = [event allTouches];
	if([allTouches count] == 1)
	{
		UITouch *t = [touches anyObject];
		CGPoint where = [t locationInView:self];
		lastPos = startPos = where.y;
		direction = 0;
		touchFlag = YES;
	}
    else
	{
		[self setScrollEnabled:NO];
		UITouch *touch1 = [[allTouches allObjects] objectAtIndex:0];
		UITouch *touch2 = [[allTouches allObjects] objectAtIndex:1];
		initialDistance = [self distanceBetweenTwoPoints:[touch1 locationInView:self]
												 toPoint:[touch2 locationInView:self]];	
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesMoved:touches withEvent:event];
	NSSet *allTouches = [event allTouches];
	if([allTouches count] == 1)
	{
		UITouch *t = [touches anyObject];
		CGPoint where = [t locationInView:self];
		int dy = where.y - lastPos;
		lastPos = where.y;
		if (touchFlag && fabs(startPos - lastPos) > TOUCHLIMIT) {
			touchFlag = NO;
		}
		if(!expanded)
		{
			if(direction == 0)
			{
				if(lastPos < startPos)      direction = 1;
				else if(lastPos > startPos) direction = 2;
			}
			UIView *v = [self getViewAtIndex:(direction==1)?MAX(mid-1, 0):mid];
			[self reframe:v withX:0.0f andY:(v.frame.origin.y + dy)];
		}

	} else if([allTouches count] == 2) {
		UITouch *touch1 = [[allTouches allObjects] objectAtIndex:0];
		UITouch *touch2 = [[allTouches allObjects] objectAtIndex:1];
		CGFloat finalDistance = [self distanceBetweenTwoPoints:[touch1 locationInView:self]
													   toPoint:[touch2 locationInView:self]];
		if(initialDistance > finalDistance) {
			[self expand];
		}
		else {
			[self unexpand];
		}
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesEnded:touches withEvent:event];
	@synchronized(self)
	{
		NSSet *allTouches = [event allTouches];
		if([allTouches count] == 1) 
		{
			UITouch *t = [touches anyObject];
			CGPoint where = [t locationInView:self];
			CGPoint prevWhere = [t previousLocationInView:self];
			double pos = where.y;
			lastPos = prevWhere.y;
			double speed = (pos - lastPos)*SPEEDFACTOR;
			
			if (touchFlag == YES) //pinch or select
			{
				if(fabs(pos-startPos) < SELECTLIMIT) 
				{
					if(expanded) {
						int selected = max-floor(pos/DECALAGE_TOP_MAX);
						if(selected >= 0)
						{
							UIViewController<SelectableHeaderDelegate> *vc = [self viewControllerAtIdx:selected];
							[vc didSelectHeader];
						}
					} else {
						UIView *v = [self getViewAtIndex:mid];
						if(v.frame.origin.y > where.y) {
							[self forward:1];
							return;
						}

						UIViewController<SelectableHeaderDelegate> *vc = [self viewControllerAtIdx:mid];
						if(v.frame.origin.y+[vc sectionHeaderHeight] > where.y) {
							[vc didSelectHeader];
							return;	
						}
						UITableView *tv = [vc getTableView];
						[tv setUserInteractionEnabled:YES];
						[tv touchesBegan:touches withEvent:event];
						[tv touchesEnded:touches withEvent:event];
						[tv setUserInteractionEnabled:NO];
					}
					return;
				} else if(!expanded) {
					if(pos > startPos)	    [self forward:1];
					else if(pos < startPos) [self backward:1];			
				}
			} else if(!expanded) {		
				if(direction == 1) //up
				{
					if(mid==0) {
						[self redrawMid];
						return;
					}
					if((startPos - pos) < TOUCHLIMIT) { //not enough to change anything
						[self redrawPageUnderMid];
						return; 
					}
					uint backward=floor(MIN_MAX(1, -speed, MAXSPEED));
					[self backward:backward];
					return;
				}
				else //down
				{ 
					if(mid==max) {
						[self redrawMid];
						return;
					}
					if((pos - startPos) < TOUCHLIMIT) { 
						[self redrawMid];
						return; 
					}
					uint forward=floor(MIN_MAX(1, speed, MAXSPEED)); 
					[self forward:forward];
					return;					
				}
			}
		}
		else {
			[self setScrollEnabled:expanded];
		}
	}
}

/*
 TODO: expand doit d'abord faire une animation, d'abord en creant toutes les vues qui vont etre visible
 puis une tableView qui va les remplacer une fois l'animation terminee, et enfin, supprimer toutes les vues devenues inutiles
 
 => il faudra donc penser a creer les methodes correspondant au protocole UITableViewDelegate, quitte a en creer d'autres dans le protocole flowViewDelegate pour obtenirles titres des vues
 => pour l'effet d'ombre, on pourra penser a creer une nouvelle Cell qui reprend le code de shadowedView que l'on pourra factoriser ailleurs
 
 ATTENTION : bien penser a gerer les verrous pour ne pas avoir de probleme en creant une vue que l'on est en train de detruire

 Cette nouvelle methodes devrait nous dispenser des manipulations de scrollEnabled, contentSize, etc.
 Sans doute aussi que cela va simplifier les methodes concerant les touches de l'ecran
 
 */
- (void) expand {
	NSLog(@"expand");
	/*if(!expanded) {
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.4];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
		
		int i = max;
		
		float posy = FIRSTY;
		for (; i >= 0; i--)
		{
			//TODO [self drawMid:i atPos:posy];
			posy += DECALAGE_TOP_MAX;
		}
		self.contentSize = CGSizeMake(self.frame.size.width, MAX(posy-FIRSTY, self.frame.size.height));
		float posmid = MIN(
						   MAX(
							   0, 
							   (FIRSTY + (max-mid)*DECALAGE_TOP_MAX) - (self.frame.size.height/2.5)
							   ), 
						   self.contentSize.height-self.frame.size.height
						   );
		[self setContentOffset:CGPointMake(0,posmid) animated:NO];
		
		[UIView commitAnimations];
		expanded = YES;
		[self setScrollEnabled:YES];
	}*/
}

/*
 TODO: unexand fait l'inverse de expand
 
 */
- (void) unexpand {
	NSLog(@"unexpand");
	/*if(expanded) {
		expanded = NO;
		mid = MAX(0, MIN(max, max - ((self.frame.size.height/2.5) + self.contentOffset.y - FIRSTY)/DECALAGE_TOP_MAX));
		[self draw];
		[self setScrollEnabled:NO];
	}*/
}


@end
	