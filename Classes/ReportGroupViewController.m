#import "ReportGroupViewController.h"

@implementation ReportGroupViewController

@synthesize parent;
@synthesize group;
@synthesize tv;
@synthesize header;

- (id) initWithReportGroup:(reportGroup *)ngroup
				 andParent:(TinUIViewController *)p {
	
	if(self=[super initWithNibName:@"ReportGroupViewController" bundle:nil]) {
		[self setParent:p];
		[self setGroup:ngroup];	
		[self setTitle:[NSString stringWithFormat:@"%@ - %@", 
						[[self group] type_],
						[[FileManager getReportsDesc] objectForKey:[[self group] type_]]]];
		//[self setTv:nil];
		//[self setHeader:nil];
		//self.tv = nil;
		self.header = nil;
	}
	return self;
}

- (void)dealloc {
	[tv release], tv = nil;
	[parent release], parent = nil;
	[group release], group = nil;
	[header release], header = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/%@", [parent getStrUrl], [group type_]];
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[tv reloadData];
}

- (void)viewDidLoad {
	[super viewDidLoad];
}

- (void)viewDidUnload {
	[super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	if(header == nil)
	{
		header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 35.0)];
		[header setAutoresizesSubviews:YES];
		[header setBackgroundColor:[PropertiesGetter getColorForKey:@"header_flow_color"]];
		UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 310.0, 35.0)];
		[headerLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
		[headerLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
		[headerLabel setMinimumScaleFactor:4.0/[UIFont labelFontSize]];//TODO check size (setMinimumFontSize:10)
		[headerLabel setAdjustsFontSizeToFitWidth:YES];
		[headerLabel setNumberOfLines:2];
		[headerLabel setTextAlignment:NSTextAlignmentCenter];
		[headerLabel setText:[self title]];
		[header addSubview:headerLabel];
		[headerLabel release];
	}
	return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[group reports] count]; 
}

- (report *) getReportForIndex:(NSIndexPath *) indexPath {
	return [[group reports] objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"simplereportcell";
    SimpleReportCell *cell = (SimpleReportCell *) [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
    	UIViewController *c = [[UIViewController alloc] initWithNibName:@"SimpleReportCell"  bundle:nil];
		cell = (SimpleReportCell *)c.view;
		[c release];
	}
    report *currentReport = [self getReportForIndex:indexPath];
	[cell setReport:currentReport];
	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	report *currentReport = [[group reports] objectAtIndex:indexPath.row];
	if(![currentReport isAvailable]) 
	{
		UIAlertView *unaivalableAlert = [[UIAlertView alloc] initWithTitle:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_title"]  
																   message:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_message"]
																  delegate:self 
														 cancelButtonTitle:@"Cancel"
														 otherButtonTitles:@"Buy", nil];
		 [unaivalableAlert show];
		 [unaivalableAlert release];
		 return nil;
	 }
	 return indexPath;
 }
  
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	 if(buttonIndex == 1)
		 [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=376803429&mt=8"]];
}
 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	report *currentReport = [[group reports] objectAtIndex:indexPath.row];
	CompleteReportViewController *anotherViewController = [[CompleteReportViewController alloc] initWithReport:currentReport];
	[[parent navigationController] pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
	[pool release];
}

- (void) didSelectHeader{}

- (float) sectionHeaderHeight {
	return [tv sectionHeaderHeight];
}

- (UITableView *) getTableView {
	return tv;
}

@end

