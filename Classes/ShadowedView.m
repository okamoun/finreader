#import "ShadowedView.h"

#define SHADOW_HEIGHT 13.0

@implementation ShadowedView

@synthesize topShadow;

- (void)dealloc {
	[topShadow release];
    [super dealloc];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	if (!topShadow)
	{
		topShadow = [[CAGradientLayer alloc] init];
		[topShadow setFrame:CGRectMake(0, 0, self.frame.size.width, SHADOW_HEIGHT)];
		CGColorRef darkColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5].CGColor;
		CGColorRef lightColor = [self.backgroundColor colorWithAlphaComponent:0.0].CGColor;
		[topShadow setColors:[NSArray arrayWithObjects: (id)lightColor, (id)darkColor, nil]];
		[[self layer] insertSublayer:topShadow atIndex:0];
	}
	else if ([[[self layer] sublayers] indexOfObjectIdenticalTo:topShadow] != 0)
	{
		[[self layer] insertSublayer:topShadow atIndex:0];
	}
	
	CGRect shadowFrame = [topShadow frame];
	shadowFrame.size.width = [self frame].size.width;
	shadowFrame.origin.y = [topShadow frame].origin.y;
	[topShadow setFrame:shadowFrame];
}




@end
