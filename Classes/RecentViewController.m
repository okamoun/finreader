#import "RecentViewController.h"


@implementation RecentViewController

@synthesize keys;
@synthesize tv;

NSComparisonResult numberSort(NSNumber *n1, NSNumber *n2, void *context) {
    return [n1 compare:n2];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        keys = nil;
		updating = NO;
    }
    return self;
}

- (void)dealloc {
	[keys release], keys = nil;
	[tv release], tv = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return @"Recent";
}

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@"Recent Reports"];
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	keys = [[NSArray alloc] initWithArray:[[[FileManager getRecentReports] allKeys] sortedArrayUsingFunction:numberSort context:nil]];
}

- (void)viewDidUnload {
    [keys release];
}

- (void)viewDidAppear:(BOOL) animated {
	[tv reloadData];
	[super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [keys count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSNumber *key = [keys objectAtIndex:section];
	if([key intValue] == 0) return @"Today";
	if([key intValue] == 1) return @"Yesterday";
	return [NSString stringWithFormat:@"%d days ago", [key intValue]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[[FileManager getRecentReports] objectForKey:[keys objectAtIndex:section]] count];
}

- (report *) getReportForIndex:(NSIndexPath *) indexPath {
	NSArray *a = [[FileManager getRecentReports] objectForKey:[keys objectAtIndex:[indexPath indexAtPosition:0]]];
	return [a objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"c";
	ReportCell *cell = (ReportCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		UIViewController *c = [[UIViewController alloc] initWithNibName:@"ReportCell"  bundle:nil];
		cell = (ReportCell *)c.view;
		[c release];
	}	
	
	report *currentReport = [self getReportForIndex:indexPath];
	[cell setReport:currentReport];
	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	report *currentReport = [self getReportForIndex:indexPath];
	if(![currentReport isAvailable]) 
	{
		UIAlertView *unaivalableAlert = [[UIAlertView alloc] initWithTitle:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_title"]  
																   message:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_message"]
																  delegate:self 
														 cancelButtonTitle:@"Cancel"
														 otherButtonTitles:@"Buy", nil];
		[unaivalableAlert show];
		[unaivalableAlert release];
		return nil;
	}
	return indexPath;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1)
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=376803429&mt=8"]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	report *currentReport = [self getReportForIndex:indexPath];
	CompleteReportViewController *anotherViewController = [[CompleteReportViewController alloc] initWithReport:currentReport];
	[self.navigationController pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
}

@end

