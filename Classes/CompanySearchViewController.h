#import <UIKit/UIKit.h>
#import "TinUITableViewController.h"
#import "ReportCell.h"
#import "company.h"
#import "reportGroup.h"
#import "report.h"
#import "CompleteReportViewController.h"
#import "PropertiesGetter.h"
#import "FileManager.h"

@interface CompanySearchViewController : TinUITableViewController <UISearchBarDelegate> {
	company *comp;
	NSMutableArray *search;
}

@property (nonatomic, retain) company *comp;
@property (nonatomic, retain) NSMutableArray *search;

- (id) initWithSCompany:(company *)c;

@end
