#import <UIKit/UIKit.h>
#import "bookmark.h"

@interface BookmarkCell : UITableViewCell {
	UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

- (void) setBookmark:(bookmark *)b;

@end
