#import <Foundation/Foundation.h>
#import "MailController.h"

#define DO_NOT_NOTIFY 0
#define NOTIFY_AS_INTERNAL_ERROR 1
#define NOTIFY 2
#define NOTIFY_NO_BUG 3

#ifdef DEBUG
#define NOTIFY_DEBUG NOTIFY
#else
#define NOTIFY_DEBUG DO_NOT_NOTIFY
#endif

#define FATAL(notify,...)	 [ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:3 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:nil];
#define ERROR(notify,...)	 [ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:2 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:nil];
#define WARNING(notify,...)	 [ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:1 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:nil];
#define USERINFO(notify,...) [ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:0 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:nil];

#define FATAL_WE(notify,error,...)		{[error retain];[ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:3 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:error];[error release];}
#define ERROR_WE(notify,error,...)		{[error retain];[ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:2 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:error];[error release];}
#define WARNING_WE(notify,error,...)	{[error retain];[ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:1 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:error];[error release];}
#define USERINFO_WE(notify,error,...)	{[error retain];[ErrorManager errorWithMessage:[NSString stringWithFormat:__VA_ARGS__] level:0 where:[NSString stringWithFormat:@"%s[%d]", __PRETTY_FUNCTION__, __LINE__] andNotify:notify andError:error];[error release];}


@interface ErrorManager : NSObject<UIAlertViewDelegate> {
}

+ (void) errorWithMessage:(NSString *)message level:(int)level where:(NSString *)where andNotify:(int)notify andError:(NSError *)err;
+ (void) setCurrentController:(UIViewController *)vc;
+ (void) setURL:(NSString *)url;

@end
