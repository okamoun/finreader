#import <UIKit/UIKit.h>
#import "TinUITableViewController.h"

#import "iAd/ADBannerView.h"
#import "PropertiesGetter.h"
#import "ErrorManager.h"
#import "CompanyViewController.h"
#import "CompanyCell.h"
#import "AppDelegate.h"

@interface CompaniesViewController : TinUIViewController <UISearchBarDelegate, ADBannerViewDelegate> {
	NSArray *companies;
	NSMutableArray *searchCompanies;
	
	BOOL searching;
	BOOL presearch;
	
	BOOL updating;
	
	BOOL keyboardShown;
#ifdef __IPHONE_3_2
	CGRect hiddenRect;
#else	
	float keyboardHeight;	
#endif
	
	UIView *updateView;
	UITableView *tv;
	
	UIView *contentView;
	id adBannerView; //is instead of UIBannerView to assure retro compatibility
	BOOL bannerVisible;
	
	int nb_companies;
	int nb_reports;
	
	//update labels
	UILabel *cancelLabel;
	UILabel *releaseToUpdateLabel;
	UILabel *nbLabels;
	UILabel *updateLabel;
	UILabel *updatingLabel;
	UIActivityIndicatorView *updateIndicator;
	UIImageView *cancelArrow;
}

@property (nonatomic, retain) NSArray *companies;
@property (nonatomic, retain) NSMutableArray *searchCompanies;
@property (nonatomic, retain) IBOutlet UIView *updateView;
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) IBOutlet UITableView *tv;
@property (nonatomic, retain) IBOutlet id adBannerView;

@property (nonatomic, retain) IBOutlet UILabel *cancelLabel;
@property (nonatomic, retain) IBOutlet UILabel *releaseToUpdateLabel;
@property (nonatomic, retain) IBOutlet UILabel *nbLabels;
@property (nonatomic, retain) IBOutlet UILabel *updateLabel;
@property (nonatomic, retain) IBOutlet UILabel *updatingLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *updateIndicator;
@property (nonatomic, retain) IBOutlet UIImageView *cancelArrow;

- (void) searchTableView:(NSString *)searchText;
- (void) cancelSearch:(id)sender;
- (void) createAdBannerView;
- (void) fixupAdView:(UIInterfaceOrientation)toInterfaceOrientation;
- (void) updateNbAndUpDate;


@end
