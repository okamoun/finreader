#import "VirtualXBRLObjCell.h"
#import "report.h"

@interface VirtualReportCell : VirtualXBRLObjCell {
	IBOutlet UILabel *dateLabel;
	IBOutlet UIImageView *grey;
	IBOutlet UIImageView *orange;
	IBOutlet UIImageView *green;
}

@property (nonatomic, retain) IBOutlet UILabel *dateLabel;
@property (nonatomic, retain) IBOutlet UIImageView *grey;
@property (nonatomic, retain) IBOutlet UIImageView *orange;
@property (nonatomic, retain) IBOutlet UIImageView *green;

- (void) setReport:(report *)r;

@end
