#import "ReportDownloader.h"
#import "AppDelegate.h"
#import "ZipArchive.h"
#import "LoadReportStates.h"

//#ifndef CHANGE_STATE(state)
#define CHANGE_STATE(state) [NSThread detachNewThreadSelector:stateCallback toTarget:caller withObject:[NSNumber numberWithInt:state]];
//#endif CHANGE_STATE(state)

@implementation ReportDownloader

+ (void) loadReport:(NSArray *)arguments {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSError *error = nil;
	
	report *rep = [arguments objectAtIndex:0];
	id caller = [arguments objectAtIndex:1];
	SEL callback = NSSelectorFromString([arguments objectAtIndex:2]);
	SEL errorCallback = NSSelectorFromString([arguments objectAtIndex:3]);
	SEL stateCallback = NSSelectorFromString([arguments objectAtIndex:4]);
	
	if([rep sections] != nil) { //already loaded
		[NSThread detachNewThreadSelector:callback 
								 toTarget:caller 
		 					   withObject:nil]; 
		return;
	}

	
	NSString *filename = [rep getFileName];
	NSString *filenamePart = [rep getPartFileName];
	NSFileManager *fm = [NSFileManager defaultManager];	
	
	NSMutableArray *reportContent;
	if([fm fileExistsAtPath:filename]) {
		CHANGE_STATE(LR_LOADING);
		reportContent = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithFile:filename] copyItems:YES];
		for(int i = 0; i < [reportContent count]; i++) {
			section *s = [reportContent objectAtIndex:i];
			[s setReport:rep andParent:nil];
			[s checkMultiplicator];
		}
		[FileManager upLocalReport:rep];
		[rep setSections:reportContent];
		CHANGE_STATE(LR_DISPLAYING);
		[NSThread detachNewThreadSelector:callback 
								 toTarget:caller 
							   withObject:nil];
		[reportContent release];
	}
	else{
		if([fm fileExistsAtPath:filenamePart]) {
			CHANGE_STATE(LR_LOADING);
			reportContent = [[NSMutableArray alloc] initWithArray:[NSKeyedUnarchiver unarchiveObjectWithFile:filenamePart] copyItems:YES];
			for(int i = 0; i < [reportContent count]; i++) {
				section *s = [reportContent objectAtIndex:i];
				[s setReport:rep andParent:nil];
				[s checkMultiplicator];
			}
			[rep setSections:reportContent];
			CHANGE_STATE(LR_DISPLAYING);
			[NSThread detachNewThreadSelector:callback
										 toTarget:caller 
									   withObject:nil];
				
			NSError *err = nil;
			for(int i=0; i < [reportContent count]; i++)
			{
				section *s = [reportContent objectAtIndex:i];
				err = [s loadHTMLValues];
				if(err != nil) {
					ERROR(NOTIFY, @"Error while loading HTML");
					break;
				}
			}
			if (err == nil) 
			{
				NSFileManager *fm = [NSFileManager defaultManager];
				[fm moveItemAtPath:filenamePart toPath:filename error:nil];
			}
		[reportContent release];
	} else {
		CHANGE_STATE(LR_DOWNLOADING);
		NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@reports/%@/report.zip&id=%@", 
													[PropertiesGetter getBaseURL], 
													[rep link], 
													[[UIDevice currentDevice] identifierForVendor]]];
		NSLog(@"url : %@", url);
		[AppDelegate addConnection];
		NSData *zipContent = [NSData dataWithContentsOfURL:url];
		CHANGE_STATE(LR_UNCOMPRESSING);
		[AppDelegate releaseConnection];
		NSString *tmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat: @"%.0f.%@", [NSDate timeIntervalSinceReferenceDate] * 1000.0, @"zip"]]; 
		[zipContent writeToFile:tmpFile atomically:YES];
		ZipArchive *za = [[ZipArchive alloc] init];
		[za UnzipOpenFile:tmpFile];
		NSString *contentTmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat: @"%.0f", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
		NSLog(@"tmp file : %@", tmpFile);
		NSLog(@"content tmp file : %@", contentTmpFile);
		[za UnzipFileTo:contentTmpFile overWrite:YES];
		[za UnzipCloseFile];
		CHANGE_STATE(LR_PARSING);
		NSString *reportFile = [contentTmpFile stringByAppendingPathComponent:@"report.xml"];
		NSLog(@"report file : %@", reportFile);
		NSData *content = [[NSData dataWithContentsOfFile:reportFile] retain];
		XMLToSectionsParser *parser = [[XMLToSectionsParser alloc] initWithData:content thread:nil];//loadingThread  -> donne lui le nmero et il interogera
		[parser parse];
		//[content release];
		[za release];
		//[fm removeItemAtPath:tmpFile error:nil];
		//	[fm removeItemAtPath:contentTmpFile error:nil];		
		[url release];
		error = [[parser parseError] retain];

		if([error code] != 0)
		{
			CHANGE_STATE(LR_ERROR);
			NSLog(@"parser error");
			[caller performSelectorOnMainThread:errorCallback 
									 withObject:[NSArray arrayWithObjects:[rep link], error, nil] 
								  waitUntilDone:NO];
		}
		else {
			CHANGE_STATE(LR_LOADING);
			reportContent = [[NSMutableArray alloc] init];
			for(int i = 0; i < [[parser items] count]; i++) {
				section *s = [[parser items] objectAtIndex:i];
				[s setReport:rep andParent:nil];
				[s checkMultiplicator];
				[reportContent addObject:s];
			}
			[fm createFileAtPath:filenamePart contents:nil attributes:nil];
			[NSKeyedArchiver archiveRootObject:reportContent toFile:filenamePart];
			[FileManager addLocalReport:rep];
			[rep setSections:reportContent];
			CHANGE_STATE(LR_DISPLAYING);
			[NSThread detachNewThreadSelector:callback
									 toTarget:caller 
								   withObject:nil];
				
			NSError *err = nil;
			for(int i=0; i < [reportContent count]; i++)
			{
				section *s = [reportContent objectAtIndex:i];
				err = [s loadHTMLValues];
				if(err != nil) {
					NSLog(@"Error while loading HTML");
					break;
				}
			}
			if (err == nil) 
			{
				NSFileManager *fm = [NSFileManager defaultManager];
				[fm moveItemAtPath:filenamePart toPath:filename error:nil];
			}
			[reportContent release];
				
		}
		[error release];
		[parser release];
        [content release];
	}
}
	
	[pool release];
}

+ (void) loadReport:(report *)selectedReport withCallbackTarget:(id)caller andSelector:(NSString *)selectorString andErrorSelector:(NSString *)errorSelectorString andStateSelector:(NSString *)stateSelector{
	[NSThread detachNewThreadSelector:@selector(loadReport:)
							 toTarget:self 
						   withObject:[NSArray arrayWithObjects:selectedReport, caller, selectorString, errorSelectorString, stateSelector, nil]];
}

@end
