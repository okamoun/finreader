#import <UIKit/UIKit.h>
#import "TinUIViewController.h"

#import "section.h"

@interface HTMLViewController : TinUIViewController {
	IBOutlet UIWebView *webView;
	NSString *html;
	BOOL isXml;
}

@property (nonatomic, retain) NSString *html;
@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, assign, getter=isWorking) BOOL isXml;

- (id) initWithSection:(section *)s;

- (void) removeFromSuperview;

@end
