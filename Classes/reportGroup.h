#import <Foundation/Foundation.h>

@class report;

@interface reportGroup : NSObject {
	NSString *type_;
	NSMutableArray *reports;
}

@property (nonatomic, retain) NSString *type_;
@property (nonatomic, retain) NSMutableArray *reports;

//xml parsing methods
- (void) addReport:(report *)r;

@end
