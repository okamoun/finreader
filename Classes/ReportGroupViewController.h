#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "report.h"
#import "reportGroup.h"
#import "CompleteReportViewController.h"
#import "PropertiesGetter.h"
#import "FileManager.h"
#import "SelectableHeader.h"
#import "ReportDownloader.h"
#import "SimpleReportCell.h"

@interface ReportGroupViewController : TinUIViewController<SelectableHeaderDelegate> {
	TinUIViewController *parent;
	UITableView *tv;
	reportGroup *group;
	UIView *header;
}

@property (nonatomic, retain) TinUIViewController *parent;
@property (nonatomic, retain) IBOutlet UITableView *tv;
@property (nonatomic, retain) reportGroup *group;
@property (nonatomic, retain) UIView *header;

- (id) initWithReportGroup:(reportGroup *)ngroup
				 andParent:(TinUIViewController *)p;

- (report *) getReportForIndex:(NSIndexPath *) indexPath;

@end
