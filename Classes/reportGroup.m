#import "reportGroup.h"

@implementation reportGroup

@synthesize type_;
@synthesize reports;

- (id) init {
	if(self = [super init])
	{
		reports = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

- (void) dealloc {
	[type_ release];
	[reports release];
	[super dealloc];
}

-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setType_:[coder decodeObject]];
		[self setReports:[coder decodeObject]];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: type_];
	[coder encodeObject: reports];
}

- (void) addReport:(report *)r {
	for(int i = 0; i < [[self reports] count]; i++) {
		if([[r date] compare:[[[self reports] objectAtIndex:i] date]] == NSOrderedDescending) {
			[[self reports] insertObject:r atIndex:i];
			return;
		}
	}
	[[self reports] addObject:r];
}

@end
