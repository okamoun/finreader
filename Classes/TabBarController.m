#import "TabBarController.h"

@implementation TabBarController

- (void)viewDidLoad {
	[self setDelegate:self];
	[FileManager performSelectorInBackground:@selector(reload:) withObject:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
	NSLog(@"ask if updating");
	if([FileManager isUpdating]) NSLog(@"updating");
	else NSLog(@"not updating");
	if([FileManager isUpdating]) return NO;
	return YES;
}

- (void)dealloc {
    [super dealloc];
}

@end
