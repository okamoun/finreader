#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window;
@synthesize tabBarController;

static UIApplication *app;
static int nbConnections;
static UIDeviceOrientation orientation;

+ (void) initialize {
	nbConnections = 0;
	app = nil;
	orientation = UIDeviceOrientationPortrait;

#ifndef DEBUG
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
#endif
}

- (void)dealloc {
	[tabBarController release];
	[window release];
	[super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
	app = application;
	[window addSubview:[tabBarController view]];
	[window makeKeyAndVisible];
	[FileManager start];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(didRotate:)
												 name:UIDeviceOrientationDidChangeNotification
											   object:nil];
	return YES;
}

- (void) didRotate:(NSNotification *)notification {
	if([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait
	   || [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown
	   || [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft
	   || [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
		orientation = [[UIDevice currentDevice] orientation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

+ (UIDeviceOrientation) getDeviceOrientation {
	if([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait
	   || [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown
	   || [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft
	   || [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
		orientation = [[UIDevice currentDevice] orientation];	
	return orientation;
}

+ (void) addConnection {
	nbConnections++;
	[app setNetworkActivityIndicatorVisible:YES];
}

+ (void) releaseConnection {
	if(--nbConnections == 0) [app setNetworkActivityIndicatorVisible:NO];
}

+ (void) hideTabs {
	//[[[app delegate] tabBarController] setHidden:YES];
}

+ (void) displayTabs {
	//[[[app delegate] tabBarController] setHidden:NO];
}

@end

