#import "value.h"


@implementation value

@synthesize date;
@synthesize val;
@synthesize footnote;
@synthesize contextNames;
@synthesize contextValues;

static NSDateFormatter *dateFormatter;

+ (void) initialize {
	dateFormatter = [[NSDateFormatter  alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
}

- (id) init {
	if(self = [super init]) {
		contextNames = [[NSMutableArray alloc] initWithCapacity:0];
		contextValues = [[NSMutableArray alloc] initWithCapacity:0];
		[self setFootnote:@""];
		[self setVal:@""];
		[self setDate:@""];
	}
	return self;
}

- (id) initWithContextnames:cn andContextValues:cv {
	if(self = [super init]) {
		contextNames = [[NSMutableArray alloc] initWithArray:cn copyItems:YES];
		contextValues = [[NSMutableArray alloc] initWithArray:cv copyItems:YES];
	}
	return self;
}

- (void) dealloc {
	[date release];
	[val release];
	[footnote release];
	[contextNames release];
	[contextValues release];
	[super dealloc];
}

-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setDate:[coder decodeObject]];
		[self setFootnote:[coder decodeObject]];
		[self setVal:[coder decodeObject]];
		[self setContextNames:[coder decodeObject]];
		[self setContextValues:[coder decodeObject]];
	}
	return self;
}

-(id)copyWithZone:(NSZone*)zone {
    value *copy = [[[self class] allocWithZone:zone] initWithContextnames:contextNames andContextValues:contextValues];
	[copy setDate:date];
	[copy setFootnote:footnote];
	[copy setVal:val];
	return copy;
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: date];
	[coder encodeObject: footnote];
    [coder encodeObject: val];
	[coder encodeObject: contextNames];
	[coder encodeObject: contextValues];
}

- (void) addContextName:(NSString *)n{
	[contextNames addObject:n];
}

- (void) addContextValue:(NSString *)v {
	[contextValues addObject:v];
}

- (BOOL) matchSingleCondition:(NSString *)condition
{
	NSArray *s = [condition componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"="]];
	if([[s objectAtIndex:0] isEqualToString:@"other"]) {
		return ([contextNames count] == 0);	
	}
	if([[s objectAtIndex:0] isEqualToString:@"date"])
	{
		return [date isEqualToString:[s objectAtIndex:1]];
	}
	if([[s objectAtIndex:0] isEqualToString:@"duration"])
	{
		return ([[s objectAtIndex:1] intValue] == [self getDuration]);
	}

	NSUInteger index = [contextNames indexOfObject:[s objectAtIndex:0]];
	if(index != NSNotFound)
		return [[contextValues objectAtIndex:index] isEqualToString:[s objectAtIndex:1]];
	return NO;
}

- (BOOL) matchConditions:(NSArray *)conditions
{
	for(int i=0; i<[conditions count]; i++)
	{
		if(![self matchSingleCondition:[conditions objectAtIndex:i]]) return NO;
	}
	return YES;
}

- (NSString *) getDate {
	return date;
}

- (NSString *) getLastDate {
	NSArray *a = [[date componentsSeparatedByString:@" to "] retain];
    if([a count] < 2) {
		[a release];
		return date;
	}
	NSString *ret = [[a objectAtIndex:1] retain];
	[a release];
	return [ret autorelease];
}


- (int) getDuration {
	NSArray *a = [[date componentsSeparatedByString:@" to "] retain];
    if([a count] < 2) {
		[a release];
		return 0;
	};
	
	NSDate *d1 = [[dateFormatter dateFromString:[a objectAtIndex:0]] retain];
	NSDate *d2 = [[dateFormatter dateFromString:[a objectAtIndex:1]] retain];
	[a release];
	NSCalendar *sysCalendar = [NSCalendar currentCalendar];
	unsigned int unitFlags = NSMonthCalendarUnit;
	//TODO : sometimes a probleme here !!
	NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:d1  toDate:d2  options:0];
	[d1 release];
	[d2 release];
	return [breakdownInfo month];
}

- (void) multValue:(int)m {
	int vi = [val intValue];
	vi *= m;
	[self setVal:[NSString stringWithFormat:@"%d", vi]];
}

@end
