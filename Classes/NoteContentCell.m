#import "NoteContentCell.h"

@implementation NoteContentCell

@synthesize content;

- (void)dealloc {
	[content release];
    [super dealloc];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
	[content setNeedsDisplay]; 
}

@end
