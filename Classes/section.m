#import "section.h"
#import "ZipArchive.h"

@implementation section

@synthesize xbrlId;
@synthesize title;
@synthesize definition;
@synthesize unit;
@synthesize stype;
@synthesize multiplicator;
@synthesize istotal;
@synthesize isbegin;
@synthesize values;
@synthesize subSections;
@synthesize dates;
@synthesize rdates;
@synthesize dimensionsStyleSheet;
@synthesize footnotes;
@synthesize graphValues;
@synthesize currentGraphValues;
@synthesize HTMLValuesLoaded;
@synthesize myReport;
@synthesize parentSection;

static NSArray *colors;
static NSDateFormatter *dateFormatter;

+ (void) initialize {
	colors = [[NSArray alloc] initWithObjects:@"aqua", @"red", @"chartreuse", 
	@"yellow", @"darkviolet", @"pink", @"purple", @"greenyellow", @"coral", @"green", nil];
	dateFormatter = [[NSDateFormatter  alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
}

- (id) init {
	if(self = [super init]) {
		values = [[NSMutableArray alloc] initWithCapacity:0];
		subSections = [[NSMutableArray alloc] initWithCapacity:0];
		[self setIstotal:[NSNumber numberWithBool:NO]];
		[self setIsbegin:[NSNumber numberWithBool:NO]];
		[self setHTMLValuesLoaded:[NSNumber numberWithBool:NO]];
	}
	return self;
}

- (id) initWithValues:(NSMutableArray *)v andSubSections:(NSMutableArray *)s andRdates:(NSArray *)rd {
	if(self = [super init]) {
		values = [[NSMutableArray alloc] initWithArray:v copyItems:YES];
		subSections = [[NSMutableArray alloc] initWithArray:s copyItems:YES];
		[self setRdates:rd];
		//rdates = [[NSArray alloc] initWithArray:rd]; //[[NSArray alloc] initWithArray:rd copyItems:YES];
	}
	return self;
}

- (void) dealloc {
	[xbrlId release];
	[title release];
	[definition release];
	[unit release];
	[stype release];
	[multiplicator release];
	[istotal release];
	[isbegin release];
	[HTMLValuesLoaded release];
	[values release];
	[subSections release];
	[rdates release];
	[myReport release];
	[parentSection release];
	[super dealloc];
}

-(id)copyWithZone:(NSZone*)zone {
    section *copy = [[[self class] allocWithZone:zone] initWithValues:values andSubSections:subSections andRdates:rdates];
	[copy setXbrlId:xbrlId];
	[copy setTitle:title];
	[copy setDefinition:definition];
	[copy setUnit:unit];
	[copy setStype:stype];
	[copy setMultiplicator:multiplicator];
	[copy setIstotal:istotal];
	[copy setHTMLValuesLoaded:HTMLValuesLoaded];
    return copy;
}


-(id)initWithCoder:(NSCoder*)coder
{
    if (self=[super init]) {
		[self setXbrlId:[coder decodeObject]];
		[self setTitle:[coder decodeObject]];
		[self setDefinition:[coder decodeObject]];
		[self setUnit:[coder decodeObject]];
		[self setStype:[coder decodeObject]];
		[self setMultiplicator:[coder decodeObject]];
		[self setIstotal:[coder decodeObject]];
		[self setValues:[coder decodeObject]];
		[self setSubSections:[coder decodeObject]];
		[self setRdates:[coder decodeObject]];
		[self setHTMLValuesLoaded:[coder decodeObject]];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject: xbrlId];
	[coder encodeObject: title];
    [coder encodeObject: definition];
	[coder encodeObject: unit];
	[coder encodeObject: stype];
	[coder encodeObject: multiplicator];
	[coder encodeObject: istotal];
	[coder encodeObject: values];
	[coder encodeObject: subSections];
	[coder encodeObject: rdates];
	[coder encodeObject: HTMLValuesLoaded];
}

- (void) addValue:(value *)v {
	[values addObject:v];
}

- (void) addSubSection:(section *)s {
	[subSections addObject:s];
}

- (int) getNbLevel {
	int max = 0;
	for(int i = 0; i < [subSections count]; i++) 
	{
		int n = [[subSections objectAtIndex:i] getNbLevel];
		max = MAX(max, n);
	}
	return max+1;
}

- (BOOL) hasMultiValue {
	for(int i = 0 ; i < [values count] ; i++)
	{
		if([[[values objectAtIndex:i] contextNames] count] > 0) return YES;
	}
	return NO;
}

- (void) setDates:(NSArray **)d {
	dates = d;
	for (int i=0; i< [subSections count]; i++) {
		[[subSections objectAtIndex:i] setDates:d];
	}
}

- (BOOL) hasOnlyOneValuePerRow {
	if([values count] > 1) return NO;
	for (int i = 0 ; i < [subSections count] ; i++) {
		if(![[subSections objectAtIndex:i] hasOnlyOneValuePerRow]) return NO;
	}
	return YES;
}

- (NSArray *) getAllDates:(BOOL)setDates
{
	BOOL nextSetDates = setDates;
	if(setDates)
	{
		for (int i=0; i< [subSections count]; i++) {
			if([[[subSections objectAtIndex:i] values] count] > 0){
				nextSetDates = NO;
				break;
			}
		}
	}
	if(nextSetDates)
	{
		for (int i=0; i< [subSections count]; i++)
			[[subSections objectAtIndex:i] getAllDates:YES];
		return nil;
	}
	//------ else --- 
	NSMutableArray * ret = [NSMutableArray arrayWithCapacity:0];
	for (int i=0; i< [values count]; i++) {
		if(![ret containsObject:[[values objectAtIndex:i] getLastDate]])
			[ret addObject:[[values objectAtIndex:i] getLastDate]];
	}
	
	for (int i=0; i< [subSections count]; i++) {
		NSArray *temp = [[subSections objectAtIndex:i] getAllDates:NO];
		for(int j=0; j < [temp count]; j++)
		{
			if(![ret containsObject:[temp objectAtIndex:j]])
				[ret addObject:[temp objectAtIndex:j]];
		}
	}
	
	if(setDates) {
		NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:nil ascending:NO selector:@selector(compare:)];	
		[ret sortUsingDescriptors:[NSArray arrayWithObject:desc]];
		[self setRdates:ret];
		//rdates = [[NSArray alloc] initWithArray:ret];
		[desc release];
		[self setDates:&rdates];
		return nil;
	}
	return ret;
}

- (BOOL) hasFootNoteForDate:(int)did {
	if(dates == nil) return NO;
	if([*dates count] <= did ) return NO; 
	NSString *d = [*dates objectAtIndex:did];
	for(int i = 0; i < [values count]; i++)
	{
		if([[[values objectAtIndex:i] getDate] isEqualToString:d])
		{
			if([[values objectAtIndex:i] footnote] != nil) {
				if(![[[values objectAtIndex:i] footnote] isEqualToString:@""])
				{
					return YES;
				}
			}
		}
	}
	return NO;
}

- (int) getMinDuration {
	if([values count] == 0) return 0;
	int ret = [[values objectAtIndex:0] getDuration];
	for(int i = 1; i < [values count] ; i++) {
		if([[values objectAtIndex:i] getDuration] < ret) ret = [[values objectAtIndex:i] getDuration];
	}
	return ret;
}

- (BOOL) valuesAreNumbers {
	return ([stype isEqualToString:@"xbrli:monetaryItemType"] || [stype isEqualToString:@"xbrli:sharesItemType"] || [stype isEqualToString:@"us-types:perShareItemType"] || [stype isEqualToString:@"us-types:percentItemType"]);
}

- (float) getRawValueForDate:(int)did {

	int minDuration = [self getMinDuration];
	if([isbegin boolValue]) did++;
	if(dates == nil) { return 0.; }
	if([*dates count] <= did ) { return 0.; }
	NSString *d = [*dates objectAtIndex:did];
	float ret = 0;
	for(int i = 0; i < [values count]; i++)
	{
		if([[[values objectAtIndex:i] getLastDate] isEqualToString:d] && ([[values objectAtIndex:i] getDuration] == minDuration))
		{
			if([stype isEqualToString:@"xbrli:monetaryItemType"] || [stype isEqualToString:@"xbrli:sharesItemType"] || [stype isEqualToString:@"us-types:perShareItemType"]) {
				if([[[values objectAtIndex:i] contextNames] count] == 0 && [self hasOtherValues]) {
					ret = [[[values objectAtIndex:i] val] floatValue];
					break;
				} else if(![self hasOtherValues]) {
					ret += [[[values objectAtIndex:i] val] floatValue];
				}
			}
			else if ([stype isEqualToString:@"us-types:percentItemType"]) {
				ret = [[[values objectAtIndex:i] val] floatValue];
				break;
			}
			else 
				return 0;
		}
	}
	return ret;
}

- (NSString*) getValueForDate:(int)did {
		
	int minDuration = [self getMinDuration];

	float ret;
	if([self valuesAreNumbers]) {
		ret = [self getRawValueForDate:did];
	} else {
		if([isbegin boolValue]) did++;
		if(dates == nil) { return @"-"; }
		if([*dates count] <= did ) { return @"-"; }
		NSString *d = [*dates objectAtIndex:did];
		for(int i = 0; i < [values count]; i++)
		{
			if([[[values objectAtIndex:i] getLastDate] isEqualToString:d] && ([[values objectAtIndex:i] getDuration] == minDuration))
			{
				return [[values objectAtIndex:i] val];
			}
		}
		return @"";
	}

	if(ret == 0) return @"-";
	NSNumberFormatter *numberStyle = [[NSNumberFormatter alloc] init];

	if([stype isEqualToString:@"us-types:percentItemType"])
	{
		[numberStyle setNumberStyle:NSNumberFormatterPercentStyle];
		[numberStyle setMaximumFractionDigits:2];
	} 
	else if([PropertiesGetter getBOOLForKey:@"print_currency"]) {	
		[numberStyle setPositiveFormat:@"¤#,###.##"];
		[numberStyle setNegativeFormat:@"¤(#,###.##)"];
		[numberStyle setNumberStyle:NSNumberFormatterCurrencyStyle];
		
		NSString *test;
		if([stype isEqualToString:@"us-types:perShareItemType"]) test = [[unit componentsSeparatedByString:@"/"] objectAtIndex:0];
		else test = unit;
				
		if([test caseInsensitiveCompare:@"iso4217:usd"] == NSOrderedSame){
			[numberStyle setCurrencySymbol:@"$"];
		} else if ([test caseInsensitiveCompare:@"iso4217:eur"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@"€"];
		} else if ([test caseInsensitiveCompare:@"iso4217:cad"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@"CA$"];
		} else if ([test caseInsensitiveCompare:@"iso4217:chf"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@"CHF"];
		} else if ([test caseInsensitiveCompare:@"iso4217:gbp"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@"£"];
		} else if ([test caseInsensitiveCompare:@"iso4217:jpy"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@"¥"];
		} else if ([test caseInsensitiveCompare:@"iso4217:brl"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@"BR"];
		} else if ([test caseInsensitiveCompare:@"xbrli:pure"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@""];
		} else if ([test caseInsensitiveCompare:@"xbrli:shares"] == NSOrderedSame || [unit caseInsensitiveCompare:@"shares"] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@""];
		} else if ([test caseInsensitiveCompare:@""] == NSOrderedSame) {
			[numberStyle setCurrencySymbol:@""];
		} else {
			[numberStyle setCurrencySymbol:@"?"];
		}
	} else {
		 [numberStyle setNumberStyle:NSNumberFormatterDecimalStyle];
	}
	NSString *retStr = [numberStyle stringFromNumber:[NSNumber numberWithFloat:ret]];
	[numberStyle release];
	return retStr;
}

- (NSString *) valueForConditions:(NSArray *)conditions
{
	int nb_values_matching = 0;
	int matching;
	float ret = 0;
	NSNumberFormatter *numberStyle = [[NSNumberFormatter alloc] init];
	[numberStyle setPositiveFormat:@"#,###.##"];
	[numberStyle setNegativeFormat:@"(#,###.##)"];
	for(int i = 0; i < [values count]; i++)
	{
		if([[values objectAtIndex:i] matchConditions:conditions])
		{
			if([stype isEqualToString:@"xbrli:monetaryItemType"] || [stype isEqualToString:@"xbrli:sharesItemType"]){
				ret += [[[values objectAtIndex:i] val] floatValue];
			}
			else if([stype isEqualToString:@"us-types:percentItemType"]) {
				NSString *ret = [numberStyle stringFromNumber:[NSNumber numberWithFloat:[[[values objectAtIndex:i] val] floatValue] * 100]];
				[numberStyle release];
				return ret;
			} else {
				[numberStyle release];
				return [[values objectAtIndex:i] val];
			}
			nb_values_matching++;
			matching = i;
		}
	}
	NSMutableString *retStr = [NSMutableString stringWithCapacity:0];
	[retStr setString:[numberStyle stringFromNumber:[NSNumber numberWithFloat:ret]]];
	[numberStyle release];
	if(nb_values_matching == 1) {
		if([[values objectAtIndex:matching] footnote] != nil) {
			if(![[[values objectAtIndex:matching] footnote] isEqualToString:@""]) {
				[retStr appendFormat:@"<sup>%d</sup>", [self addFootNote:[[values objectAtIndex:matching] footnote]]];
			}
		}
	}
	return retStr;
}

- (NSArray *) valuesForDimension:(NSString *)dim withConditions:c
{
	//for a dimension, give all the possible values in that section
	NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
	if([dim isEqualToString:@"date"])
	{
		for (int i=0; i< [values count]; i++) {
			if ([[values objectAtIndex:i] matchConditions:c]) {			
				if(![ret containsObject:[[values objectAtIndex:i] getDate]])
					[ret insertObject:[[values objectAtIndex:i] getDate] atIndex:0];
			}
			//TODO : order by date increasing...
		}
	} else if([dim isEqualToString:@"duration"]){ 
		NSLog(@"values for dimension : %@", dim);
		NSLog(@"nb values : %d", [values count]);
		for (int i=0; i< [values count]; i++) {
			if ([[values objectAtIndex:i] matchConditions:c]) {	
				NSString *duration = [NSString stringWithFormat:@"%d", [[values objectAtIndex:i] getDuration]];
				NSLog(@"%@", duration);
				if(![ret containsObject:duration])
					[ret addObject:duration];
			}
		}
	}
	else {
		for (int i=0; i< [values count]; i++) {
			if ([[values objectAtIndex:i] matchConditions:c]) {
				NSUInteger index = [[[values objectAtIndex:i] contextNames] indexOfObject:dim];
				if(index != NSNotFound) {
					NSString * s = [[[values objectAtIndex:i] contextValues] objectAtIndex:index];
					if(![ret containsObject:s])
						[ret addObject:s];
				}
			}
		}
	}
	return ret;
}

- (NSString *) headForCols:(NSArray *)cols level:(int)l withConditions:c
{
	if([cols count] == 0) return @"";
	NSRange subRange;
	subRange.location = 1;
	subRange.length = [cols count] - 1;
	NSArray * v = [self valuesForDimension:[cols objectAtIndex:0] withConditions:c];
	if([v count] == 0) return [self headForCols:[cols subarrayWithRange:subRange] level:l withConditions:c]; //skip that col
	
	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	for(int i = 0; i < [v count]; i++)
	{
		NSString *val = [[v objectAtIndex:i] stringByReplacingOccurrencesOfString:@" to " withString:@"<br/>"];
		
    	[ret appendFormat:@"<th class=\"c%d\">%@</th>%@",
			   l, 
			   val, 
			   [self headForCols:[cols subarrayWithRange:subRange] level:l+1 withConditions:c]];	
	}
	return ret;	
}
					 
- (NSString *) valuesForCols:(NSArray *)cols colsValues:(NSArray *)colsValues level:(int)l withConditions:c
{
	if([cols count] == 0) return @"";
	NSRange subRange;
	subRange.location = 1;
	subRange.length = [cols count] - 1;
	
	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	for (int i = 0 ; i < [[colsValues objectAtIndex:0] count]; i++) {
		
		NSMutableArray *conditions = [[NSMutableArray alloc] initWithArray:c];
		[conditions addObject:[NSString stringWithFormat:@"%@=%@", [cols objectAtIndex:0], [[colsValues objectAtIndex:0] objectAtIndex:i]]];
		[currentGraphValues addObject:[self valueForConditions:conditions]];
		[ret appendFormat:@"<td class=\"c%d\">%@</td>%@",
		 l, 
		 [self valueForConditions:conditions], 
		 [self valuesForCols:[cols subarrayWithRange:subRange] colsValues:[colsValues subarrayWithRange:subRange] level:l+1 withConditions:conditions]];	
		[conditions release];
	}
	return ret;
	
	
	/*NSArray *prevConditions = [NSArray array];
	if([c count] > 1) {
		NSRange subRange2;
		subRange2.location = 0;
		subRange2.length = [c count] - 1;
		prevConditions = [c subarrayWithRange:subRange2];
	}
			
	NSArray * v = [self valuesForDimension:[cols objectAtIndex:0] withConditions:prevConditions];
	if([v count] == 0) return [NSString stringWithFormat:@"<td class=\"c%d\">-</td>%@", [self valuesForCols:[cols subarrayWithRange:subRange] level:l withConditions:c]]; //skip that col
	
	v = [self valuesForDimension:[cols objectAtIndex:0] withConditions:c];
	
	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	for(int i = 0; i < [v count]; i++)
	{
		NSMutableArray *conditions = [[NSMutableArray alloc] initWithArray:c];
		[conditions addObject:[NSString stringWithFormat:@"%@=%@", [cols objectAtIndex:0], [v objectAtIndex:i]]];
		[currentGraphValues addObject:[self valueForConditions:conditions]];
		[ret appendFormat:@"<td class=\"c%d\">%@</td>%@",
			   l, 
			   [self valueForConditions:conditions], 
			   [self valuesForCols:[cols subarrayWithRange:subRange] level:l+1 withConditions:conditions]];	
		[conditions release];
	}*/
	return ret;	
}					 

- (NSString *) htmlForRows:(NSArray *)rows andCols:(NSArray *)cols andColsValues:(NSArray *)colsValues level:(int)l withConditions:(NSArray *)c
{	
	if([rows count] == 0)
	{
		if(l == 0) {
			currentGraphValues = [[NSMutableArray alloc] initWithCapacity:[cols count]+1];
			[currentGraphValues addObject:@"Total"];
			[graphValues addObject:currentGraphValues];
			[currentGraphValues release];
			return [NSString stringWithFormat:@"<tr class=\"r%d\"><th>Total%@</th>%@</tr>",
				   l, 
				   [self svgLegend],
					[self valuesForCols:cols colsValues:colsValues level:0 withConditions:c]];
		}
		else 
			return @"";
	}
	
	NSRange subRange;
	subRange.location = 1;
	subRange.length = [rows count] - 1;
	NSArray * v = [self valuesForDimension:[rows objectAtIndex:0] withConditions:c];//nil ??
	if([v count] == 0) return [self htmlForRows:[rows subarrayWithRange:subRange] andCols:cols andColsValues:colsValues level:l withConditions:c]; //skip that row

	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	for(int i=0; i < [v count]; i++)
	{
		NSMutableArray *conditions = [[NSMutableArray alloc] initWithArray:c];
		[conditions addObject:[NSString stringWithFormat:@"%@=%@", [rows objectAtIndex:0], [v objectAtIndex:i]]];
		
		currentGraphValues = [[NSMutableArray alloc] initWithCapacity:[cols count]+1];
		[currentGraphValues addObject:[v objectAtIndex:i]];
		[graphValues addObject:currentGraphValues];
		[currentGraphValues release];
		[ret appendFormat:@"<tr class=\"r%d\"><th class=\"normal\">%@%@</th>%@</tr>%@", 
		 l, 
		 [v objectAtIndex:i],
		 [self svgLegend],
		 [self valuesForCols:cols colsValues:colsValues level:0 withConditions:conditions], 
		 [self htmlForRows:[rows subarrayWithRange:subRange] andCols:cols andColsValues:colsValues level:l+1 withConditions:conditions]];
		[conditions release];
	}
	if(l == 0) {
		
		NSMutableArray *conditions = [[NSMutableArray alloc] initWithArray:c];
		
		if([self hasOtherValues])
    		[conditions addObject:@"other"];
		currentGraphValues = [[NSMutableArray alloc] initWithCapacity:[cols count]+1];
		[currentGraphValues addObject:@"Total"];
		[graphValues addObject:currentGraphValues];
		[currentGraphValues release];
		[ret appendFormat:@"<tr class=\"r%d\"><th>Total%@</th>%@</tr>",
			   l, 
			   [self svgLegend],
		 [self valuesForCols:cols colsValues:colsValues level:0 withConditions:conditions]];
		[conditions release];
	}
	return ret;
}
	
- (NSString *) htmlForTables:(NSArray *)tables rows:(NSArray *)rows andCols:(NSArray *)cols withConditions:(NSArray *)c {
	//this is a recursive function : we have conditionn in tables, one by one, we put the condition in c (we the different cases), and release it from tables
	if([tables count] == 0)
	{
		footnotes = [[NSMutableArray alloc] initWithCapacity:0];
		graphValues = [[NSMutableArray alloc] initWithCapacity:0];
		NSMutableString * ret = [NSMutableString stringWithCapacity:0];
		
		NSMutableArray *colsValues = [[NSMutableArray alloc] initWithCapacity:[cols count]];
		for(int i = 0; i < [cols count]; i++) {
			[colsValues addObject:[self valuesForDimension:[cols objectAtIndex:i] withConditions:c]];
		}
			
		if(hasSeveralDurations) {
			NSLog(@"last object : %@", [c lastObject]);
			NSArray *a=[[c lastObject] componentsSeparatedByString:@"="];
			if([a count] > 1)
				[ret appendFormat:@"<h3>%d monthes</h3>",  [[a objectAtIndex:1] intValue]+1];
		}else {
			NSLog(@"not several durations");
		}

		[ret appendFormat:@"<table><tr><th>%@ %@</th>%@</tr>%@", [self htmlUnit], [self multiplicatorStr], [self headForCols:cols level:0 withConditions:c], [self htmlForRows:rows andCols:cols andColsValues:colsValues level:0 withConditions:c]];
        [colsValues release];
		[ret appendFormat:@"%@</table>", [self getSVGGraphWithConditions:c]];
		[graphValues release];
		[ret appendFormat:@"%@", [self getHTMLFootNotes]];
		[footnotes release];
		return ret;
	}	
	else /// tables 
	{
		NSMutableString *ret = [NSMutableString stringWithCapacity:0];
		NSArray * v = [self valuesForDimension:[tables objectAtIndex:0] withConditions:nil];
		hasSeveralDurations = ([v count] > 1);
		NSRange subRange;
		subRange.location = 1;
		subRange.length = [tables count] - 1;
		
		for(int i=0; i < [v count]; i++)
		{
			NSMutableArray *conditions = [[NSMutableArray alloc] initWithArray:c];
			[conditions addObject:[NSString stringWithFormat:@"%@=%@", [tables objectAtIndex:0], [v objectAtIndex:i]]];
			[ret appendFormat:@"%@", 
				   [self htmlForTables:[tables subarrayWithRange:subRange] 
								  rows:rows 
							   andCols:cols 
						withConditions:conditions]];
			[conditions release];
		}	
		return ret;
	}
}

- (NSArray *) allDimensions {
	NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
	for(int i = 0; i < [values count]; i++) {
		value *v = [values objectAtIndex:i];
		for (int j = 0; j < [[v contextNames] count]; j++)
		{
			if(![ret containsObject:[[v contextNames] objectAtIndex:j]])
				[ret addObject:[[v contextNames] objectAtIndex:j]];
		}
	}
	
	return ret;
}

- (NSString *) html {
	
	if(([stype isEqualToString:@"us-types:textBlockItemType"] || [stype isEqualToString:@"xbrli:stringItemType"]) && [values count] > 0) {
		NSError *err = [self loadHTMLValues];
		if(err == nil) {	
			NSString *filename = [[[self myReport] getDirName] stringByAppendingFormat:@"/%@", [[values objectAtIndex:0] val]];
			return [NSString stringWithContentsOfFile:filename encoding:NSUTF8StringEncoding error:nil];
		}
		else
			return [NSString stringWithFormat:@"Sorry, an error is occured, error: %@. Please retry later. If the problem persists, please contact us.", [err localizedDescription]];
	}
	
	NSMutableString * ret = [NSMutableString stringWithCapacity:0];
	[ret setString:[NSString stringWithFormat:@"<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><link rel=\"stylesheet\" media=\"screen\" type=\"text/css\" href=\"sections.css\" /></head><body><table class=\"content\"><tr><td><img class=\"transparent\" src=\"transparent.png\" alt=\"-\" /></td><td><table class=\"firstLevel\"><tr class=\"firstLevel\"><th class=\"firstLevel\"><h1>%@</h1></th></tr>", title]];
	
	if([values count] > 0) {
		if([unit isEqualToString:@""]) {
			[ret appendFormat:@"<tr class=\"firstLevel\"><th class=\"firstLevel\"><h2>%@</h2></th></tr>", [[values objectAtIndex:0] val]];
		} else {
			if(dimensionsStyleSheet == nil) dimensionsStyleSheet = @"default_dss";
			NSString *errorDesc = nil;
			NSPropertyListFormat format;
			NSString *plistPath = [[NSBundle mainBundle] pathForResource:dimensionsStyleSheet ofType:@"plist"];
			NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
			NSDictionary *properties = (NSDictionary *)[[NSPropertyListSerialization										  
														 propertyListFromData:plistXML
														 mutabilityOption:NSPropertyListMutableContainersAndLeaves
														 format:&format
														 errorDescription:&errorDesc] retain];
			NSString * tablesString = [properties objectForKey:@"tables"];
			NSMutableArray * tables = [[NSMutableArray alloc] initWithArray:[tablesString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]]]; 
			if([tables count] > 0) if([[tables lastObject] isEqualToString:@""]) [tables removeLastObject];
			
			NSString * rowsString = [properties objectForKey:@"rows"];
			NSMutableArray * rows = [[NSMutableArray alloc] initWithArray:[rowsString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]]];
			if([rows count] > 0) if([[rows lastObject] isEqualToString:@""]) [rows removeLastObject];
	
			NSString * colsString = [properties objectForKey:@"cols"];
			NSMutableArray * cols = [[NSMutableArray alloc] initWithArray:[colsString componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@", "]]];
			if([cols count] > 0) if([[cols lastObject] isEqualToString:@""]) [cols removeLastObject];
	
			[properties release];
	
			NSArray *allDimensions = [self allDimensions];
			if([[tables lastObject] isEqualToString:@"*"]){
				[tables removeLastObject];
				for(int i=0; i < [allDimensions count]; i++)
				{
					if([tables indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound
					   && [rows indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound
					   &&[cols indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound) {
						[tables addObject:[allDimensions objectAtIndex:i]];
					}
				}
			}
			else if ([[rows lastObject] isEqualToString:@"*"]) {
				[rows removeLastObject];
				for(int i=0; i < [allDimensions count]; i++)
				{
					if([tables indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound
					   && [rows indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound
					   && [cols indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound) {
						[rows addObject:[allDimensions objectAtIndex:i]];
					}
				}
			} else if([[cols lastObject] isEqualToString:@"*"]) {
				[cols removeLastObject];
				for(int i=0; i < [allDimensions count]; i++)
				{
					if([tables indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound
					   && [rows indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound
					   && [cols indexOfObject:[allDimensions objectAtIndex:i]] == NSNotFound) {
						[cols addObject:[allDimensions objectAtIndex:i]];
					}
				}
			}
		
			for (int i = 0 ;i < [cols count]; i++) {
				NSArray * v = [self valuesForDimension:[cols objectAtIndex:i] withConditions:nil];
				if([v count] == 0) {
					[cols removeObjectAtIndex:i];
					i--;
				}
			}
		
			NSLog(@"tables");
			for(int i = 0; i < [tables count]; i++) {
				NSLog(@"t : %@", [tables objectAtIndex:i]);
			}
	
			NSString *htmlTables = [self htmlForTables:tables rows:rows andCols:cols withConditions:nil];
			
			[tables release];
			[rows release];
			[cols release];
			
			[ret appendFormat:@"<tr class=\"firstLevel\"><th class=\"firstLevel\">%@</th></tr>", htmlTables];
		}
	}
		
	if([definition length]> 0)
		[ret appendFormat:@"<tr class=\"firstLevel\"><th class=\"firstLevel\"><p><strong>Definition : </strong>%@</p></th></tr>", definition];
	[ret appendFormat:@"</table></td><td><img class=\"transparent\" src=\"transparent.png\" alt=\"-\" /></td></tr><tr><td colspan=\"3\"><img class=\"transparent\" src=\"transparent.png\" alt=\"-\" /></td></tr></table></body></html>"];
	
	return ret;
}

- (NSString *) unit2Str:(NSString *)u {
	if([u caseInsensitiveCompare:@"iso4217:usd"] == NSOrderedSame)
		return @"USD $";
	if ([u caseInsensitiveCompare:@"iso4217:eur"] == NSOrderedSame)
		return @"€";
	if ([u caseInsensitiveCompare:@"iso4217:cad"] == NSOrderedSame)
		return @"CA $";
	if ([u caseInsensitiveCompare:@"iso4217:chf"] == NSOrderedSame)
		return @"CHF";
	if ([u caseInsensitiveCompare:@"iso4217:gbp"] == NSOrderedSame)
		return @"£";
	if ([u caseInsensitiveCompare:@"iso4217:jpy"] == NSOrderedSame)
		return @"¥";
	if ([u caseInsensitiveCompare:@"iso4217:brl"] == NSOrderedSame)
		return @"BR";
	if ([u caseInsensitiveCompare:@"xbrli:pure"] == NSOrderedSame)
		return @"";
	if ([u caseInsensitiveCompare:@"xbrli:shares"] == NSOrderedSame || [u caseInsensitiveCompare:@"shares"] == NSOrderedSame)
		return @"Shares";
	if ([u caseInsensitiveCompare:@""] == NSOrderedSame)
		return @"";
	ERROR(NOTIFY_AS_INTERNAL_ERROR, @"Unknown unit : %@", u);
	return @"?";
}
	
- (NSString *) htmlUnit {
	
	if([stype isEqualToString:@"us-types:percentItemType"])
		return @"%";
	if([stype isEqualToString:@"us-types:perShareItemType"]) {
		NSArray *units = [unit componentsSeparatedByString:@"/"];
		if([units count] == 2)
			return [NSString stringWithFormat:@"%@ per %@", [self unit2Str:[units objectAtIndex:0]], [self unit2Str:[units objectAtIndex:1]]];
	}
	return [self unit2Str:unit];
}

- (int) addFootNote:(NSString *)note {
	for(int i = 0; i < [footnotes count]; i++)
	{
		if([[footnotes objectAtIndex:i] isEqualToString:note]) return (i+1);
	}
	[footnotes addObject:note];
	return [footnotes count];
}

- (NSString *) getHTMLFootNotes {
	NSMutableString *ret = [NSMutableString string];
	for(int i =0; i < [footnotes count] ; i++) {
		[ret appendFormat:@"<p><strong>%d. </strong>%@</p>", i+1, [footnotes objectAtIndex:i]];
	}
	return ret;
}

- (NSString *) svgLegend {
	return [NSString stringWithFormat:@"<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 30 30\" style=\"width:30px; height: 30px;\"> <path d=\"M5,25 L25,5\" style=\"stroke:%@; fill:none; stroke-width:7;\" /> </svg>", 
					 [colors objectAtIndex:(([graphValues count]-1)%[colors count])]];
}

- (NSString *)getSVGGraphWithConditions:(NSArray *)c {
	if([currentGraphValues count] < 3) return @"";
	int nbYpoints = 4;//this is the minimum
	int nbXpoints = [currentGraphValues count]-1;
	int height = 350;
	int width = 520;
	int arrayLength = 25;
	int arrayWidth = 10;
	int offset = 235;
	int bottomMarge = 20;

	NSNumberFormatter *numberStyle = [[NSNumberFormatter alloc] init];
	[numberStyle setPositiveFormat:@"#,###.##"];
	[numberStyle setNegativeFormat:@"(#,###.##)"];
	
	float min = 0, max = 0;
	for(int i=0 ; i < [graphValues count] ; i++)
	{
		NSArray *c = [graphValues objectAtIndex:i];
		for(int j=1; j < [c count]; j++)
		{
			float v = [[numberStyle numberFromString:[c objectAtIndex:j]] floatValue];
			if (i == 0 && j == 1) { min = max = v; }
			else if (v < min) min = v;
			else if (v > max) max = v;
		}
	}

	float ad = (max-min)/(float)(nbYpoints-1);
	double d = 1;
	while (d < ad)
	{
		d *= 10;	
	}
	int i = 0;
	while(d > 2*ad && i < 3)
	{
		d /= 2;
		i++;
	}
	if((int)d == 0) d = 1;
	
	while ((nbYpoints-1) * d < (max-min))
	{
		nbYpoints++;	
	}
	max = min + d*(nbYpoints-1);
	int delta = (int)(max-min);
	
	int first = floor(min/d)*d;
	if(first < min) first = first +d;
	
	int firstdy = (first-min)*height/delta;
	float dx = (float)width/(float)(nbXpoints-1);
	float dy = (float)height/(float)(nbYpoints-1);
	
	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	[ret setString:[NSString stringWithFormat:@"<tr style=\"border : 0px;\"><td style=\"border : 0px;\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 %d %d\" width=\"%dpx\" height=\"%dpx\">", offset, height+arrayWidth+2*arrayLength+bottomMarge, offset, height+arrayWidth+2*arrayLength+bottomMarge]];
	[ret appendFormat:@"<g x=\"0\" y=\"0\" style=\"stroke-width:3;\">"];
	//Y axis
	
	for(int i=0 ; i < nbYpoints -1 ; i++) {
		[ret appendFormat:@"<text style=\"text-anchor: end;\" x=\"%d\" y=\"%d\" width=\"30px\">%.2f</text>", offset, ((height+2*arrayLength-firstdy)-(int)floor((float)i*dy))+15, (float)(first+(i*d))];
	}
	 
	[ret appendFormat:@"</g>"];
	 
	
	[ret appendFormat:@"</svg></td><td colspan=\"%d\" style=\"border : 0px; text-align: left; width: 100%%;\"><svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 %d %d\" width=\"100%%\" height=\"%dpx\" preserveAspectRatio=\"none\">", [[graphValues objectAtIndex:0] count]-1, width+arrayWidth+2*arrayLength, height+arrayWidth+2*arrayLength+bottomMarge, height+arrayWidth+2*arrayLength+bottomMarge];//, width+arrayWidth+2*arrayLength
	
	/* ----------------------------- */
	
	[ret appendFormat:@"<g x=\"0\" y=\"0\" style=\"stroke-width:3;\">"];
	
	[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\" />", arrayWidth, 0, arrayWidth, height+2*arrayLength];
	[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\" />", arrayWidth, 0, 0, arrayLength];
	[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\" />", arrayWidth, 0, 2*arrayWidth, arrayLength];
	
	for(int i=0 ; i < nbYpoints -1 ; i++) {
		[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\"/>", 0, (height+2*arrayLength-firstdy)-(int)floor((float)i*dy), arrayWidth, (height+2*arrayLength-firstdy)-(int)floor((float)i*dy)];
	}
	
	//X axis
	[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\" />", arrayWidth-2, height+2*arrayLength, arrayWidth+width+2*arrayLength, height+2*arrayLength];
	[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\" />", arrayWidth+width+2*arrayLength, height+2*arrayLength, arrayWidth+width+2*arrayLength-arrayLength, height+2*arrayLength-arrayWidth];
	[ret appendFormat:@"<path d=\"M%d,%d L%d,%d\" />", arrayWidth+width+2*arrayLength, height+2*arrayLength, arrayWidth+width+2*arrayLength-arrayLength, height+2*arrayLength+arrayWidth];
	//lines
	for(int i=0; i< [graphValues count] ; i++)
	{
		NSArray *current = [graphValues objectAtIndex:i];
		for(int j=1; j < [current count]-1 ; j++) {
			float v1 = [[numberStyle numberFromString:[current objectAtIndex:j]] floatValue]-min;
			float v2 = [[numberStyle numberFromString:[current objectAtIndex:j+1]] floatValue]-min;			
			float tv1 = (float)v1*(float)height/(float)delta;
			float tv2 = (float)v2*(float)height/(float)delta;		
			[ret appendFormat:@"<path d=\"M%d, %d L%d, %d\" style=\"stroke:%@; fill:none;\" />", arrayWidth+(int)floor((float)(j-1)*dx), (height+2*arrayLength)-(int)tv1, arrayWidth+(int)floor((float)j*dx), (height+2*arrayLength)-(int)tv2, [colors objectAtIndex:(i%[colors count])]];
		}
	}
	[numberStyle release];
	//end
	[ret appendFormat:@"</g></svg></td></tr>"];
	
	return ret;	
}

- (BOOL) isXML {
	return !([stype isEqualToString:@"us-types:textBlockItemType"] || [stype isEqualToString:@"xbrli:stringItemType"]);
}

- (BOOL) isHTML {
	return ([stype isEqualToString:@"us-types:textBlockItemType"] || [stype isEqualToString:@"xbrli:stringItemType"]);
}

- (BOOL) hasOtherValues {
	BOOL hasDim = NO;
	BOOL hasNoDim= NO;
	for(int i=0; i< [values count] ; i++) {
		if([[[values objectAtIndex:i] contextNames] count] == 0)
			hasNoDim = YES;
		else
			hasDim = YES;
	}
	return hasDim && hasNoDim;
}

- (NSMutableArray *) matchingSections:(NSString *)search {
	NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
	NSRange resultsRange = [title rangeOfString:search options:NSCaseInsensitiveSearch];
	if (resultsRange.length > 0) {
		[ret addObject:self];
	}
	else { 
		resultsRange = [definition rangeOfString:search options:NSCaseInsensitiveSearch];
		if (resultsRange.length > 0) {
			[ret addObject:self];
		}
	}
	for(int i = 0 ; i < [subSections count]; i++)
		[ret addObjectsFromArray:[[subSections objectAtIndex:i] matchingSections:search]];
	return ret;
}

- (void) setReport:(report *)r andParent:(section *)p {
	self.myReport = r;
	self.parentSection = p;
	//[self setMyReport:r];
	//[self setParentSection:p];
	for(int i =0; i < [subSections count] ; i++)
		[[subSections objectAtIndex:i] setReport:r andParent:self];
}

- (NSError *) loadHTMLValues {
	@synchronized(self) {
		if([HTMLValuesLoaded boolValue]) return nil;
		NSString *dirName = [[self myReport] getDirName];
		NSFileManager *fm = [NSFileManager defaultManager];
		if(![fm fileExistsAtPath:dirName]) [fm createDirectoryAtPath:dirName withIntermediateDirectories:NO attributes:nil error:nil];
		
		NSError *err = nil;
		for(int i = 0; i < [subSections count]; i++) {
			err = [[subSections objectAtIndex:i] loadHTMLValues];
			if(err != nil) return err;
		}
		if([stype isEqualToString:@"us-types:textBlockItemType"] || [stype isEqualToString:@"xbrli:stringItemType"])
		{
			for(int i = 0 ; i < [values count] ; i++)
			{
				value *v = [values objectAtIndex:i];
				NSString *filename = [[[self myReport] getDirName] stringByAppendingFormat:@"/%@", [v val]];
				
				if(![[NSFileManager defaultManager] fileExistsAtPath:filename]) {
					NSURL *curl = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@reports/%@/%@.zip&id=%@", 
																 [PropertiesGetter getBaseURL], 
																 [myReport link],
																 [v val],
																 [[UIDevice currentDevice] identifierForVendor]]];
					//NSLog(@"curl : %@", curl);
					//NSLog(@"vval : %@", [v val]);
					NSData *zipContent = [NSData dataWithContentsOfURL:curl];
					NSString *tmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat: @"%.0f.%@", [NSDate timeIntervalSinceReferenceDate] * 1000.0, @"zip"]]; 
					[zipContent writeToFile:tmpFile atomically:YES];
					ZipArchive *za = [[ZipArchive alloc] init];
					[za UnzipOpenFile:tmpFile];
					NSString *contentTmpFile = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat: @"%.0f", [NSDate timeIntervalSinceReferenceDate] * 1000.0]];
					NSLog(@"tmp file : %@", tmpFile);
					NSLog(@"content tmp file : %@", contentTmpFile);
					[za UnzipFileTo:contentTmpFile overWrite:YES];
					[za UnzipCloseFile];
					NSString *htmlFile = [contentTmpFile stringByAppendingPathComponent:[v val]];
					NSLog(@"html file : %@", htmlFile);
                    NSError *error = nil;
					NSString *newVal = [[NSString alloc] initWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:&error];
					//[content release];
					[za release];
					//[fm removeItemAtPath:tmpFile error:nil];
					//	[fm removeItemAtPath:contentTmpFile error:nil];		
	
		
					//NSString *newVal = [[NSString alloc] initWithContentsOfURL:curl encoding:NSUTF8StringEncoding error:&err];		
	
					if(err != nil) {
						ERROR(DO_NOT_NOTIFY, @"URL ERROR : %@", curl);
						[newVal release];
						[curl release];
						return err;
					}
					[curl release];
					[newVal writeToFile:filename atomically:YES encoding:NSUTF8StringEncoding error:nil];
					[newVal release];
				}
			}
		}
		HTMLValuesLoaded = [NSNumber numberWithBool:YES];
	}
	return nil;
}

- (NSString *) multiplicatorStr {
  switch ([multiplicator intValue]) {
	  case -12:
		  return @"in Trillions";
	  case -9:
		  return @"in Billions";
	  case -6:
		  return @"in Millions";
	  case -3:
		  return @"in Thousands";
	  default:
		  return @"";
  }	
}

- (void) checkMultiplicator {
	if([subSections count] == 0) return;
	int max = 0;
	for(int i=0; i < [subSections count]; i++) {
		if([[[subSections objectAtIndex:i] stype] isEqualToString:@"xbrli:monetaryItemType"]) {
			if (max < [[[subSections objectAtIndex:i] multiplicator] intValue] || max == 0)
				max = [[[subSections objectAtIndex:i] multiplicator] intValue];
		}
	}
	for(int i=0; i < [subSections count]; i++) {
		if([[[subSections objectAtIndex:i] stype] isEqualToString:@"xbrli:monetaryItemType"]) {
			if (max > [[[subSections objectAtIndex:i] multiplicator] intValue] && max != 0){
			    int diff = max - [[[subSections objectAtIndex:i] multiplicator] intValue];
				int mult = 10^diff;
				[[subSections objectAtIndex:i] setMultiplicator:[NSNumber numberWithInt:max]];
				for(int j=0; j < [[[subSections objectAtIndex:i] values] count] ; j++) {
				  	[[[[subSections objectAtIndex:i] values] objectAtIndex:j] multValue:mult];
				}
			}
		}
		[[subSections objectAtIndex:i] checkMultiplicator];
	}
}

- (NSString*) getRawCsvWithOffset:(int)offset andNbLevel:(int)nbl andSeparator:(NSString *)sep {
	
	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	if([self parentSection] != nil) {//else, it is a sub report, and the title is on the dates row
		for(int i = 0; i < offset;i++) {
			[ret appendString:sep];
		}
		[ret appendFormat:@"\"%@\"", title];
		for(int i = offset+1; i < nbl; i++) {
			[ret appendString:sep];
		}
		if(dates != nil) {
			for(int i = 0; i < [*dates count]; i++){
				[ret appendFormat:@"%@\"%f\"", sep, [self getRawValueForDate:i]];
			}
		} else {
			WARNING(NOTIFY_DEBUG, @"dates are nil in section");
		}

	}
		
	for(int i = 0; i < [subSections count]; i++) {
		section *s = [subSections objectAtIndex:i];
		
		[ret appendFormat:@"\n%@", [s getRawCsvWithOffset:(offset+1) andNbLevel:nbl andSeparator:sep]]; 
	}
	return ret;
}

/// ==== XBRL OBJ FUNCTIONS ===

- (NSString *) getRawCsvWithSeparator:(NSString *)sep {
	NSMutableString *content = [NSMutableString stringWithCapacity:0];
	if([self parentSection] == nil) {
		[content setString:title];		
	}
	else {
		[content setString:@"Section"];
	}
	
	for(int i = 0; i < [self getNbLevel]-1; i++){
		[content appendString:sep];
	}
	
	for(int i = 0; i < [*dates count]; i++){
		[content appendFormat:@"%@\"%@\"", sep, [*dates objectAtIndex:i]];
	}
	[content appendString:@"\n"];
	[content appendString:[self getRawCsvWithOffset:0 andNbLevel:[self getNbLevel] andSeparator:sep]];
	return content;
}

- (NSString *) getCsvAsString {
	return [self getRawCsvWithSeparator:@","];
}

- (NSData *) getCsv {
	 return [[self getCsvAsString] dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *) getXlsAsString {
	return [self getRawCsvWithSeparator:@"\t"];
}

- (NSData *) getXls {
	return [[self getXlsAsString] dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSArray *) getBreadCrumb {
	if(parentSection) {
		return [[parentSection getBreadCrumb] arrayByAddingObject:self];
	}
	else {
		return [[myReport getBreadCrumb] arrayByAddingObject:self];
	}
}

- (NSString *) getTitle {
	return title;
}
	
- (NSString *) getDescription {
	return definition;
}

- (ObjID *) getId {
	return [ObjID ObjIdWithObjId:[myReport getId] andItem:[[self xbrlId] stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

- (BOOL) correspondsToId:(ObjID *)i {
	return [[i reportId] isEqualToString:[[self getId] reportId]] && [[i xbrlId] isEqualToString:[[self xbrlId] stringByReplacingOccurrencesOfString:@"/" withString:@"_"]];
}

- (NSString *) getSuggestedName {
	return [NSString stringWithFormat:@"%@ - %@ - %@ - %@", [myReport getCompanyName], [myReport getReportType], [myReport getShortDate], [self title]];
}

- (NSString *) getSuggestedMailObject {
	return [NSString stringWithFormat:@"%@ - %@ - %@ : %@", [myReport getCompanyName], [myReport getReportType], [myReport getShortDate], [self title]];
}

- (NSString *) getSuggestedCsvFileName {
	return [NSString stringWithFormat:@"%@_%@_%@_%@.csv", [myReport getCompanyName], [myReport getReportType], [myReport getShortDate], [self title]];
}

- (NSString *) getSuggestedXlsFileName {
	return [NSString stringWithFormat:@"%@_%@_%@_%@.xls", [myReport getCompanyName], [myReport getReportType], [myReport getShortDate], [self title]];
}

- (BOOL) isEqual:(id)object {
	if([object isKindOfClass:[section class]]) {
		return [[object myReport] isEqual:myReport] && [[object xbrlId] isEqualToString:[self xbrlId]];
	}
	return NO;
}

- (NSArray *) sectionsCorrespondingToId:(ObjID *)objid {
	if([[objid xbrlId] isEqualToString:[[self xbrlId] stringByReplacingOccurrencesOfString:@"/" withString:@"_"]]) {return [NSArray arrayWithObject:self];}
	NSMutableArray *ret = [NSMutableArray arrayWithCapacity:0];
	for(section *s in subSections) {
		[ret addObjectsFromArray:[s sectionsCorrespondingToId:objid]];
	}
	return ret;
}
	
@end


