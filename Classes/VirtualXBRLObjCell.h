#import <UIKit/UIKit.h>
#import "XBRLObject.h"
#import "FileManager.h"

@interface VirtualXBRLObjCell : UITableViewCell {
	IBOutlet UIImageView *favoriteIcon;
	IBOutlet UIImageView *noteIcon;
	UIColor *fontColor;
}

@property (nonatomic, retain) IBOutlet UIImageView *favoriteIcon;
@property (nonatomic, retain) IBOutlet UIImageView *noteIcon;
@property (nonatomic, retain) UIColor *fontColor;

- (void) setObj:(NSObject<XBRLObject> *)obj;

@end
