#import <Foundation/Foundation.h>

@interface bookmark : NSObject {
	NSString *name;
	NSString *complement;
	NSString *directory;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *complement;
@property (nonatomic, retain) NSString *directory;

-(id) initWithName:(NSString *)n complement:(NSString *)c andDirectory:(NSString *)d;

@end
