#import <UIKit/UIKit.h>
#import "note.h"

@interface NoteCell : UITableViewCell {
	IBOutlet UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

- (void) setNote:(note *)n;

@end
