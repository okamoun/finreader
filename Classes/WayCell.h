#import <UIKit/UIKit.h>
#import "XBRLObject.h"

@interface WayCell : UITableViewCell {
	UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

- (void) setObj:(NSObject<XBRLObject> *)obj;

@end
