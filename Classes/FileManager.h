#import <Foundation/Foundation.h>
#import "PropertiesGetter.h"
#import "XBRLObject.h"

NSComparisonResult dateSort(NSDate *d1, NSDate *d2, void *context);

enum toUpdate {
	upCompanies = 1,
	upReportDesc=2,
	upInfos=4
} ;
typedef enum toUpdate toUpdate;


@class company;
@class report;
@class note;
@class bookmark;

@interface FileManager : NSObject {
}

+ (void) start;
+ (void) reload:(id)sender; //must be called times to times
+ (void) checkVersionCompatibility;

//necessary
+ (NSArray *) getAlpha;
+ (NSArray *) getCompanies;
+ (NSDictionary *) getReportsDesc;
+ (NSDictionary *) getLocalReports;
+ (NSDictionary *) getNotes;
+ (NSDictionary *) getBookmarks;
+ (NSString *) getInfosFile;
+ (NSDictionary *) getRecentReports;

+ (company *) getCompanyForId:(ObjID *)rid;
+ (report *) getReportForId:(ObjID *)rid;

//local reports management
+ (void) addLocalReport:(report *)r;
+ (void) upLocalReport:(report *)r;
+ (void) removeLocalReport:(NSDate *)key;

//bookmark management
+ (BOOL) isBookmarked:(NSObject<XBRLObject> *)obj;
+ (void) addBookmark:(bookmark *)b forObject:(NSObject<XBRLObject> *)obj;//can be used to update to
+ (void) unbookmark:(NSObject<XBRLObject> *)obj;
+ (void) unbookmarkId:(ObjID *)obj;
+ (bookmark *) getBookmarkForObj:(ObjID *)obj;

//notes management
+ (BOOL) hasNote:(NSObject<XBRLObject> *)obj;
+ (NSArray*) getNotesForObj:(NSObject<XBRLObject> *)obj;
+ (note *) addNoteNamed:(NSString *)name forObj:(NSObject<XBRLObject> *)obj;
+ (void) updateNote:(note *)n;
+ (void) removeNote:(note *)n;
+ (void) moveNote:(note *)n to:(int)position;
+ (note *) getNoteForId:(ObjID *)nid;

//update
+ (void) downloadNewVersions:(int)update;
+ (void) updateNewVersions:(int)update;
+ (void) updateCompaniesWithCallbackTarget:(id)caller andSelector:(NSString *)selectorString;
+ (void) ResetRecentReportsAndUpdateCompaniesWithCallbackTarget:(id)caller andSelector:(NSString *)selectorString;
+ (NSDate *) getLastUpdate;
+ (BOOL) isUpdating;
+ (void) setUpdating;
+ (void) setNotUpdating;

@end

