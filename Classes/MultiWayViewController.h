#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "FileManager.h"
#import "WayCell.h"
#import "report.h"
#import "ReportDownloader.h"
#import "ReportViewController.h"
#import "CanContainsNote.h"

@interface MultiWayViewController : TinUIViewController<CanContainsNote> {
	report *myReport;
	ObjID *targetId;
	NSArray *correspondingObjects;
	UITableView *tv;
	ObjID *noteToBeOpened;
	
	UILabel *loadingLabel;
	UIActivityIndicatorView *activityView;
}

@property (nonatomic, retain) report *myReport;
@property (nonatomic, retain) NSArray *correspondingObjects;
@property (nonatomic, retain) IBOutlet UITableView *tv;
@property (nonatomic, retain) ObjID *noteToBeOpened;

@property (nonatomic, retain) IBOutlet UILabel *loadingLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;

- (id) initWithReport:(report *)selectedReport andTargetId:(ObjID *)objid;
- (void) reportDownloaded:(id)sender;
- (void) reportDownloadError:(id)sender;

@end

