#import "MoreOptionsController.h"
#import "WithMoreOptions.h"
#import "WithMoreOptionsDelegate.h"
#import "AppDelegate.h"

@implementation MoreOptionsController

@synthesize parent;

@synthesize favoriteButton;
@synthesize breadCrumb;
@synthesize titleLabel;
@synthesize descLabel;
@synthesize docController;
@synthesize noteToBeOpened;


@synthesize noteTitleField;
@synthesize noteView;
@synthesize noteCancelButton;
@synthesize noteDeleteButton;
@synthesize noteSaveButton;
@synthesize currentNote;

@synthesize goButton;
@synthesize addNoteButton;
//@synthesize searchButton;
@synthesize openWithButton;
@synthesize sendByEmailButton;
@synthesize previewButton;

@synthesize tv;

@synthesize topBar;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id) initWithParent:(WithMoreOptions *)p {
    if ((self = [super initWithNibName:@"MoreOptionsController" bundle:nil])) {
        [self setParent:p];
		[self setBreadCrumb:[[parent getXbrlObject] getBreadCrumb]];
		selectedSection = -1;
		selectedNote = -1;
		self.tv = nil;
		viewAlreadyAppear = NO;
	}
    return self;
}

- (void)dealloc {
	[parent release], parent = nil;
	[favoriteButton release], favoriteButton = nil;
	[breadCrumb release], breadCrumb = nil;
	[titleLabel release], titleLabel = nil;
	[descLabel release], descLabel = nil;
	[docController release], docController = nil;
	[noteToBeOpened release], noteToBeOpened = nil;
	[noteTitleField release], noteTitleField = nil;
	[noteView release], noteView = nil;
	[noteCancelButton release], noteCancelButton = nil;
	[noteDeleteButton release], noteDeleteButton = nil;
	[noteSaveButton release], noteSaveButton = nil;
	[currentNote release], currentNote = nil;
	[goButton release], goButton = nil;
	[addNoteButton release], addNoteButton = nil;
	[openWithButton release], openWithButton = nil;
	[sendByEmailButton release], sendByEmailButton = nil;
	[previewButton release], previewButton = nil;
	[tv release], tv = nil;
	[topBar release], topBar = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/MoreOptionsIPad", [parent getStrUrl]];
}

- (int) selectedHeader {
	return 1;
}

- (NSObject<XBRLObject>*) getCurrentObj {
	if(selectedSection == -1) return nil;
	return [breadCrumb objectAtIndex:selectedSection];
}

- (void) completeReload {
	selectedNote = -1;
	selectedSection = -1;
	[self reload];
	[[self tv] reloadData];
}

- (void) reload {
	NSObject<XBRLObject> *currentObj = [self getCurrentObj];
	
	[[[self topBar] topItem] setTitle:[currentObj getTitle]];
	[descLabel setText:[currentObj getDescription]];
	
	if(selectedNote == -1) {
		if([FileManager isBookmarked:[self getCurrentObj]]) NSLog(@"SET SELECTED !!!!!");
		[favoriteButton setSelected:[FileManager isBookmarked:[self getCurrentObj]]];
		[self setCurrentNote:nil];
		[descLabel setHidden:NO];
		[noteTitleField setHidden:YES];
		[noteView setHidden:YES];
		[noteCancelButton setHidden:YES];
		[noteDeleteButton setHidden:YES];
		[noteDeleteButton setEnabled:YES];
		[noteSaveButton setHidden:YES];
		[noteTitleField resignFirstResponder];
		[noteView resignFirstResponder];
		
		BOOL enableButtons = (selectedSection != -1);
		[goButton setEnabled:enableButtons];
		[addNoteButton setEnabled:enableButtons];
		[favoriteButton setEnabled:enableButtons];
		[openWithButton setEnabled:enableButtons];
		[sendByEmailButton setEnabled:enableButtons];
		[previewButton setEnabled:enableButtons];			
		
	} else {
		[favoriteButton setSelected:NO];
		[self setCurrentNote:[[FileManager getNotesForObj:[breadCrumb objectAtIndex:selectedSection]] objectAtIndex:selectedNote]];
		[descLabel setHidden:YES];
		[noteTitleField setHidden:NO];
		[noteView setHidden:NO];
		[noteCancelButton setHidden:NO];
		[noteDeleteButton setHidden:NO];
		[noteDeleteButton setEnabled:YES];
		[noteSaveButton setHidden:NO];
		[noteTitleField setText:[currentNote name]];
		[noteView setText:[currentNote getContent]];
		
		[goButton setEnabled:NO];
		[addNoteButton setEnabled:NO];
		[favoriteButton setEnabled:NO];
		[openWithButton setEnabled:NO];
		[sendByEmailButton setEnabled:NO];
		[previewButton setEnabled:NO];
	}
}

- (void)viewDidLoad {
	/*UIImage *buttonImageSel = [[UIImage imageNamed:@"favorites.png"] retain];
	UIImage *buttonImage = [[UIImage imageNamed:@"nofavorites.png"] retain];
	[favoriteButton setImage:buttonImageSel forState:UIControlStateSelected];
	[favoriteButton setImage:buttonImage forState:UIControlStateNormal];
	*/
	
	keyboardIsVisible = NO;
    [super viewDidLoad];
	[[self noteView] setColorsAndFont];	
	//[[self topBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[self reload];	
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	//[self didRotate:nil];
	if(noteToBeOpened != nil) {
		int i;
		int nbRows = 0;
		for(i = 0; i < [breadCrumb count]; i++) {
			//TODO : on peut optimiser en tester les id des sections
			NSArray *notes = [FileManager getNotesForObj:[breadCrumb objectAtIndex:i]];
			int j;
			BOOL tobreak = NO;
			nbRows++;
			for(j = 0; j < [notes count]; j++) {
				if([[[notes objectAtIndex:j] getId] isEqual:noteToBeOpened]) {
					tobreak = YES;
					break;
				}
				nbRows++;
			}
			if(tobreak) break;
		}
		[[self tv] selectRowAtIndexPath:[NSIndexPath indexPathForRow:nbRows inSection:0] animated:YES scrollPosition:UITableViewScrollPositionBottom];
		[self tableView:[self tv] didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:nbRows inSection:0]];		
		[noteToBeOpened release], noteToBeOpened = nil;
	} else {
		if(!viewAlreadyAppear) {
			viewAlreadyAppear = YES;
			int nbRows = 0;
			for(int i=0; i < [breadCrumb count]; i++)
				nbRows += 1 + [[FileManager getNotesForObj:[breadCrumb objectAtIndex:i]] count];
			
			int nbNotesInLastSection = [[FileManager getNotesForObj:[breadCrumb lastObject]] count];
		
			[[self tv] selectRowAtIndexPath:[NSIndexPath indexPathForRow:(nbRows - nbNotesInLastSection - 1) inSection:0] animated:YES scrollPosition:UITableViewScrollPositionBottom];
			[self tableView:[self tv] didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:(nbRows - nbNotesInLastSection - 1) inSection:0]];		
		} else {
			selectedSection = -1;
			selectedNote = -1;
			[self reload];			
		}
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];	
}

- (void) viewWillAppear:(BOOL)animated {
	NSLog(@"superframe : %f;%f;%f;%f", self.view.superview.frame.origin.x, self.view.superview.frame.origin.y, self.view.superview.frame.size.width, self.view.superview.frame.size.height);
	wasLandscape = NO;
	//[self didRotate:nil];
	/*[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(didRotate:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:nil];*/
	[super viewWillAppear:animated];
	[[self tv] reloadData];
}

-(void) popViewController:(id)sender {
	int selected = selectedSection;
	if(selected < 0) selected = 0;
	if(selected >= ([parent.navigationController.viewControllers count]-1)) selected = ([parent.navigationController.viewControllers count]-2);
	
	if([[parent.navigationController.viewControllers objectAtIndex:selected+1] respondsToSelector:@selector(selectSection:)])
		[[parent.navigationController.viewControllers objectAtIndex:selected+1] performSelector:@selector(selectSection:) withObject:[self getCurrentObj]];
	
	[parent.navigationController popToViewController:[parent.navigationController.viewControllers objectAtIndex:selected+1] 
											animated:YES];
}

- (void) GoBack:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) GoTo:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
	[NSTimer scheduledTimerWithTimeInterval:0.4 
									 target:self 
								   selector:@selector(popViewController:) 
								   userInfo:nil
									repeats:NO];
}

/// SEARCH

- (void) search:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
	int selected = selectedSection;
	if(selected < 0) 
		selected = 0;
	if(selected >= ([parent.navigationController.viewControllers count]-1)) 
		selected = ([parent.navigationController.viewControllers count]-2);
	
	[NSTimer scheduledTimerWithTimeInterval:0.4 
									 target:self 
								   selector:@selector(popViewController:) 
								   userInfo:nil
									repeats:NO];
	
	[NSTimer scheduledTimerWithTimeInterval:0.8 
									 target:[parent.navigationController.viewControllers objectAtIndex:selected+1] 
								   selector:@selector(search:) 
								   userInfo:nil
									repeats:NO];
}

/// SECTIONS AND NOTES

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int total = 0;
	for(int i=0; i < [breadCrumb count]; i++)
		total += 1 + [[FileManager getNotesForObj:[breadCrumb objectAtIndex:i]] count];
	return total;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	int sec = -1;
	int not = -1;
	int i = 0, j = 0 ;
	while (true) {
		if(i==indexPath.row) {
			sec = j;
			break;
		}
		i++;
		if((i+[[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count]) > indexPath.row)
		{
			sec = j;
			not = indexPath.row - i;
			break;
		}
		i += [[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count];
		j++;
	}
	
	if(not == -1) {
		static NSString *CellIdentifierH = @"h";
		TreeCell *cell = (TreeCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifierH];
		if (cell == nil) {
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"TreeCell"  bundle:nil];
			cell = (TreeCell *)c.view;
			[c release];
		}
		UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
		if(sec%2 == 1) {
			[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
		} else {
			[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
		}
		
		[cell setBackgroundView:backgroundView];
		[backgroundView release];
		
		[cell setObj:[breadCrumb objectAtIndex:sec] andLevel:j];
		return cell;
	} else {
		static NSString *CellIdentifier = @"c";
		NoteCell *cell = (NoteCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"NoteCell"  bundle:nil];
			cell = (NoteCell *)c.view;
			[c release];
		}
		[cell setNote:[[FileManager getNotesForObj:[breadCrumb objectAtIndex:sec]] objectAtIndex:not]];
		return cell;
	}
	return nil;
}

- (void) resetTapCount {
	tapCount = 0;
}

- (IBAction) removeKeyboard:(id)sender
{
	if(keyboardIsVisible) {
		NSLog(@"remove kb");
		[noteTitleField becomeFirstResponder];
		[noteTitleField resignFirstResponder];
		//[noteView resignFirstResponder];
		
		/*@try
		{
			Class UIKeyboardImpl = NSClassFromString(@"UIKeyboardImpl");
			id activeInstance = [UIKeyboardImpl performSelector:@selector(activeInstance)];
			[activeInstance performSelector:@selector(dismissKeyboard)];
		}
		@catch (NSException *exception)
		{
			NSLog(@"%@", exception);
		}*/
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	NSLog(@"TOUCH BEGAN");
	//[self removeKeyboard:self];
}


/*-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(noteTitleField.hidden == NO) {
		[self removeKeyboard:self];
		[self saveNote:self];
	}
	return indexPath;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	int i = 0, j = 0;
	while (true) {
		if(i==indexPath.row) {
			if(++tapCount == 2) {
				if(selectedSection == j) {
					[self GoTo:self];
				} else {
					tapCount = 0;
				}
				
			} else {
				[self performSelector:@selector(resetTapCount) withObject:nil afterDelay:.4];
			}
			selectedSection = j;
			selectedNote = -1;
			break;
		}
		i++;
		if((i+[[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count]) > indexPath.row)
		{
			selectedSection = j;
			selectedNote = indexPath.row - i;
			
			///edit note
			NoteController *noteController = [[NoteController alloc] initWithObject:[self getCurrentObj] andNote:[[FileManager getNotesForObj:[self getCurrentObj]] objectAtIndex:selectedNote]];
			[self presentViewController:noteController animated:YES completion:nil];
			[noteController release];
			
			
			break;
		}
		i += [[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count];
		j++;
	}	
	[self reload];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    int i = 0, j = 0;
	while (true) {
		if(i==indexPath.row) {
			return NO;
		}
		i++;
		if((i+[[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count]) > indexPath.row)
		{
			return YES;
		}
		i += [[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count];
		j++;
	}
	ERROR(NOTIFY_DEBUG, @"Don't find wich row it is.");
    return NO;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
 return @"test";	
 }*/

/*
 - (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
 NSLog(@"did end editing...");
 }*/

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	int i = 0, j = 0;
	while (true) {
		i++;
		if((i+[[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count]) > indexPath.row)
		{
			selectedSection = j;
			selectedNote = indexPath.row - i;
			break;
		}
		i += [[FileManager getNotesForObj:[breadCrumb objectAtIndex:j]] count];
		j++;
	}
	[self setCurrentNote:[[FileManager getNotesForObj:[breadCrumb objectAtIndex:selectedSection]] objectAtIndex:selectedNote]];
	[currentNote remove];
	[FileManager removeNote:currentNote];
	selectedSection = -1;
	selectedNote = -1;
	[[self tv] reloadData];
	[self reload];
}

/// NOTES

- (void) cancelNote:(id)sender {
	if([noteTitleField isFirstResponder]) [noteTitleField resignFirstResponder];
	if([noteView isFirstResponder]) [noteView resignFirstResponder];
	
	[descLabel setHidden:NO];
	[noteTitleField setHidden:YES];
	[noteView setHidden:YES];
	[noteCancelButton setHidden:YES];
	[noteDeleteButton setHidden:YES];
	[noteDeleteButton setEnabled:YES];
	[noteSaveButton setHidden:YES];	
	[[self tv] reloadData];		
}

- (void) addNote:(id)sender {
	NoteController *noteController = [[NoteController alloc] initWithObject:[self getCurrentObj] andNote:nil];
	[self presentViewController:noteController animated:YES completion:nil];
	[noteController release];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1) {
		if(currentAction == DELETE_NOTE) {
			[currentNote remove];
			[FileManager removeNote:currentNote];
			[self cancelNote:nil];
		} else if(currentAction == UNSAVED_NOTE) {
			//so save the note
			[self saveNote:self];
		} else {
			ERROR(NOTIFY_AS_INTERNAL_ERROR, @"Unknown current acrion");
		}
	}
}

- (void) deleteNode:(id)sender {
	currentAction = DELETE_NOTE;
	UIAlertView *confirm = [[UIAlertView alloc] initWithTitle:@"Are you sure ?" message:@"You will not be able to get your note back !" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
	[confirm show];
	[confirm release];
}

- (void) saveNote:(id)sender {
	if(currentNote == nil) {
		[self setCurrentNote:[FileManager addNoteNamed:noteTitleField.text forObj:[self getCurrentObj]]];
	} else {
		[currentNote setName:[noteTitleField text]];
		[FileManager updateNote:currentNote];
	}
	[currentNote setContent:[noteView text]];
	[self cancelNote:nil];
}

/// FAVORITES

- (void) setFavorite:(id)sender {
    BookmarkController *bookmarkController = [[BookmarkController alloc] initWithObject:[self getCurrentObj] andBookmark:[FileManager getBookmarkForObj:[[self getCurrentObj] getId]]];
	[bookmarkController setModalPresentationStyle:UIModalPresentationFormSheet];	
	[self presentViewController:bookmarkController animated:YES completion:nil];
	[bookmarkController release];
}

//// EMAIL 

- (void) sendMail:(id) sender {
	if(selectedSection == -1) {
		ERROR(NOTIFY_NO_BUG, @"You have to select a section first");
		return;
	}
	if([MFMailComposeViewController canSendMail]) {
		MailController *mailController = [[MailController alloc] init];
		[mailController addAttachmentData:[[breadCrumb objectAtIndex:selectedSection] getXls] mimeType:@"application/vnd.ms-excel" fileName:[[breadCrumb objectAtIndex:selectedSection] getSuggestedXlsFileName]];
		[mailController setSubject:[[breadCrumb objectAtIndex:selectedSection] getSuggestedMailObject]];
		[mailController setMessageBody:[PropertiesGetter getStringForKey:@"mail_message"] isHTML:NO];
		[mailController setModalPresentationStyle:UIModalPresentationFormSheet];
		[self presentViewController:mailController animated:YES completion:nil];
		[mailController release];
	} else {
		ERROR(NOTIFY_NO_BUG, @"You cannot send mail, you must configure your mail box before !");
	}
	
}

/// OPEN WITH

- (void) openWith:(id)sender {
	if(selectedSection == -1) {
		ERROR(NOTIFY_NO_BUG, @"You have to select a section first");
		return;
	}
	
	NSString *filename = [NSTemporaryDirectory() stringByAppendingFormat:@"finreader_%@", [[breadCrumb objectAtIndex:selectedSection] getSuggestedXlsFileName]];
	[[[breadCrumb objectAtIndex:selectedSection] getXlsAsString] writeToFile:filename atomically:NO encoding:NSUTF8StringEncoding error:nil];
	
	[docController release];
	docController = [[UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filename]] retain];
	[docController setDelegate:self];
	
	BOOL openin = [docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:NO];
	
	if(!openin) {
		ERROR(NOTIFY_NO_BUG, @"No application found to open the document.");
	}
	
}

- (void) filePreview:(id) sender {
	
	if(selectedSection == -1) {
		ERROR(NOTIFY_NO_BUG, @"You have to select a section first");
		return;
	}
	
	
	NSString *filename = [NSTemporaryDirectory() stringByAppendingFormat:@"finreader_%@", [[breadCrumb objectAtIndex:selectedSection] getSuggestedCsvFileName]];
	[[[breadCrumb objectAtIndex:selectedSection] getCsvAsString] writeToFile:filename atomically:NO encoding:NSUTF8StringEncoding error:nil];
	
	NSLog(@"FILENAME : %@", filename);
	
	[docController release];
	docController = [[UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filename]] retain];
	[docController setDelegate:self];
	BOOL test = [docController presentPreviewAnimated:NO];
	if(!test) ERROR(NOTIFY, @"Cannot display preview");
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
	return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
	return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{
	return self.view.frame;
}

- (void)documentInteractionControllerWillBeginPreview:(UIDocumentInteractionController *)controller {
	
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller {
}

- (void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller {	
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application {
}


- (BOOL)documentInteractionController:(UIDocumentInteractionController *)controller canPerformAction:(SEL)action {
	return YES;
}

- (BOOL)documentInteractionController:(UIDocumentInteractionController *)controller  performAction:(SEL)action {
	return YES;
}

- (void) rotateWithKeyBoard:(NSNotification  *)notification {
}
	
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
	[self.noteView setNeedsDisplay]; 
}

@end
