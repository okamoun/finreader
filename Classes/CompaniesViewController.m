#import "CompaniesViewController.h"

#define PI 3.14159265

@implementation CompaniesViewController

@synthesize companies;
@synthesize searchCompanies;
@synthesize updateView;
@synthesize tv;
@synthesize adBannerView;
@synthesize contentView;

@synthesize cancelLabel;
@synthesize releaseToUpdateLabel;
@synthesize nbLabels;
@synthesize updateLabel;
@synthesize updatingLabel;
@synthesize updateIndicator;
@synthesize cancelArrow;

- (void)viewDidLoad {
	[super viewDidLoad];
	[self setCompanies:[FileManager getCompanies]];
	//------------------------------------------------------------------------------------------------------------
	searchCompanies = [[NSMutableArray alloc] initWithCapacity:0];
	//------------------------------------------------------------------------------------------------------------	
	searching = NO;
	presearch = NO;
	updating = NO;
	//------------------------------------------------------------------------------------------------------------
	UISearchBar *searchBar = [[UISearchBar alloc] init];
	[searchBar setDelegate:self];
	[searchBar sizeToFit];
	[searchBar setTintColor:[PropertiesGetter getColorForKey:@"search_bar_color"]];
	[searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
	[[self tv] setTableHeaderView:searchBar];
	[searchBar release];
	//------------------------------------------------------------------------------------------------------------
	[self setTitle:@"Companies"];
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[[self view] setAutoresizesSubviews:YES];
	[[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	//------------------------------------------------------------------------------------------------------------
#ifdef LITEVERSION
	[self createAdBannerView];
#endif
	
	[self updateNbAndUpDate];
}

- (void) updateNbAndUpDate {
	nb_companies = 0;
	nb_reports = 0;
	for(int i = 0 ; i < [companies count]; i++) {
		nb_companies += [[companies objectAtIndex:i] count];
		for(int j = 0; j < [[companies objectAtIndex:i] count]; j++) {
			nb_reports += [(company *)[[companies objectAtIndex:i] objectAtIndex:j] getNbReports];
		}
	}
	NSLog(@"NB COMPANIES : %d", nb_companies);
	NSLog(@"NB REPORTS : %d", nb_reports);
	CGAffineTransform rotate = CGAffineTransformMakeRotation(-PI/2);
	[cancelLabel setTransform:rotate];
	[nbLabels setText:[NSString stringWithFormat:@"%d companies | %d reports", nb_companies, nb_reports]];
	[updateLabel setText:[NSString stringWithFormat:@"Last update : %@", [FileManager getLastUpdate]]];
}

- (void) viewDidUnload {
	[companies release], companies = nil;
	[searchCompanies release], searchCompanies = nil;
	[super viewDidUnload];
}

- (void)dealloc {
	[companies release];
	[searchCompanies release];
	[updateView release];
	[adBannerView release];
	[contentView release];
	
	[cancelLabel release];
	[releaseToUpdateLabel release];
	[nbLabels release];
	[updateLabel release];
	[updatingLabel release];
	[updateIndicator release];
	[cancelArrow release];
    [super dealloc];
}

- (NSString *) getStrUrl {
	return @"Companies";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    [self.tv reloadData];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self.tv reloadData];
	[self fixupAdView:[UIDevice currentDevice].orientation];
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self fixupAdView:toInterfaceOrientation];
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];	
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillShow:)
	 name:UIKeyboardWillShowNotification
	 object:nil];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(keyboardWillHide:)
	 name:UIKeyboardWillHideNotification
	 object:nil];
}

- (void) viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	if(scrollView.contentOffset.y <= 0) {
		CGAffineTransform translate = CGAffineTransformMakeTranslation(0, -scrollView.contentOffset.y);
		[updateView setTransform:translate];
		if(scrollView.contentOffset.y < -64) {
			[self.releaseToUpdateLabel setHidden:NO];
			if(scrollView.contentOffset.y < -65) {
				scrollView.contentOffset = CGPointMake(0, -65);
			}
		} else {
			[self.releaseToUpdateLabel setHidden:YES];
		}
	}	
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if(self.tv.contentOffset.y < -64) {
		NSLog(@"ok...");

		@synchronized(self) {
			if(updating == YES) return;
			updating = YES;
		}
		[FileManager setUpdating];
		[self.tv setHidden:YES];
		
		[updatingLabel setHidden:NO];
		[updateIndicator setHidden:NO];
		[releaseToUpdateLabel setHidden:YES];
		[nbLabels setHidden:YES];
		[updateLabel setHidden:YES];
		[cancelLabel setHidden:YES];
		[cancelArrow setHidden:YES];
		[AppDelegate hideTabs];
		//updateView
		[FileManager updateCompaniesWithCallbackTarget:self andSelector:@"tableUpdated:"];
	} 
}

- (void) tableUpdated:(id)sender {
		
	[self setCompanies:[FileManager getCompanies]];
	[updateLabel setText:[NSString stringWithFormat:@"Last update : %@", [FileManager getLastUpdate]]];
	[self.tv reloadData];
	[updatingLabel setHidden:YES];
	[updateIndicator setHidden:YES];
	[releaseToUpdateLabel setHidden:NO];
	[nbLabels setHidden:NO];
	[updateLabel setHidden:NO];
	[cancelLabel setHidden:NO];
	[cancelArrow setHidden:NO];
	[AppDelegate displayTabs];
	[self.tv setHidden:NO];
	updating = NO;
	[FileManager setNotUpdating];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if(searching) return 1;
	return [companies count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 32;
}

- (NSInteger) tableView:(UITableView *)tableView
sectionForSectionIndexTitle:(NSString *)title
                atIndex:(NSInteger)index {
    if (index == 0) {
        [tableView setContentOffset:CGPointZero animated:NO];
        return NSNotFound;
    }
    return index-1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if(searching) return nil;
	if([[companies objectAtIndex:section] count] == 0) return nil;
	return [[FileManager getAlpha] objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if(searching) return [searchCompanies count];
	if([companies count] <= section) return 0;
	return [[companies objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	company *currentCompany;
	if (searching) 
		currentCompany = [searchCompanies objectAtIndex:indexPath.row];
	else
		currentCompany = [[companies objectAtIndex:[indexPath indexAtPosition:0]] objectAtIndex:indexPath.row];
	
	static NSString *CellIdentifier = @"simplereportcell";
    CompanyCell *cell = (CompanyCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
    	UIViewController *c = [[UIViewController alloc] initWithNibName:@"CompanyCell" bundle:nil];
		cell = (CompanyCell *)c.view;
		[c release];
	}
	[cell setCompany:currentCompany];

	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	company *currentCompany;
	if (searching)
		currentCompany = [searchCompanies objectAtIndex:indexPath.row];
	else
		currentCompany = [[companies objectAtIndex:[indexPath indexAtPosition:0]] objectAtIndex:indexPath.row];
	
	CompanyViewController *anotherViewController = [[CompanyViewController alloc] initWithCCompany:currentCompany];
	[self.navigationController pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	if(presearch) return nil;
	NSMutableArray *ret = [[NSMutableArray alloc] initWithCapacity:[[FileManager getAlpha] count]+1];
	[ret addObject:@"{search}"];
	[ret addObjectsFromArray:[FileManager getAlpha]];
	return [ret autorelease];
	return [FileManager getAlpha];
}

- (void) searchTableView:(NSString *)searchText; {
	for (int i=0; i < [companies count]; i++) {
		NSArray *a = [companies objectAtIndex:i];
		for(int j = 0; j < [a count]; j++) {
			NSString *name = [[a objectAtIndex:j] name];
			NSRange resultsRange = [name rangeOfString:searchText options:NSCaseInsensitiveSearch];			
			if (resultsRange.length > 0) {
				[searchCompanies addObject:[a objectAtIndex:j]];
			}
		}
	}
}

- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar {
	presearch = YES;
	UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																			target:self 
																			action:@selector(cancelSearch:)];
	[[self navigationItem] setLeftBarButtonItem:cancel];
	[cancel release];
	if([[theSearchBar text] length] > 0) {
		searching = YES;
		[searchCompanies removeAllObjects];
		[self searchTableView:[theSearchBar text]];
	}
	[[self tv] reloadData];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
	searching = YES;
	[searchCompanies removeAllObjects];
	if([searchText length] > 0) {
		[self searchTableView:[theSearchBar text]];
	}
	[[self tv] reloadData];
}

- (void) cancelSearch:(id)sender {
	searching = NO;
	presearch = NO;	
	[[[self tv] tableHeaderView] resignFirstResponder];
	[[self navigationItem] setLeftBarButtonItem:nil];
	[[self tv] reloadData];
}

- (void) keyboardShown:(id) sender {	
	NSDictionary* info = [sender userInfo];
	
	//TODO : no, use NSObject::respondToSelector method...
#ifdef __IPHONE_3_2
	CGRect keyboardScreenFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	UIScrollView *scrollView = (UIScrollView *) self.tv;
    CGRect viewFrame = scrollView.frame;
    CGRect keyboardFrame = [scrollView.superview convertRect:keyboardScreenFrame fromView:nil];
    hiddenRect = CGRectIntersection(viewFrame, keyboardFrame);
	
    CGRect remainder, slice;
    CGRectDivide(viewFrame, &slice, &remainder, CGRectGetHeight(hiddenRect), CGRectMaxYEdge);
    scrollView.frame = remainder;
#else
    NSValue *boundsValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
    CGRect keyboardBounds = [boundsValue CGRectValue];
	keyboardHeight = keyboardBounds.size.height-49;
	CGRect frame = self.view.frame;
	frame.size.height -= keyboardHeight;
	self.view.frame = frame;	
#endif
	keyboardShown = YES;	
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    if (keyboardShown)
        return;
	
	[NSTimer scheduledTimerWithTimeInterval:0.3 
									 target:self 
								   selector:@selector(keyboardShown:) 
								   userInfo:[aNotification userInfo]
									repeats:NO];
	
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
	if (!keyboardShown)
        return;
#ifdef __IPHONE_3_2
	UIScrollView *scrollView = (UIScrollView *) self.tv;
    CGRect viewFrame = [scrollView frame];
    scrollView.frame = CGRectUnion(viewFrame, hiddenRect);	
#else	
   	CGRect frame = self.view.frame;
	frame.size.height += keyboardHeight;
	self.view.frame = frame;	
#endif
	keyboardShown = NO;
}

//iAd methods

- (int)getBannerHeight:(UIDeviceOrientation)orientation {
	NSString *deviceType = [UIDevice currentDevice].model;
	NSLog(@"device type : %@", deviceType);
    if (UIInterfaceOrientationIsLandscape(orientation)) {
		if([deviceType isEqualToString:@"iPad"] || [deviceType isEqualToString:@"iPad Simulator"])
			return 66;
		else
			return 32;
    } else {
		if([deviceType isEqualToString:@"iPad"] || [deviceType isEqualToString:@"iPad Simulator"])
			return 66;
		else
			return 50;
    }
}

- (int)getBannerHeight {
    return [self getBannerHeight:[UIDevice currentDevice].orientation];
}

- (void)createAdBannerView {
    
    [self setAdBannerView:[[[ADBannerView alloc] initWithAdType:ADAdTypeBanner] autorelease]];
    [[self view] addSubview:[self adBannerView]];
    [[self adBannerView] setDelegate:self];
    
    /* --- old version ---
    Class classAdBannerView = NSClassFromString(@"ADBannerView");
    if (classAdBannerView != nil) {
        self.adBannerView = [[classAdBannerView alloc] initWithFrame:CGRectZero];
		NSSet *dims;
		
		NSString *strVersion = [[UIDevice currentDevice] systemVersion];
		float version = [strVersion floatValue];
		if(version > 4.1) {
     		dims = [[NSSet alloc] initWithObjects: ADBannerContentSizeIdentifierPortrait, ADBannerContentSizeIdentifierLandscape, nil];
		} else {
			dims = [[NSSet alloc] initWithObjects: ADBannerContentSizeIdentifier320x50, ADBannerContentSizeIdentifier480x32, nil];
		}
        [adBannerView setRequiredContentSizeIdentifiers:dims];
        [dims release];
		[self.view addSubview:adBannerView]; 
		[adBannerView setDelegate:self];
    } else {
		self.adBannerView = nil;
	}*/
}

- (void)fixupAdView:(UIInterfaceOrientation)toInterfaceOrientation {
	if (adBannerView != nil) {
		/*
        NSString *strVersion = [[UIDevice currentDevice] systemVersion];
		float version = [strVersion floatValue];
        if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
			if(version > 4.1) {
				[adBannerView setCurrentContentSizeIdentifier:ADBannerContentSizeIdentifierLandscape];
			} else {
				[adBannerView setCurrentContentSizeIdentifier:ADBannerContentSizeIdentifier480x32];				
			}
        } else {
			if(version > 4.1) {
				[adBannerView setCurrentContentSizeIdentifier:ADBannerContentSizeIdentifierPortrait];
			} else {
				[adBannerView setCurrentContentSizeIdentifier:ADBannerContentSizeIdentifier320x50];
			}

        }*/
		[UIView beginAnimations:nil context:nil];
		if(bannerVisible) {
			CGRect adBannerViewFrame = [adBannerView frame];
            adBannerViewFrame.origin.x = 0;
            adBannerViewFrame.origin.y = 0;
            [adBannerView setFrame:adBannerViewFrame];
            CGRect contentViewFrame = contentView.frame;
            contentViewFrame.origin.y = [self getBannerHeight:toInterfaceOrientation];
            contentViewFrame.size.height = self.view.frame.size.height - [self getBannerHeight:toInterfaceOrientation];
            contentView.frame = contentViewFrame;
		} else {
			CGRect adBannerViewFrame = [adBannerView frame];
            adBannerViewFrame.origin.x = 0;
            adBannerViewFrame.origin.y = - [self getBannerHeight:toInterfaceOrientation];
            [adBannerView setFrame:adBannerViewFrame];
            CGRect contentViewFrame = contentView.frame;
            contentViewFrame.origin.y = 0;
            contentViewFrame.size.height = self.view.frame.size.height;
            contentView.frame = contentViewFrame;
		}
		[UIView commitAnimations];
	}
}

- (void) bannerViewDidLoadAd:(ADBannerView *)banner {
	if(!bannerVisible) {
		bannerVisible = YES;
		[self fixupAdView:[UIDevice currentDevice].orientation];
	}
}

- (void) bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
	if(bannerVisible) {
		bannerVisible = NO;
		[self fixupAdView:[UIDevice currentDevice].orientation];
	}
}

@end

