#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "MailController.h"
#import "PropertiesGetter.h"
#import "XBRLObject.h"
#import "FileManager.h"
#import "bookmark.h"
#import "BookmarkController.h"
#import "NoteController.h"
#import "NoteCell.h"
#import "WithMoreOptionsDelegate.h"
#import "ErrorManager.h"
#import "TreeCell.h"
#import "NoteView.h"
#import "CanBeReload.h"

@class WithMoreOptions;

@interface MoreOptionsController_ipad : TinUIViewController<UIScrollViewDelegate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate, UITableViewDelegate, CanBeReload> {
	WithMoreOptions *parent;
	NSArray *breadCrumb;	
	UILabel *titleLabel;
	UILabel *descLabel;	

	UIButton *goButton;
	UIButton *addNoteButton;
	UIButton *favoriteButton;
	//IBOutlet UIButton *searchButton;
	UIButton *openWithButton;
	UIButton *sendByEmailButton;
	UIButton *previewButton;
	
	UIDocumentInteractionController *docController;
	
	IBOutlet UITableView *tv;
		
	//MailController *mailController;
	//BookmarkController *bookmarkController;
	//NoteController *noteController;
	//UIDocumentInteractionController	*docController;
	
	UITextField *textField;
	
	
	IBOutlet UITextField *noteTitleField;
	IBOutlet NoteView *noteView;
	
	IBOutlet UIButton *noteCancelButton;
	IBOutlet UIButton *noteDeleteButton;
	IBOutlet UIButton *noteSaveButton;
	note *currentNote;

	ObjID *noteToBeOpened;
	
	BOOL wasLandscape;
	
	int selectedSection;
	int selectedNote;
	
	IBOutlet UINavigationBar *topBar;
	
	CGRect fullFrame;
	
	BOOL editingNote;
	
#define DELETE_NOTE 0
#define UNSAVED_NOTE 1	
	NSInteger currentAction;
	
	//to detect double tap on tableView
	int tapCount;
	BOOL keyboardIsVisible;
	
	BOOL viewAlreadyAppear;	
}

@property (nonatomic, retain) WithMoreOptions *parent;
//@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
//@property (nonatomic, retain) IBOutlet UIToolbar *toolbar;
@property (nonatomic, retain) IBOutlet UINavigationBar *topBar;

@property (nonatomic, retain) IBOutlet UITableView *tv;

@property (nonatomic, retain) IBOutlet UIButton *goButton;
@property (nonatomic, retain) IBOutlet UIButton *addNoteButton;
@property (nonatomic, retain) IBOutlet UIButton *favoriteButton;
//@property (nonatomic, retain) IBOutlet UIButton *searchButton;
@property (nonatomic, retain) IBOutlet UIButton *openWithButton;
@property (nonatomic, retain) IBOutlet UIButton *sendByEmailButton;
@property (nonatomic, retain) IBOutlet UIButton *previewButton;

@property (nonatomic, retain) NSArray *breadCrumb;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *descLabel;

@property (nonatomic, retain) UIDocumentInteractionController *docController;

@property (nonatomic, retain) ObjID *noteToBeOpened;

@property (nonatomic, retain) IBOutlet UITextField *noteTitleField;
@property (nonatomic, retain) IBOutlet NoteView *noteView;
@property (nonatomic, retain) IBOutlet UIButton *noteCancelButton;
@property (nonatomic, retain) IBOutlet UIButton *noteDeleteButton;
@property (nonatomic, retain) IBOutlet UIButton *noteSaveButton;
@property (nonatomic, retain) note *currentNote;


- (IBAction) removeKeyboard:(id)sender;

- (id) initWithParent:(WithMoreOptions *)p;

- (void) filePreview:(id) sender;
- (void) sendMail:(id) sender;
- (void) openWith:(id)sender;
- (void) GoBack:(id)sender;
- (void) GoTo:(id)sender;
- (void) addNote:(id)sender;
- (void) search:(id)sender;
- (void) reload;
- (void) setFavorite:(id)sender;
- (void) cancelNote:(id)sender;
- (void) deleteNode:(id)sender;
- (void) saveNote:(id)sender;

- (void) rotateWithKeyBoard:(NSNotification  *)notification;

@end
