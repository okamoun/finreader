#import <UIKit/UIKit.h>
#import "FlowView.h"
#import "ReportGroupViewController.h"
#import "company.h"
#import "WithMoreOptions.h"
#import "CompanySearchViewController.h"
#import "CanContainsNote.h"

@interface CompanyViewController : WithMoreOptions <FlowViewDelegate, WithMoreOptionsDelegate, CanContainsNote>
{
	company *comp;
	NSMutableDictionary *subViews;
	ObjID *noteToBeOpened;
	report *reportToGo;
	BOOL firstToGo;
}

@property (nonatomic, retain) company *comp;
@property (nonatomic, retain) NSMutableDictionary *subViews;
@property (nonatomic, retain) ObjID *noteToBeOpened;
@property (nonatomic, retain) report *reportToGo;
@property (nonatomic) BOOL firstToGo;

- (id) initWithCCompany:(company *)ncompany;
- (void) redraw:(id)sender;

@end

