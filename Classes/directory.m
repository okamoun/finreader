#import "directory.h"

@implementation directory

@synthesize title;

- (id) initWithTitle:(NSString *)ititle {
	if(self= [super init]) {
		[self setTitle:ititle];
	}
	return self;
}

- (void) dealloc {
	[title release];
	[super dealloc];
}

@end
