#import "VirtualSectionCell.h"

@implementation VirtualSectionCell

@synthesize titleLabel;
@synthesize valueLabel;
@synthesize valueLabel2;
@synthesize valueLabel3;
@synthesize web;

- (void) dealloc {
	[titleLabel release];
	[valueLabel release];
	[valueLabel2 release];
	[valueLabel3 release];
	[web release];
	[super dealloc];
}

- (void) setSection:(section *)s {
	[titleLabel setText:[s title]];
	if([[s parentSection] hasOnlyOneValuePerRow] && ![s isHTML]) {
		if([[s values] count] > 0) {
			[valueLabel setText:[[[s values] objectAtIndex:0] val]];
		} else {
			[valueLabel setText:@""];	
		}
		[valueLabel2 setText:@""];
		[valueLabel3 setText:@""];
	}
	else if([[s values] count] > 0  && ![s isHTML]) {
		[valueLabel setText:[s getValueForDate:0]];
		[valueLabel2 setText:[s getValueForDate:1]];
		[valueLabel3 setText:[s getValueForDate:2]];
	} else {
		if([[s values] count] > 0) [web setHidden:NO];
		[valueLabel setText:@""];
		[valueLabel2 setText:@""];
		[valueLabel3 setText:@""];
	}
	if(fontColor != nil) {
		[titleLabel setTextColor:fontColor];
		[valueLabel setTextColor:fontColor];
		[valueLabel2 setTextColor:fontColor];
		[valueLabel3 setTextColor:fontColor];
	}
	[super setObj:s];
}

@end
