#import <UIKit/UIKit.h>
#import "directory.h"

@interface DirectoryCell : UITableViewCell {
	UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

- (void) setDirectory:(directory *)d;

@end
