#import "HTMLViewController.h"


@implementation HTMLViewController

@synthesize html;
@synthesize webView;
@synthesize isXml;

- (id) initWithSection:(section *)s {
	if(self = [super initWithNibName:@"HTMLViewController" bundle:nil]) {
		[self setTitle:[s title]];
		[self setIsXml:[s isXML]];
		[self setHtml:[s html]];
		[[self webView] setScalesPageToFit:YES];
	}
	return self;
}

- (void)dealloc {
	[html release], html = nil;
	[webView release], webView = nil;
    [super dealloc];
}

- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/HTML", [self title]];
}

- (void)viewDidLoad {
	if(isXml)
		[webView loadData:[html dataUsingEncoding:NSUTF8StringEncoding] MIMEType:@"application/xhtml+xml" textEncodingName:@"UTF-8" baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
	else //report HTML
		[webView loadHTMLString:html baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
	[super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void) removeFromSuperview {
	[[self view] removeFromSuperview];
}

@end
