#import "DirectoryCell.h"

@implementation DirectoryCell

@synthesize label;

- (void)dealloc {
	[label release];
    [super dealloc];
}

- (void) setDirectory:(directory *)d {
	[label setText:[d title]];
}

@end
