#import "VirtualReportCell.h"

@interface ReportCell : VirtualReportCell {
	UILabel *typeLabel;
    UILabel *companyLabel;
}

@property (nonatomic, retain) IBOutlet UILabel *typeLabel;
@property (nonatomic, retain) IBOutlet UILabel *companyLabel;

- (void) setReport:(report *)r;

@end
