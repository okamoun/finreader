#import <UIKit/UIKit.h>
#import "XMLToSectionsParser.h"
#import "PropertiesGetter.h"
#import "HTMLViewController.h"
#import "FirstLevelCell.h"
#import "SecondLevelCell.h"
#import "SearchViewController.h"
#import "WithMoreOptions.h"
#import "WithMoreOptionsDelegate.h"
#import "CanContainsNote.h"

@interface ReportViewController : WithMoreOptions<UIActionSheetDelegate, WithMoreOptionsDelegate, CanContainsNote> {
	NSMutableArray *sections;
    HTMLViewController *HTMLView;
	UIViewController *separator;
	UITableView *tv;
	float HTMLHeight;
	float SeparatorHeight;
	
	NSIndexPath *activateIndexPath;
	
	BOOL displayTabBar;
	
	NSString *reportTitle;
	
	section *sectionToGo;
	BOOL firstToGo;
	
	BOOL wasLandscape;
	
	UIView *header;
	ObjID *noteToBeOpened;
	section *parent;
	NSNumber *gotorow;
}

@property (nonatomic, retain) section *parent;
@property (nonatomic, retain) NSNumber *gotorow;
@property (nonatomic, retain) NSMutableArray *sections;
@property (nonatomic, retain) HTMLViewController *HTMLView;
@property (nonatomic, retain) UIViewController *separator;
@property (nonatomic, retain) IBOutlet UITableView *tv;
@property (nonatomic, retain) NSIndexPath *activateIndexPath;
@property (nonatomic, retain) NSString *companyName;
@property (nonatomic, retain) NSString *reportTitle;
@property (nonatomic, retain) section *sectionToGo;
@property (nonatomic) BOOL firstToGo;
@property (nonatomic, retain) UIView *header;
@property (nonatomic, retain) ObjID *noteToBeOpened;

- (id) initWithSection:(section *)parentSection	gotoRow:(NSNumber *)r;
- (void) HTMLView:(section *)s;
- (void) closeHTML:(id)sender;
- (void) maximize:(id)sender;
- (void) minimize:(id)sender;
- (void) search:(id)sender;
- (void) manageTabBar;
- (void) performSearch:(NSTimer *)timer;
- (void) push:(NSTimer *)timer;

- (void) selectSection:(id)s;

@end
