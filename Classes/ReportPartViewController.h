#import <UIKit/UIKit.h>
#import "TinUIViewController.h"
#import "report.h"
#import "reportGroup.h"
#import "ReportViewController.h"
#import "PropertiesGetter.h"
#import "FileManager.h"
#import "SelectableHeader.h"

@interface ReportPartViewController : TinUIViewController<UITableViewDelegate,SelectableHeaderDelegate> {
	NSString *companyName;
	TinUIViewController *parent;
	UITableView *tv;
	section *sec;		
	UIView *header;
}

@property (nonatomic, retain) TinUIViewController *parent;
@property (nonatomic, retain) IBOutlet UITableView *tv;
@property (nonatomic, retain) section *sec;
@property (nonatomic, retain) NSString *companyName;
@property (nonatomic, retain) UIView *header;

- (id) initWithSection:(section *)nsec andParent:(TinUIViewController *)p;

@end
