#import "FavoritesViewController.h"

@implementation FavoritesViewController

- (void)dealloc {
    [super dealloc];
}

- (NSString *) getStrUrl {
	return @"Favorites";
}

- (void)viewDidLoad {
    [[self navigationItem] setRightBarButtonItem:[self editButtonItem]];
	[self setTitle:@"Favorites"];
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
	[[self tableView] reloadData];
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/// TABLE VIEW

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[FileManager getBookmarks] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"c";
    BookmarkCell *cell = (BookmarkCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
    	UIViewController *c = [[UIViewController alloc] initWithNibName:@"BookmarkCell"  bundle:nil];
		cell = (BookmarkCell *)c.view;
		[c release];
	}
	
	ObjID *key = [[[FileManager getBookmarks] allKeys] objectAtIndex:indexPath.row];
	bookmark *bm = [[FileManager getBookmarks] objectForKey:key];
	[cell setBookmark:bm];
	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero ];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ObjID *key = [[[FileManager getBookmarks] allKeys] objectAtIndex:indexPath.row];
	if([key reportId] != nil) { // a report
		report *currentReport = [FileManager getReportForId:key];
		if(![currentReport isAvailable]) 
		{
			UIAlertView *unaivalableAlert = [[UIAlertView alloc] initWithTitle:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_title"]  
																	   message:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_message"]
																	  delegate:self 
															 cancelButtonTitle:@"Cancel"
															 otherButtonTitles:@"Buy", nil];
			[unaivalableAlert show];
			[unaivalableAlert release];
			return nil;
		}
	}
	return indexPath;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1)
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=376803429&mt=8"]];
}

/// TABLE VIEW EDITION

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		[FileManager unbookmarkId:[[[FileManager getBookmarks] allKeys] objectAtIndex:indexPath.row]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	//TODO 
}

/// TABLE VIEW SELECTION

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	ObjID *key = [[[FileManager getBookmarks] allKeys] objectAtIndex:indexPath.row];
	UIViewController *anotherViewController;
	
	if([key xbrlId] != nil) { //item in a report
		report *selectedReport = [FileManager getReportForId:key];
		anotherViewController = [[MultiWayViewController alloc] initWithReport:selectedReport andTargetId:key];
	}
	else if([key reportId] != nil) { // a report
		report *selectedReport = [FileManager getReportForId:key];
		anotherViewController = [[CompleteReportViewController alloc] initWithReport:selectedReport];
	} else { // a company
		company *selectedCompany = [FileManager getCompanyForId:key];
		anotherViewController = [[CompanyViewController alloc] initWithCCompany:selectedCompany];
	}
	[[self navigationController] pushViewController:anotherViewController animated:YES];
	[anotherViewController release];
}

@end

