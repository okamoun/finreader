#import "VirtualSectionCell.h"

@interface SecondLevelCell : VirtualSectionCell {
	UILabel *star;
	UILabel *star2;
	UILabel *star3;
	UILabel *pointLabel;
	UIImageView *multivalue;
}

@property (nonatomic, retain) IBOutlet UILabel *star;
@property (nonatomic, retain) IBOutlet UILabel *star2;
@property (nonatomic, retain) IBOutlet UILabel *star3;
@property (nonatomic, retain) IBOutlet UILabel *pointLabel;
@property (nonatomic, retain) IBOutlet UIImageView *multivalue; 

- (void) setSection:(section *)s;

@end
