#import <Foundation/Foundation.h>
#import "PropertiesGetter.h"
#import "FileManager.h"
#import "XBRLObject.h"
#import "section.h"
#import "reportGroup.h"

@class company;
@class reportGroup;

typedef enum downloadedTypes {
	not_downloaded=0,
	partially_downloaded=1,
	totally_downloaded=2
} Downloaded;

@interface report : NSObject<XBRLObject> {
	NSDate *date;
	NSString *link;/// the link is like an id, it is : SOURCE/COMPANY_ID/REPORT_ID
	NSNumber *available;
	NSNumber *amend;
	NSNumber *aggregate;
	NSMutableArray *sections;
	
	
	reportGroup *myGroup;
	company *myCompany;
}

@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *link;
@property (nonatomic, retain) NSNumber *available;
@property (nonatomic, retain) NSNumber *amend;
@property (nonatomic, retain) NSNumber *aggregate;
@property (nonatomic, retain) NSMutableArray *sections;

@property (nonatomic, retain) company *myCompany;
@property (nonatomic, retain) reportGroup *myGroup;

//xml parsing methods
- (void) setDateFromXML:(NSString *)dstr;
- (void) setAvailableFromXML:(NSString *)bstr;

//accessors
- (BOOL) isAvailable;
- (BOOL) isAmend;
- (BOOL) isAggregate;
- (NSString *) getCompanyName;
- (NSString *) getReportType;
- (NSString *) getDate;
- (NSString *) getShortDate;//same as getDate but in a shorter format

- (NSString *) getFileName;
- (NSString *) getPartFileName;
- (NSString *) getDirName;
+ (NSString *) getFileNameForId:(ObjID *)objId;
+ (NSString *) getPartFileNameForId:(ObjID *)objId;
+ (NSString *) getDirNameForId:(ObjID *)objId;

- (Downloaded) getDownloaded;

- (NSArray *) sectionsCorrespondingToId:(ObjID *)objid;

- (NSString *) getRawCsvWithSeparator:(NSString *)sep;

- (void) unload;

@end
