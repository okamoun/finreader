#import "VirtualXBRLObjCell.h"
#import "company.h"

@interface CompanyCell : VirtualXBRLObjCell {
	UILabel *label;
}

@property (nonatomic, retain) IBOutlet UILabel *label;

- (void) setCompany:(company *)c;

@end
