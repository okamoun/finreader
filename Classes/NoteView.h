#import <UIKit/UIKit.h>

@interface NoteView : UITextView<UIScrollViewDelegate> {
}

- (void) setColorsAndFont;

@end
