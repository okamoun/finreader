#import <UIKit/UIKit.h>

@interface HeaderCell : UIView {
	UILabel *label;
	UIColor *fontColor;
}

@property (nonatomic, retain) IBOutlet UILabel *label;
@property (nonatomic, retain) UIColor *fontColor;

- (void) setTitle:(NSString *)title;

@end
