#import "ErrorManager.h"
#import "AppDelegate.h"

@implementation ErrorManager

static NSDateFormatter *dateFormatter;

static NSString *currentURL;
static UIViewController *currentViewController;

+ (void) initialize {
	dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:SS"];
}

+ (void) setCurrentController:(UIViewController *)vc {
	currentViewController = vc;
}

+ (void) setURL:(NSString *)url {
	[currentURL release];
	currentURL = [url retain];
}

+ (void) errorWithMessage:(NSString *)message level:(int)level where:(NSString *)where andNotify:(int)notify andError:(NSError *)err{		
	
	NSLog(@"[%@]%@ (%@) : %@", 
		  [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:0]],
		  level==3 ? @"FATAL ERROR" : (level == 2 ? @"ERROR" : (level == 1 ? @"WARNING" : @"INFO")),
		  where,
		  message
		  );
	
	if(err != nil) {
		NSLog(@"ERROR : %@", err);
		NSLog(@"CODE : %d", [err code]);
		NSLog(@"DOMAIN : %@", [err domain]);
		NSLog(@"USER_INFO : %@", [err userInfo]);
		NSLog(@"DESCRIPTION: %@", [err localizedDescription]);
		NSLog(@"RECOVERY_OPTIONS: %@", [err localizedRecoveryOptions]);
		NSLog(@"RECOVERY_SUGGESTION: %@", [err localizedRecoverySuggestion]);
		NSLog(@"FAILURE_REASON: %@", [err localizedFailureReason]);
	}
	
	if(notify != DO_NOT_NOTIFY) 
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"An error is occured"  
														message:(notify == NOTIFY_AS_INTERNAL_ERROR) ? @"Internal error" : ((err != nil) ? [NSString stringWithFormat:@"%@ | Error code : %@:%d (%@) | Suggestion : %@", message, [err domain], [err code], [err localizedDescription], [err localizedRecoverySuggestion]] : message)
													   delegate:self
											  cancelButtonTitle:@"Cancel"
											  otherButtonTitles:(notify == NOTIFY_NO_BUG) ? nil : @"Report bug", nil];
			[alert show];
			[alert release];
	}
	
}

+ (void) reportBug {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
														 NSUserDomainMask, YES);
	
	NSLog(@"uniqueIdentifier: %@", [[UIDevice currentDevice] identifierForVendor]);
	NSLog(@"name: %@", [[UIDevice currentDevice] name]);
	NSLog(@"systemName: %@", [[UIDevice currentDevice] systemName]);
	NSLog(@"systemVersion: %@", [[UIDevice currentDevice] systemVersion]);
	NSLog(@"model: %@", [[UIDevice currentDevice] model]);
	NSLog(@"localizedModel: %@", [[UIDevice currentDevice] localizedModel]);
	NSLog(@"userInterfaceIdiom: %d", [[UIDevice currentDevice] userInterfaceIdiom]);
	NSLog(@"currentURL : %@", currentURL	);

	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
	
	if([MFMailComposeViewController canSendMail]) {
		MailController *mailController = [[MailController alloc] init];
		[mailController setToRecipients:[NSArray arrayWithObject:@"support@finreader.com"]];
		[mailController addAttachmentData:[NSData dataWithContentsOfFile:logPath] mimeType:@"text" fileName:@"logfile.log"];
		[mailController setSubject:@"Bug report"];
		[mailController setMessageBody:@"Please, discribe here how to reproduce the bug." isHTML:NO];
		[currentViewController presentViewController:mailController animated:YES completion:nil];
		[mailController release];
	} else {
		ERROR(NOTIFY_NO_BUG, @"You cannot send mail, you must configure your mail box before ! Try to reproduce the bug then. Thnk you.");
	}
	
	//so first, we have to stock the messages, and release them when the user send the report, 
	//or when there are some memory leaks...
	
	//then, send a mail so the user can complete the report...
}

+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1) {
		[self reportBug];
	}
}

@end
