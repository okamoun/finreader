#import <UIKit/UIKit.h>
#import "VirtualReportCell.h"

@interface TreeCell : VirtualXBRLObjCell {
	UILabel *title;
}

@property (nonatomic, retain) IBOutlet UILabel *title;

- (void) setObj:(NSObject<XBRLObject> *)obj andLevel:(int)l;

@end
