#import "Object.h"

@protocol XBRLObject<Object>
- (NSString *) getTitle;
- (NSString *) getDescription;
- (NSArray *) getBreadCrumb;
- (NSData *) getCsv;
- (NSString *) getCsvAsString;
- (NSData *) getXls;
- (NSString *) getXlsAsString;
- (NSString *) getSuggestedName;
- (NSString *) getSuggestedMailObject;
- (NSString *) getSuggestedCsvFileName;
- (NSString *) getSuggestedXlsFileName;
@end
