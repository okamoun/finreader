#import "TreeCell.h"

#define TREE_CELL_OFFSET 10

@implementation TreeCell

@synthesize title;

- (void)dealloc {
	[title release];
    [super dealloc];
}

- (void) setObj:(NSObject<XBRLObject> *)obj andLevel:(int)l; {
	[self.title setText:[obj getTitle]];
	[self.title setFrame:CGRectMake(self.title.frame.origin.x+(l*TREE_CELL_OFFSET), 
									self.title.frame.origin.y, 
									self.title.frame.size.width-(2*l*TREE_CELL_OFFSET), 
									self.title.frame.size.height)];
	[super setObj:obj];
}

@end
