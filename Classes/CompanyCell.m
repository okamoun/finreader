#import "CompanyCell.h"

@implementation CompanyCell

@synthesize label;

- (void) dealloc {
	[label release];
	[super dealloc];
}

- (void) setCompany:(company *)c {
	[label setText:[c name]];
	if(fontColor != nil) {
		[label setTextColor:fontColor];
	}
	[super setObj:c];
}

@end
