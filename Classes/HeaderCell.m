#import "HeaderCell.h"

@implementation HeaderCell

@synthesize label;
@synthesize fontColor;

- (void) dealloc {
	[label release];
	[fontColor release];
	[super dealloc];
}

- (void) setTitle:(NSString *)title {
	[label setText:title];
	if(fontColor != nil) {
		[label setTextColor:fontColor];
	}
}

@end
