#import "WayCell.h"

@implementation WayCell

@synthesize label;

- (void)dealloc {
	[label release];
    [super dealloc];
}

- (void) setObj:(NSObject<XBRLObject> *)obj {
	NSArray *breadCrumbs = [obj getBreadCrumb];
	NSMutableString *ret = [NSMutableString stringWithCapacity:0];
	[ret setString:[[breadCrumbs objectAtIndex:0] getTitle]];
	for (int i = 1 ; i < [breadCrumbs count] ; i++) {
		[ret appendFormat:@" > %@", [[breadCrumbs objectAtIndex:i] getTitle]];
	}
	[label setText:ret];
}

@end
