#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ShadowedView : UIView {
	CAGradientLayer *topShadow;
}

@property (nonatomic, retain) CAGradientLayer *topShadow;

@end
