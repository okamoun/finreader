#import <Foundation/Foundation.h>
#import "ErrorManager.h"
#import "Object.h"
#import "ObjID.h"
#import "report.h"

/*
 Please note that the note, unlike a bookmark, is an object, because we can have several notes for an xbrlObject !
*/

@interface note : NSObject<Object> {
	NSString *name;
	NSNumber *nbId;
	ObjID *parentId;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *nbId;
@property (nonatomic, retain) ObjID *parentId;

- (id) initWithName:(NSString *)n parentId:(ObjID *)pi andNbId:(NSNumber *)ni;

- (NSString *) getContent;
- (void) setContent:(NSString *)content;
- (void) remove;

@end
