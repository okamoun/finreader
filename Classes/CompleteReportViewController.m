#import "CompleteReportViewController.h"

@implementation CompleteReportViewController

@synthesize rep;
@synthesize sections;
@synthesize subViews;
@synthesize sectionToGo;
@synthesize firstToGo;
@synthesize noteToBeOpened;

@synthesize loadingLabel;
@synthesize activityView;

- (id) initWithReport:(report *)selectedReport {
	
	if(self=[super initWithNibName:@"CompleteReportViewController" bundle:nil]) {
		[self setRep:selectedReport];
		
		//[self setSections:nil];
		//[self setSectionToGo:nil];
		self.sections = nil;
		self.sectionToGo = nil;
		
		self.firstToGo = NO;
		[self setTitle:[rep getCompanyName]];
		UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
																 style:UIBarButtonItemStyleBordered
																target:nil
																action:nil];
		[[self navigationItem] setBackBarButtonItem:back];
		[back release];
		loadingState = 0;
	}
	return self;
}

- (void)viewDidLoad {
	NSLog(@"VIEW DID LOAD");
	self.subViews = [[[NSMutableDictionary alloc] initWithCapacity:0] autorelease];
	[super removePlusButton];
	if([rep isAvailable]) {
		if([rep sections] == nil) {
			[ReportDownloader loadReport:rep withCallbackTarget:self andSelector:@"reportDownloaded:" andErrorSelector:@"reportDownloadError:" andStateSelector:@"setLoadingState:"];
		}
		else {
			[self reportDownloaded:self];
		}
	} else {
		UIViewController *unavilable = [[UIViewController alloc] initWithNibName:@"Unavailable" bundle:nil];
		[[unavilable view] setFrame:self.view.bounds];
		[[self view] addSubview:unavilable.view];
		[[unavilable view] setNeedsDisplay];
		[unavilable release];
	}
    [super viewDidLoad];
}

- (void)viewDidUnload {
	[subViews release];
	[rep unload];
	[super viewDidUnload];
}


- (void)dealloc 
{
	[rep release], rep = nil;
	[subViews release], subViews = nil;
	[sections release], sections = nil;
	[sectionToGo release], sectionToGo = nil;
	[loadingLabel release], loadingLabel = nil;
	[activityView release], activityView = nil;
    [super dealloc];
}

- (void) reportDownloadError:(id)objArray {
	[activityView stopAnimating];
	[loadingLabel setText:@"Error"];
	
	NSError *err = [objArray objectAtIndex:1];
	if([err code] == -1009) {
		ERROR(NOTIFY_NO_BUG, @"No Internet connection");
	}
	else {
		ERROR(NOTIFY, @"Impossible to get the report %@, error code : %d", [objArray objectAtIndex:0], [err code]);
	}	
}

- (void) reportDownloaded:(id)sender {
	NSLog(@"REPORT DOWNLOADED");
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[self setSections:[rep sections]];
	FlowView *fv = (FlowView *)self.view;
	[fv initDraw];
	[super addPlusButton];
	[pool release];
	NSLog(@"ok done");
}

- (void) push:(NSTimer *)timer {
	[[self navigationController] pushViewController:[[timer userInfo] objectAtIndex:0] animated:YES];
	[[self view] setUserInteractionEnabled:YES];
}

- (void)didRotate:(NSNotification  *)notification {
	if([self view] != nil) {
		FlowView *fv = (FlowView *)self.view;
		[fv draw];
	}
}

- (void) performSearch:(NSTimer *)timer {
	section *s = sectionToGo;
	while ([s parentSection] != nil) {
		s = [s parentSection];
	}

	for(int i = 0; i < [sections count]; i++) {
		if(s == [sections objectAtIndex:i]) {
			FlowView *fv = (FlowView *)self.view;
			[fv setMid:i];
			[fv draw];

			if([[s subSections] count] == 1) {
				HTMLViewController *anotherViewController = [[HTMLViewController alloc] initWithSection:[[s subSections] objectAtIndex:0]];
				[NSTimer scheduledTimerWithTimeInterval:0.4 
												 target:self 
											   selector:@selector(push:) 
											   userInfo:[NSArray arrayWithObject:anotherViewController]
												repeats:NO];
				[anotherViewController release];
			} else {
				ReportViewController *anotherViewController = [[ReportViewController alloc] initWithSection:s gotoRow:nil];
				if(noteToBeOpened != nil) {
					[anotherViewController setNoteToBeOpened:noteToBeOpened];
					[noteToBeOpened release], noteToBeOpened = nil;
				}
				[anotherViewController setSectionToGo:sectionToGo];
				[NSTimer scheduledTimerWithTimeInterval:0.4 
												 target:self 
											   selector:@selector(push:) 
											   userInfo:[NSArray arrayWithObject:anotherViewController]
												repeats:NO];
				[anotherViewController release];
			}
			break;
		}
	}
	[sectionToGo release], sectionToGo = nil;
}

- (void) redraw:(id)sender {
	if(self.view) {
		FlowView *fv = (FlowView *)self.view;
		[fv initDraw];
	}
}

- (void)viewDidAppear:(BOOL)animated {
	NSLog(@"VIEW DID APPEAR");
	if(sectionToGo != nil)
	{
		[self.view setUserInteractionEnabled:NO];
		NSTimeInterval ti;
		if(firstToGo) { ti = 0.8; firstToGo = NO ; }
		else ti = 0.4;
		
		[NSTimer scheduledTimerWithTimeInterval:ti 
										 target:self 
									   selector:@selector(performSearch:) 
									   userInfo:nil
										repeats:NO];
		//[t fire];
	} else if(noteToBeOpened != nil) {
		[super displayNote:noteToBeOpened];
		[noteToBeOpened release], noteToBeOpened = nil;
	}

	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(didRotate:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:nil];
	
	FlowView *fv = (FlowView *)self.view;
	[fv draw];
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
	NSLog(@"VIEW WILL APPEAR");
	//FlowView *fv = (FlowView *)self.view;
	//[fv draw];
	[super viewWillAppear:animated];
}
	
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	//FlowView *fv = (FlowView *)self.view;
	//[fv draw];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	return YES;
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

- (int)flowNumberOfViews:(FlowView *)view
{
	return [sections count];
}

- (UIView *)flow:(FlowView *)view number:(int)i
{
	if(i >= [sections count]) { return nil; }
	ReportPartViewController *c = [subViews objectForKey:[NSNumber numberWithInt:i]];
	UIView *ret;
	if(c == nil)
	{
		c = [[ReportPartViewController alloc] initWithSection:[sections objectAtIndex:i] andParent:self];
		[subViews setObject:c forKey:[NSNumber numberWithInt:i]];
		ret = [[c view] retain];
		[c release];
	} else {
		ret = [[c view] retain];
	}
	return [ret autorelease];
}

- (UIViewController<SelectableHeaderDelegate> *)flow:(FlowView *)view controllerAtIdx:(int)i  {
	if(i >= [sections count]) { return nil; }
	ReportPartViewController *c = [[subViews objectForKey:[NSNumber numberWithInt:i]] retain];
	if(c == nil)
	{
		c = [[ReportPartViewController alloc] initWithSection:[sections objectAtIndex:i] 
													andParent:self];
		[subViews setObject:c forKey:[NSNumber numberWithInt:i]];
	} /*else {
		[c retain];
	}*/
	return [c autorelease];
	//return [subViews objectForKey:[NSNumber numberWithInt:i]];
}

- (void) search:(id)sender {
	SearchViewController *anotherViewController = [[SearchViewController alloc] initWithSections:sections];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[[self navigationController] view] cache:YES];
	[self.navigationController pushViewController:anotherViewController animated:NO];
	[UIView commitAnimations];
	[anotherViewController release];
}

- (NSObject<XBRLObject> *) getXbrlObject {
	return (NSObject<XBRLObject> *)rep;
}

- (void) setLoadingState:(NSNumber *) newState {
	loadingState = [newState intValue];
	if(loadingState == LR_INITIALIZING) {
		[loadingLabel setText:@"Initializing..."];
		return;
	}
	if(loadingState == LR_DOWNLOADING) {
		[loadingLabel setText:@"Downloading..."];
		return;
	}
	if(loadingState == LR_UNCOMPRESSING) {
		[loadingLabel setText:@"Downloading..."];
		return;
	}
	if(loadingState == LR_LOADING) {
		[loadingLabel setText:@"Loading..."];
		return;
	}
	if(loadingState == LR_DISPLAYING) {
		[loadingLabel setText:@"Displaying..."];
		return;
	}
	if(loadingState == LR_ERROR) {
		[loadingLabel setText:@"Error !"];
		[activityView setHidden:YES];
		return;
	}
	if(loadingState == LR_DONE) {
		[loadingLabel setHidden:YES];
		[activityView setHidden:YES];
		return;
	}
}

@end
