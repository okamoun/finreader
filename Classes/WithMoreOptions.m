#import "WithMoreOptions.h"

@implementation WithMoreOptions

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
		[self addPlusButton];
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void) removePlusButton {
	[[self navigationItem] setRightBarButtonItem:nil];
}

- (void) addPlusButton {
	UIBarButtonItem *plus = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd 
																		  target:self 
																		  action:@selector(moreOptions:)];
	[plus setStyle:UIBarButtonItemStyleBordered];
	[[self navigationItem] setRightBarButtonItem:plus];
	[plus release];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void) displayNote:(ObjID *)nid {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		MoreOptionsController_ipad *moreOptionsController = [[MoreOptionsController_ipad alloc] initWithParent:self];
		[moreOptionsController setModalPresentationStyle:UIModalPresentationFormSheet];
		[moreOptionsController setNoteToBeOpened:nid];
		[self presentViewController:moreOptionsController animated:NO completion:nil];
		[moreOptionsController release];
	} else {
		MoreOptionsController *moreOptionsController = [[MoreOptionsController alloc] initWithParent:self];
		[moreOptionsController setNoteToBeOpened:nid];
		[self presentViewController:moreOptionsController animated:NO completion:nil];
		[moreOptionsController release];
	}
}

- (void) moreOptions:(id)sender {
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		MoreOptionsController_ipad *moreOptionsController = [[MoreOptionsController_ipad alloc] initWithParent:self];
		[moreOptionsController setModalPresentationStyle:UIModalPresentationFormSheet];
		[self presentViewController:moreOptionsController animated:YES completion:nil];
		[moreOptionsController release];		
	} else {
		MoreOptionsController *moreOptionsController = [[MoreOptionsController alloc] initWithParent:self];
		[self presentViewController:moreOptionsController animated:YES completion:nil];
		[moreOptionsController release];
	}
}

- (NSString *) getStrUrl {
	return [[[self getXbrlObject] getId] description];
}

- (NSObject<XBRLObject> *) getXbrlObject {
	ERROR(NOTIFY_DEBUG, @"You must implement getXbrlObject method in derivate class");
	return nil;
}

@end
