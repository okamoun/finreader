#import "SecondLevelCell.h"

@implementation SecondLevelCell

@synthesize star;
@synthesize star2;
@synthesize star3;
@synthesize pointLabel;
@synthesize multivalue;

- (void) dealloc {	
	[star release];
	[star2 release];
	[star3 release];
	[pointLabel release];
	[multivalue release];
	[super dealloc];
}

- (void) setSection:(section *)s {
	if(![[s parentSection] hasOnlyOneValuePerRow] && ![s isHTML]) {
		[star setHidden:![s hasFootNoteForDate:0]];
		[star2 setHidden:![s hasFootNoteForDate:1]];
		[star3 setHidden:![s hasFootNoteForDate:2]];
	}
	[super setSection:s];
}

@end
