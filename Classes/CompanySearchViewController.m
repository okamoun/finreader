#import "CompanySearchViewController.h"
#import "CompanyViewController.h"

@implementation CompanySearchViewController

@synthesize comp;
@synthesize search;

- (id) initWithSCompany:(company *)c {
	if(self=[super initWithNibName:@"CompanySearchViewController" bundle:nil]) {
		[self setTitle:@"Search in company"];
		[self setComp:c];
		search = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

- (void)dealloc {
	[comp release], comp = nil;
	[search release], search = nil;
	[super dealloc];
}

- (NSString *) getStrUrl {
	return @"CompanySearch";
}

- (void)viewDidLoad {
	UISearchBar *searchBar = [[UISearchBar alloc] init];
	[searchBar setDelegate:self];
	[searchBar becomeFirstResponder];
	[searchBar sizeToFit];
	[searchBar setTintColor:[PropertiesGetter getColorForKey:@"search_bar_color"]];
	[searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
	[[self tableView] setTableHeaderView:searchBar];
	[searchBar release];
	//------------------------------------------------------------------------------------------------------------
	UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																			target:self 
																			action:@selector(cancelSearch:)];
	[[self navigationItem] setLeftBarButtonItem:cancel];
	[cancel release];
	//------------------------------------------------------------------------------------------------------------	
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[[self view] setAutoresizesSubviews:YES];
	[[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	//------------------------------------------------------------------------------------------------------------	
	[super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [search count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"c";
	
	report *r = [search objectAtIndex:indexPath.row];
	ReportCell *cell = (ReportCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		UIViewController *c = [[UIViewController alloc] initWithNibName:@"ReportCell" bundle:nil];
		cell = (ReportCell *)c.view;
		[c release];
	}	
		
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
	
	[cell setReport:r];
	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	report *currentReport = [search objectAtIndex:indexPath.row];
	if(![currentReport isAvailable]) 
	{
		UIAlertView *unaivalableAlert = [[UIAlertView alloc] initWithTitle:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_title"]  
																   message:(NSString *)[PropertiesGetter getStringForKey:@"unavailable_message"]
																  delegate:self 
														 cancelButtonTitle:@"Cancel"
														 otherButtonTitles:@"Buy", nil];
		[unaivalableAlert show];
		[unaivalableAlert release];
		return nil;
	}
	return indexPath;
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 1)
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=376803429&mt=8"]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	report *r = [search objectAtIndex:indexPath.row];	
	NSInteger prev = [[[self navigationController] viewControllers] count] - 2;
	CompanyViewController *cc = (CompanyViewController *)[[[self navigationController] viewControllers] objectAtIndex:prev];
	[cc setReportToGo:r];
	[cc setFirstToGo:YES];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:[[self navigationController] view] cache:YES];
	[[self navigationController] popViewControllerAnimated:NO];
	[UIView commitAnimations];
}


/* ---------- search --------------*/

- (void) searchTableView:(NSString *)searchText; {
	for (int i=0; i < [[comp reportGroups] count]; i++) {
		reportGroup *rg = [[comp reportGroups] objectAtIndex:i];
		
		NSRange resultsRange = [[rg type_] rangeOfString:searchText options:NSCaseInsensitiveSearch];
		if(resultsRange.length > 0) {
			[search addObjectsFromArray:[rg reports]];
		} else {
			for(int j = 0 ; j < [[rg reports] count];j++) {
				report *r = [[rg reports] objectAtIndex:j];
				NSRange resultsRange2 = [[r getDate] rangeOfString:searchText options:NSCaseInsensitiveSearch];
				if(resultsRange2.length > 0) {
					[search addObject:r];
				}
			}
		}
	}
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
	[search removeAllObjects];
	if([searchText length] > 2) {	
		[self searchTableView:[theSearchBar text]];
	}
	[[self tableView] reloadData];
}

- (void) cancelSearch:(id)sender {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:[[self navigationController] view] cache:YES];
	[[self navigationController] popViewControllerAnimated:NO];
	[UIView commitAnimations];
}

@end

