#import "XMLToCompaniesParser.h"
#import "CompaniesViewController.h"

@implementation XMLToCompaniesParser

@synthesize companies;
@synthesize parseError;

- (id) initWithContentsOfURL:(NSURL *)url {
	if(self = [super init]) {
		companies = [[NSMutableArray alloc] initWithCapacity:[[FileManager getAlpha] count]];
		NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
		currentCompany = nil;
		currentReport = nil;
		//currentNodeName = nil;
		currentNodeContent = nil;
		currentCompanyName = nil;
		currentType = nil;
		groupCounter = 0;
		[parser setDelegate:self];
		[parser parse];
		[self setParseError:[parser parserError]];
		[parser release];
	}
	return self;
}

- (id) initWithData:(NSData *)data {
	if(self = [super init]) {
		companies = [[NSMutableArray alloc] initWithCapacity:[[FileManager getAlpha] count]];
		NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
		currentCompany = nil;
		currentReport = nil;
		//currentNodeName = nil;
		currentNodeContent = nil;
		currentCompanyName = nil;
		currentType = nil;
		groupCounter = 0;
		[parser setDelegate:self];
		[parser parse];
		[self setParseError:[parser parserError]];
		[parser release];
	}
	return self;
}

- (void)dealloc
{
	[companies release];
	[parseError release];
	[super dealloc];
}

- (void) parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
   namespaceURI:(NSString *)namespaceURI
  qualifiedName:(NSString *)qName
	 attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"report"]) {
		currentReport = [[report alloc] init];
	}
	else if (currentReport != nil) {
		//currentNodeName = [elementName copy];
	    currentNodeContent = [[NSMutableString alloc] initWithCapacity:0];
	}
}

- (void)parser:(NSXMLParser *)parser
foundCharacters:(NSString *)string
{  
	if(currentNodeContent != nil) [currentNodeContent appendString:string];
}

- (void)parser:(NSXMLParser *)parser
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI
 qualifiedName:(NSString *)qName
{   
	if([elementName isEqualToString:@"report"]) {

		if(currentCompany == nil) // first report
		{
			currentCompany = [[company alloc] init]	;
			currentCompany.name = currentCompanyName;
			currentCompanyGroup = [[NSMutableArray alloc] initWithCapacity:0];
			///Warning : we assume there is at least a company which began with a number (il faudrait ajouter une boucle pour savoir combien de tableau vide inserer avant)
			[currentCompanyGroup addObject:currentCompany];
			[companies addObject:currentCompanyGroup];
		} // first report 
		
		else if(![currentCompany.name isEqualToString:currentCompanyName]) { // new company
			[currentCompany release], currentCompany = nil;
			currentCompany = [[company alloc] init];
			[currentCompany setName:currentCompanyName];
			
			NSString *firstChar = [[currentCompany.name substringToIndex:1] uppercaseString];
			NSString *currentFirstChar = [[FileManager getAlpha] objectAtIndex:groupCounter];

			///Warning : we assume there is at least a company which began with an A
			if(! ([firstChar isEqualToString:currentFirstChar] || (groupCounter == 0 && ![firstChar isEqualToString:@"A"]))) //new group
			{	
				[currentCompanyGroup release];
				currentCompanyGroup = [[NSMutableArray alloc] initWithCapacity:0];
				[companies addObject:currentCompanyGroup];
				groupCounter++;
				if(groupCounter >= [[FileManager getAlpha] count]) {
					ERROR(NOTIFY, @"An error is occured while getting last reports");
					[parser abortParsing];
					return;
				}
				
			} //new group
			
			[currentCompanyGroup addObject:currentCompany];
		} //new company
		[currentCompany addReport:currentReport ofType:currentType];
		[currentReport release], currentReport = nil;
		
	} else if ([elementName isEqualToString:@"reports"]) {
		[currentCompany release];
		[currentCompanyGroup release];
		[currentCompanyName release];
		[currentType release];
	} else if([elementName isEqualToString:@"company"]) {
		[currentCompanyName release];
		currentCompanyName = [currentNodeContent copy];
		[currentNodeContent release], currentNodeContent = nil;
	}
	else if([elementName isEqualToString:@"type"]) {
		[currentType release];
		currentType = [currentNodeContent copy];
		[currentNodeContent release], currentNodeContent = nil;
	}
	else /*if([elementName isEqualToString:currentNodeName])*/ {  
		if([elementName isEqualToString:@"date"])
		{
			[currentReport setDateFromXML:currentNodeContent];
		} else if([elementName isEqualToString:@"available"]) {
			[currentReport setAvailableFromXML:currentNodeContent];			
		} else {
			[currentReport setValue:currentNodeContent forKey:elementName];
		}
		[currentNodeContent release], currentNodeContent = nil;
		//[currentNodeName release], currentNodeName = nil;
	} 
}


@end
