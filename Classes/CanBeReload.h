#import <UIKit/UIKit.h>

@protocol CanBeReload

- (void) completeReload;

@end
