#import "ReportViewController.h"

@implementation ReportViewController

@synthesize sections;
@synthesize HTMLView;
@synthesize separator;
@synthesize tv;
@synthesize activateIndexPath;
@synthesize companyName;
@synthesize reportTitle;
@synthesize sectionToGo;
@synthesize firstToGo;
@synthesize header;
@synthesize noteToBeOpened;
@synthesize parent;
@synthesize gotorow;

- (id) initWithSection:(section *)parentSection
			   gotoRow:(NSNumber *)r {
	if(self=[super initWithNibName:@"ReportViewController" bundle:nil]) {
		[self setParent:parentSection];
		[self setGotorow:r];
		[self setReportTitle:[parent title]];
		[self setTitle:[[parent myReport] getCompanyName]];
		[self setHeader:nil];
		[self setSections:nil];
		HTMLHeight = [[self view] frame].size.height*[PropertiesGetter getFloatForKey:@"HTMLViewHeight"];
		SeparatorHeight = [PropertiesGetter getFloatForKey:@"SeparatorHeight"];
		firstToGo = NO;
	
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	UIButton *titleLabel = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
	[titleLabel setTitle:self.title forState:UIControlStateNormal];
	[titleLabel setFrame:CGRectMake(0, 0, 320, 44)];
	[[titleLabel titleLabel] setFont:[UIFont boldSystemFontOfSize:20]];
	[titleLabel addTarget:self action:@selector(showWay:) forControlEvents:UIControlEventTouchUpInside];
	[[self navigationItem] setTitleView:titleLabel];
	[titleLabel release];
	
	if(self.sections == nil) {
		NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:0];
		[self setSections:newArray];
		[newArray release];
	
		if([[[self parent] subSections] count] == 0) {//TODO : check that case exists
			NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@&id=%@", 
														[PropertiesGetter getBaseURL], 
														[[[self parent] myReport] link], 
														[[UIDevice currentDevice] identifierForVendor]]];
			XMLToSectionsParser *parser = [[XMLToSectionsParser alloc] initWithURL:url thread:nil];
			[url release];
			[parser parse];
			for(int i = 0; i < [[parser items] count]; i++) {
				section *s = [[parser items] objectAtIndex:i];
				[s getAllDates:YES];
				[self.sections addObject:s];
			}
			[parser release];
		}
		else {
			for(int i = 0; i < [[[self parent] subSections] count]; i++) {
				section *s = [[[self parent] subSections] objectAtIndex:i];
				[s getAllDates:YES];
				[self.sections addObject:s];
			}	
		}
	}

	if(gotorow != nil)
	{
		int ri = [gotorow intValue];
		for(int i = 0; i < [gotorow intValue] ; i++)
		{
			ri += [[[sections objectAtIndex:i] subSections] count];
		}
		activateIndexPath = [NSIndexPath indexPathForRow:ri inSection:0];
	}
	displayTabBar = YES;		
	UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:@"Back"
															 style:UIBarButtonItemStyleBordered
															target:nil
															action:nil];
	
	[[self navigationItem] setBackBarButtonItem:back];
	[back release];	
}

- (void) viewDidUnload {
	[sections release], sections = nil;
	[super viewDidUnload];
}

- (void)dealloc {
	[parent release], parent = nil;
	[gotorow release], gotorow = nil;
	[companyName release], companyName = nil;
	[reportTitle release], reportTitle = nil;
	[sectionToGo release], sectionToGo = nil;
	[sections release], sections = nil;
	[HTMLView release], HTMLView = nil;
	[separator release], separator = nil;
	[tv release], tv = nil;
	[activateIndexPath release], activateIndexPath = nil;
	[header release], header = nil;
    [super dealloc];
}

- (void) selectSection:(id)s {
	ObjID *sid = [(section *)s getId];
	int I = 0;
	int i;
	for(i = 0; i < [sections count]; i++) {
		NSIndexPath *ip = [NSIndexPath indexPathForRow:I inSection:0];
		if([[[sections objectAtIndex:i] getId] isEqual:sid]) {
			[tv selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionMiddle];
		} else {
			[tv deselectRowAtIndexPath:ip animated:NO];
		}
		I++;
		int j;
		for(j = 0; j < [[[sections objectAtIndex:i] subSections] count]; j++) {
			NSIndexPath *ip = [NSIndexPath indexPathForRow:I inSection:0];
			if([[[[[sections objectAtIndex:i] subSections] objectAtIndex:j] getId] isEqual:sid]) {
				[tv selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionMiddle];
			} else {
				[tv deselectRowAtIndexPath:ip animated:NO];
			}
			I++;
		}
	}
}

- (void) showWay:(id) sender {
	UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil
													delegate:self 
										   cancelButtonTitle:reportTitle
									  destructiveButtonTitle:nil 
										   otherButtonTitles:nil];
	section *s = [[[sections objectAtIndex:0] parentSection] parentSection];
	while (s != nil) {
		[as addButtonWithTitle:[s title]];
		s = [s parentSection];
	}
	NSString *reportButton = [NSString stringWithFormat:@"%@ - %@", [[[sections objectAtIndex:0] myReport] getReportType], [[[sections objectAtIndex:0] myReport] getCompanyName]];
	[as addButtonWithTitle:reportButton];
	[as showInView:[self view]];
	[as release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSInteger nb =[[[self navigationController] viewControllers] count] - buttonIndex - 1;
	[[self navigationController] popToViewController:[[[self navigationController] viewControllers] objectAtIndex:nb] animated:YES];
}

- (void) push:(NSTimer *)timer {
	[[self navigationController] pushViewController:[[timer userInfo] objectAtIndex:0] animated:YES];
	[[self view] setUserInteractionEnabled:YES];
}

- (void) performSearch:(NSTimer *)timer {
	BOOL done = NO;
	int I = 0;
	for (int i = 0 ; i < [sections count] ; i++) {
		section *s = [sections objectAtIndex:i];
		if(sectionToGo == s) {
			NSIndexPath *ip = [NSIndexPath indexPathForRow:I inSection:0];
			[tv selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionMiddle];
			if([sectionToGo getNbLevel] > 2) {
				ReportViewController *anotherViewController = [[ReportViewController alloc] initWithSection:s gotoRow:nil];
				if(noteToBeOpened != nil){
					[anotherViewController setNoteToBeOpened:noteToBeOpened];
					[noteToBeOpened release], noteToBeOpened = nil;
				}
				[NSTimer scheduledTimerWithTimeInterval:0.4 
												 target:self 
											   selector:@selector(push:) 
											   userInfo:[NSArray arrayWithObject:anotherViewController]
												repeats:NO];
				[anotherViewController release];
			} else {
				if(noteToBeOpened != nil) {
					[super displayNote:noteToBeOpened];
					[noteToBeOpened release], noteToBeOpened = nil;
				}				
				[self.view setUserInteractionEnabled:YES];
			}
			done = YES;
			break;
		}
		I++;
		for( int j = 0 ; j < [[s subSections] count] ; j++) {
			if(sectionToGo == [[s subSections] objectAtIndex:j]) {
				NSIndexPath *ip = [NSIndexPath indexPathForRow:I inSection:0];
				[tv selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionMiddle];
				[[self view] setUserInteractionEnabled:YES];
				done = YES;
				if(noteToBeOpened != nil) {
					[super displayNote:noteToBeOpened];
					[noteToBeOpened release], noteToBeOpened = nil;
				}
				break;
			}
			I++;
		}
		
	}
	section *csectionToGo = [sectionToGo parentSection];
	while (!done) {
		csectionToGo = [csectionToGo parentSection];
		if(csectionToGo == nil) {
			break;
		}
		int I = 0;
		for(int i = 0 ; i < [sections count] ; i++) {
			section *s = [sections objectAtIndex:i];
			if(csectionToGo == s){
				NSIndexPath *ip = [NSIndexPath indexPathForRow:I inSection:0];
				[tv selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionMiddle];
				ReportViewController *anotherViewController = [[ReportViewController alloc] initWithSection:s gotoRow:nil];
				[anotherViewController setSectionToGo:sectionToGo];
				if(noteToBeOpened != nil){
					[anotherViewController setNoteToBeOpened:noteToBeOpened];
					[noteToBeOpened release], noteToBeOpened = nil;
				}
				[NSTimer scheduledTimerWithTimeInterval:0.4 
												 target:self 
											   selector:@selector(push:) 
											   userInfo:[NSArray arrayWithObject:anotherViewController]
												repeats:NO];
				[anotherViewController release];
				done = YES;
				break;
			}
			I += ([[[sections objectAtIndex:i] subSections] count] + 1);
		}
	}
	sectionToGo = nil;
}


- (void)didRotate:(NSNotification  *)notification {
	if(!displayTabBar) {
		
		UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
		if(orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
			if(!wasLandscape) {
				HTMLHeight = (int)((float)HTMLHeight*(float)([[[self view] window] frame].size.width)/(float)([[[self view] window] frame].size.height));
			}
			wasLandscape = YES;
		}
		else if(orientation == UIDeviceOrientationPortrait || orientation == UIDeviceOrientationPortraitUpsideDown) {		
			if(wasLandscape) {				
				HTMLHeight = (int)((float)HTMLHeight*((float)[[[self view] window] frame].size.height)/(float)([[[self view] window] frame].size.width));
			}
			wasLandscape = NO;
		}
		[[HTMLView view] setFrame:CGRectMake( 0.0f, 0.0f, [[self view] bounds].size.width, HTMLHeight+SeparatorHeight)];
		[[separator view] setFrame:CGRectMake( 0.0f, HTMLHeight, [[self view] bounds].size.width, SeparatorHeight)];
		[tv setFrame:CGRectMake(0.0f, HTMLHeight + SeparatorHeight, [[self view] bounds].size.width, [tv frame].size.height - (-[tv frame].origin.y + HTMLHeight + SeparatorHeight))];		
	}
	else {
		HTMLHeight = [[[self view] window] frame].size.height*[PropertiesGetter getFloatForKey:@"HTMLViewHeight"];
		[[separator view] setFrame:CGRectMake( 0.0f, -1*SeparatorHeight, [[self view] bounds].size.width, SeparatorHeight)];
	}
}

- (void)viewWillAppear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector(didRotate:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:nil];
	[super viewWillAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super viewWillDisappear:animated];
}

- (void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[self manageTabBar];
	if(activateIndexPath != nil)
	{
		if(tv != nil)
			[tv selectRowAtIndexPath:activateIndexPath animated:YES scrollPosition:UITableViewScrollPositionMiddle];
		[activateIndexPath release], activateIndexPath = nil;
	}	
	if(sectionToGo != nil)
	{
		if(sectionToGo == [[sections objectAtIndex:0] parentSection]) {
			if(noteToBeOpened != nil) {
				[super displayNote:noteToBeOpened];
				[noteToBeOpened release], noteToBeOpened = nil;
			}
		} else {
			[[self view] setUserInteractionEnabled:NO];
			NSTimeInterval ti;
			if(firstToGo) { ti = 0.8; firstToGo = NO ; }
			else ti = 0.4;
			[NSTimer scheduledTimerWithTimeInterval:ti 
											 target:self 
										   selector:@selector(performSearch:) 
										   userInfo:nil
											repeats:NO];			
		}
	} else if(noteToBeOpened != nil) {
		[super displayNote:noteToBeOpened];
		[noteToBeOpened release], noteToBeOpened = nil;
	}

	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	wasLandscape = (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight);
}

- (void) manageTabBar {
	
	if(displayTabBar) {
		for(UIView *view in self.tabBarController.view.subviews)
		{
			if([view isKindOfClass:[UITabBar class]])
			{
				[[view layer] setValue:[NSNumber numberWithInt:0] forKeyPath:@"transform.translation.y"];
			} else if ([view frame].size.height == [[view window] frame].size.height) {
					[view setFrame:CGRectMake([view frame].origin.x, [view frame].origin.y, [view frame].size.width, [[view window] frame].size.height-49)];
			} else if([view frame].size.height == [[view window] frame].size.width) {
				[view setFrame:CGRectMake([view frame].origin.x, [view frame].origin.y, [view frame].size.width, [[view window] frame].size.width-49)];
			}
		}
    } else {
		for(UIView *view in [[[self tabBarController] view] subviews])
		{
			if([view isKindOfClass:[UITabBar class]])
			{
				[[view layer] setValue:[NSNumber numberWithInt:[view frame].size.height] forKeyPath:@"transform.translation.y"];
			} else if([view frame].size.height == [[view window] frame].size.height-49) {
					[view setFrame:CGRectMake([view frame].origin.x, [view frame].origin.y, [view frame].size.width, [[view window] frame].size.height)];
			} else if([view frame].size.height == [[view window] frame].size.width-49) {
				[view setFrame:CGRectMake([view frame].origin.x, [view frame].origin.y, [view frame].size.width, [[view window] frame].size.width)];
			}
		}
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	if(header == nil)
	{
		header = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 35.0)];
		[header setAutoresizesSubviews:YES];
		[header setBackgroundColor:[PropertiesGetter getColorForKey:@"header_flow_color"]];
		UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 310.0, 35.0)];
		[headerLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
		[headerLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
		[headerLabel setMinimumScaleFactor:4.0/[UIFont labelFontSize]];//TODO : check size (was setMinimumFontSize:10)
		[headerLabel setAdjustsFontSizeToFitWidth:YES];
		[headerLabel setNumberOfLines:2];
		[headerLabel setTextAlignment:NSTextAlignmentCenter];
		[headerLabel setText:[self reportTitle]];
		[header addSubview:headerLabel];
		[headerLabel release];
	}
	return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int total = 0;
	for(int i=0; i < [sections count]; i++)
		total += 1 + [[[sections objectAtIndex:i] subSections] count];
	return total;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"c";
	static NSString *CellIdentifier2 = @"c2";
	
	section *s;
	BOOL isSubSection;
	int i = 0, j = 0 ;
	while (true) {
		if(i==indexPath.row) {
			s = [sections objectAtIndex:j];
			isSubSection = NO;
			break;
		}
		i++;
		if((i+[[[sections objectAtIndex:j] subSections] count]) > indexPath.row)
		{
			isSubSection = YES;
			s = [[[sections objectAtIndex:j] subSections] objectAtIndex:(indexPath.row - i)];		
			break;
		}
		i += [[[sections objectAtIndex:j] subSections] count];
		j++;
	}
	
	/*---*/
	
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
	if(isSubSection){
		SecondLevelCell *cell = (SecondLevelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		if (cell == nil) {
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"SecondLevelCell" bundle:nil];
			cell = (SecondLevelCell *)c.view;
			[c release];
		}	

		if(indexPath.row %2 == 1) {
			[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
		} else {
			[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
		}
		[cell setBackgroundView:backgroundView];
		[backgroundView release];
		[cell setFontColor:[PropertiesGetter getColorForKey:@"second_level_font_color"]];
		[cell setSection:s];
		return cell;
	} else {
		FirstLevelCell *cell = (FirstLevelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
		if (cell == nil) {
			UIViewController *c = [[UIViewController alloc] initWithNibName:@"FirstLevelCell"  bundle:nil];
			cell = (FirstLevelCell *)c.view;
			[c release];
		}
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"first_level_color"]];
		[cell setBackgroundView:backgroundView];
		[backgroundView release];
		[cell setFontColor:[PropertiesGetter getColorForKey:@"first_level_font_color"]];
		[cell setSection:s];
		return cell;
	}	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	section *s;
	int i = 0, j = 0 ;
	while (true) {
		if(i==indexPath.row) {
			s  = [sections objectAtIndex:j];
			if([s getNbLevel] < 3) {
				[self HTMLView:s];
				return;
			}
			break;
		}
		i++;
		if((i+[[[sections objectAtIndex:j] subSections] count]) > indexPath.row)
		{
			s = [[[sections objectAtIndex:j] subSections] objectAtIndex:(indexPath.row - i)];
			[self HTMLView:s];
			return;
		}
		i += [[[sections objectAtIndex:j] subSections] count];
		j++;
	}
	if(HTMLView != nil) {
		NSThread *close = [[NSThread alloc] initWithTarget:self selector:@selector(closeHTML:) object:nil];
		[close start];
		[close release];
		[NSThread sleepForTimeInterval:0.4];
	}
	
	ReportViewController *anotherViewController = [[ReportViewController alloc] initWithSection:s gotoRow:nil];
	[[self navigationController] pushViewController:anotherViewController animated:YES];
	[anotherViewController release];

}

- (void) HTMLView:(section *)s{
	HTMLViewController *tempHTML = HTMLView;
	UIViewController *tempSep = separator;
	
	HTMLView = [[HTMLViewController alloc] initWithSection:s];
	[[HTMLView view] setFrame:CGRectMake(0.0f, -1 * (HTMLHeight + SeparatorHeight), [[self view] bounds].size.width, HTMLHeight+ SeparatorHeight)];
	[[HTMLView view] setBackgroundColor:[UIColor whiteColor]];
	
	separator = [[UIViewController alloc] initWithNibName:@"GripView" bundle:nil];
	[[separator view] setFrame:CGRectMake(0.0f, -1 * SeparatorHeight, [[self view] bounds].size.width, SeparatorHeight)];
	[[separator view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
	[tv setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
	
	[[self view] addSubview:(UIView *)[HTMLView view]];
	[[self view] addSubview:[separator view]];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	displayTabBar = NO;
	[self manageTabBar];
	[[HTMLView view] setFrame:CGRectMake( 0.0f, 0.0f, [[self view] bounds].size.width, HTMLHeight+SeparatorHeight)];
	[[separator view] setFrame:CGRectMake( 0.0f, HTMLHeight, [[self view] bounds].size.width, SeparatorHeight)];
	[tv setFrame:CGRectMake(0.0f, HTMLHeight + SeparatorHeight, [[self view] bounds].size.width, [tv frame].size.height - (-[tv frame].origin.y + HTMLHeight + SeparatorHeight))];
	[UIView commitAnimations];
	UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop 
																				target:self 
																				action:@selector(closeHTML:)];
	[[self navigationItem] setLeftBarButtonItem:closeButton];
	[closeButton release];
		
	UIBarButtonItem *maximize = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"expand.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(maximize:)];
	[[self navigationItem] setRightBarButtonItem:maximize];
	[maximize release];
	
	[NSTimer scheduledTimerWithTimeInterval:0.4
									 target:self 
								   selector:@selector(removeOldSubViews:) 
								   userInfo:[NSArray arrayWithObjects:tempHTML, tempSep.view, nil] 
									repeats:NO];
	[tempHTML release];
	[tempSep release];
}

- (void) removeOldSubViews:(NSTimer*)timer {
	NSLog(@"REPORT VIEW CONTROLLER REMOVE OLD SUBVIEW");
	for(int i= 0; i < [[timer userInfo] count]; i++) {
		[[[timer userInfo] objectAtIndex:i] removeFromSuperview];
	}
}

- (void) closeHTML:(id)sender {
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[[HTMLView view] setFrame:CGRectMake(0.0f, -1 * ([[HTMLView view] frame].size.height + SeparatorHeight), [[self view] bounds].size.width, [[HTMLView view] frame].size.height)];
	[[separator view] setFrame:CGRectMake(0.0f, -1 * SeparatorHeight, [[self view] bounds].size.width, SeparatorHeight)];
	displayTabBar = YES;
	[self manageTabBar];
	[tv setFrame:CGRectMake(0.0f, 0, [[self view] bounds].size.width, [[self view] bounds].size.height)];
	[UIView commitAnimations];
	[[self navigationItem] setRightBarButtonItem:nil];
	[[self navigationItem] setLeftBarButtonItem:nil];
	[self addPlusButton];
	[pool release];
}

- (void) maximize:(id)sender {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	[[separator view] setFrame:CGRectMake(0.0f, [[self view] bounds].size.height, [[self view] bounds].size.width, SeparatorHeight)];
	[[HTMLView view] setFrame:CGRectMake(0.0f, 0.0f, [[self view] bounds].size.width, [[self view] bounds].size.height)];
	[[HTMLView view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
	[[separator view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin];
	displayTabBar = YES;
	[self manageTabBar];
	[tv setFrame:CGRectMake(0.0f, [[self view] bounds].size.height+SeparatorHeight, [[self view] bounds].size.width, [tv frame].size.height)];
	[UIView commitAnimations];
		
	UIBarButtonItem *minimize = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"unexpand.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(minimize:)];
	[[self navigationItem] setRightBarButtonItem:minimize];
	[minimize release];
}

- (void) minimize:(id)sender {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.4];
	displayTabBar = NO;
	[self manageTabBar];
	[[HTMLView view] setFrame:CGRectMake( 0.0f, 0.0f, [[self view] bounds].size.width, HTMLHeight+SeparatorHeight)];
	[[HTMLView view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin];
	[[separator view] setFrame:CGRectMake( 0.0f, HTMLHeight, [[self view] bounds].size.width, SeparatorHeight)];
	[[separator view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleBottomMargin];
	[tv setFrame:CGRectMake(0.0f, HTMLHeight + SeparatorHeight, [[self view] bounds].size.width, [tv frame].size.height)];
	
	[UIView commitAnimations];
	
	UIBarButtonItem *maximize = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"expand.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(maximize:)];
	[[self navigationItem] setRightBarButtonItem:maximize];
	[maximize release];
}

- (void)touchesMoved: (NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *t = [touches anyObject];
	CGPoint where = [t locationInView:[self view]];
	CGPoint prevWhere =[t previousLocationInView:[self view]];
	double pos = where.y;
	double lastPos = prevWhere.y;
	HTMLHeight += (pos - lastPos);
	double max = 30;
	if(HTMLHeight < max) HTMLHeight = max;
	if(HTMLHeight > ([[self view] bounds].size.height - max)) HTMLHeight = ([[self view] bounds].size.height - max);
	[tv setFrame:CGRectMake(0.0f, (HTMLHeight + SeparatorHeight), [[self view] bounds].size.width, [tv frame].size.height - (-[tv frame].origin.y + HTMLHeight + SeparatorHeight))];
	[[separator view] setFrame:CGRectMake( 0.0f, HTMLHeight, [[self view] bounds].size.width, SeparatorHeight)];
	[[HTMLView view] setFrame:CGRectMake( 0.0f, 0.0f, [[self view] bounds].size.width, HTMLHeight + SeparatorHeight)];
}

- (void) search:(id)sender {
	SearchViewController *anotherViewController = [[SearchViewController alloc] initWithSections:sections];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[[self navigationController] view] cache:YES];
	[[self navigationController] pushViewController:anotherViewController animated:NO];
	[UIView commitAnimations];
	[anotherViewController release];
}

- (NSObject<XBRLObject> *) getXbrlObject {
	
	if([tv indexPathForSelectedRow]) {
		int i = 0, j = 0 ;
		while (true) {
			if(i==[[tv indexPathForSelectedRow] row]) {
				return (NSObject<XBRLObject> *)[sections objectAtIndex:j];
			}
			i++;
			if((i+[[[sections objectAtIndex:j] subSections] count]) > [[tv indexPathForSelectedRow] row])
			{
				return (NSObject<XBRLObject> *)[[[sections objectAtIndex:j] subSections] objectAtIndex:([[tv indexPathForSelectedRow] row] - i)];
			}
			i += [[[sections objectAtIndex:j] subSections] count];
			j++;
		}
	} 
	return (NSObject<XBRLObject> *)[[sections objectAtIndex:0] parentSection];
}

@end

