#import "VirtualReportCell.h"

@implementation VirtualReportCell

@synthesize dateLabel;
@synthesize grey;
@synthesize orange;
@synthesize green;

- (void) dealloc {
	[dateLabel release];
	[grey release];
	[orange release];
	[green release];
	[super dealloc];
}

- (void) setReport:(report *)r {
	[dateLabel setText:[r getDate]];
	
	if([r isAggregate]) {
		[dateLabel setText:[NSString stringWithFormat:@"%@ - Aggregate", [r getDate]]];
	} else if([r isAmend]) {
		[dateLabel setText:[NSString stringWithFormat:@"%@ - Amend", [r getDate]]];
	} else {
		[dateLabel setText:[r getDate]];
	}
		
	[dateLabel setEnabled:[r isAvailable]];
	Downloaded d = [r getDownloaded];
	if(d == not_downloaded) {
		[grey setHidden:NO];
		[orange setHidden:YES];
		[green setHidden:YES];
	} else if(d == partially_downloaded) {
		[grey setHidden:YES];
		[orange setHidden:NO];
		[green setHidden:YES];	
	} else if(d == totally_downloaded) {
		[grey setHidden:YES];
		[orange setHidden:YES];
		[green setHidden:NO];		
	}
	if(fontColor != nil) {
		[dateLabel setTextColor:fontColor];
	}
	[super setObj:r];
}

@end
