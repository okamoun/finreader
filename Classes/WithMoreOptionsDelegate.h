#import <UIKit/UIKit.h>

@protocol WithMoreOptionsDelegate

- (NSObject<XBRLObject> *) getXbrlObject;

@end
