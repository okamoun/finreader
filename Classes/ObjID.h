#import <Foundation/Foundation.h>

@interface ObjID : NSObject<NSCopying> {
	NSString *compId;
	NSString *reportId;
	NSString *xbrlId;
	NSNumber *nbId;
}

@property (nonatomic, retain) NSString *compId;
@property (nonatomic, retain) NSString *reportId;
@property (nonatomic, retain) NSString *xbrlId;
@property (nonatomic, retain) NSNumber *nbId;

+ (id) ObjIdWithCompany:(NSString *)comp report:(NSString *)rep item:(NSString *)item andNb:(NSNumber *)nb;
+ (id) ObjIdWithCompany:(NSString *)comp;
+ (id) ObjIdWithObjId:(ObjID *)i andReport:(NSString *)rep;
+ (id) ObjIdWithObjId:(ObjID *)i andItem:(NSString *)item;
+ (id) ObjIdWithObjId:(ObjID *)i andNb:(NSNumber *)nb;

- (id) initWithCompany:(NSString *)comp report:(NSString *)rep item:(NSString *)item andNb:(NSNumber *)nb;
- (id) initWithCompany:(NSString *)comp;
- (id) initWithObjId:(ObjID *)i andReport:(NSString *)rep;
- (id) initWithObjId:(ObjID *)i andItem:(NSString *)item;
- (id) initWithObjId:(ObjID *)i andNb:(NSNumber *)nb;

@end
