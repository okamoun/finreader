#import <Foundation/Foundation.h>
#import "XBRLObject.h"
#import "reportGroup.h"

@class report;

@interface company : NSObject<XBRLObject> {
	NSString *name;
	NSMutableArray *reportGroups;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSMutableArray *reportGroups;

//xml parsing methods
- (void) addReport:(report *)r ofType:(NSString *)reportType;
//
- (void) loadCompany;

- (report *) getReport:(ObjID *)rid;
- (int) getNbReports;

@end
