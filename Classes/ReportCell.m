#import "ReportCell.h"

@implementation ReportCell

@synthesize typeLabel;
@synthesize companyLabel;

- (void) dealloc {	
	[typeLabel release];
	[companyLabel release];
	[super dealloc];
}

- (void) setReport:(report *)r {
	[typeLabel setEnabled:[r isAvailable]];
	[companyLabel setEnabled:[r isAvailable]];
	[companyLabel setText:[r getCompanyName]];
	[typeLabel setText:[r getReportType]];
	if(fontColor != nil) {
		[companyLabel setTextColor:fontColor];
		[typeLabel setTextColor:fontColor];
	}
	[super setReport:r];
}

@end
