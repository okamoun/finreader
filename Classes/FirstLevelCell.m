#import "FirstLevelCell.h"

@implementation FirstLevelCell

- (void) dealloc {
	[super dealloc];
}

- (void) setSection:(section *)s {
	if([s getNbLevel] > 2) {
		[self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
		[valueLabel setFrame:CGRectMake(valueLabel.frame.origin.x, valueLabel.frame.origin.y, valueLabel.frame.size.width-16, valueLabel.frame.size.height)];
		[valueLabel2 setFrame:CGRectMake(valueLabel2.frame.origin.x, valueLabel2.frame.origin.y, valueLabel2.frame.size.width-16, valueLabel2.frame.size.height)];
		[valueLabel3 setFrame:CGRectMake(valueLabel3.frame.origin.x, valueLabel3.frame.origin.y, valueLabel3.frame.size.width-16, valueLabel3.frame.size.height)];
	}
	[super setSection:s];
}

@end
