#import "SearchViewController.h"

@implementation SearchViewController

@synthesize sections;
@synthesize search;

- (id) initWithSections:(NSArray *)subSections {
	if(self=[super initWithNibName:@"SearchViewController" bundle:nil]) {
		[self setTitle:@"Search in report"];
		[self setSections:subSections];
		search = [[NSMutableArray alloc] initWithCapacity:0];
	}
	return self;
}

- (void)dealloc {
	[sections release], sections = nil;
	[search release], search = nil;
	[super dealloc];
}

///TODO : a quoi ca sert ?
- (NSString *) getStrUrl {
	return [NSString stringWithFormat:@"%@/Search", [[[[sections objectAtIndex:0] parentSection] getId] description]];
}

- (void)viewDidLoad {
	UISearchBar *searchBar = [[UISearchBar alloc] init];
	[searchBar setDelegate:self];
	[searchBar becomeFirstResponder];  
	[searchBar sizeToFit];
	[searchBar setTintColor:[PropertiesGetter getColorForKey:@"search_bar_color"]];
	[searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
	[[self tableView] setTableHeaderView:searchBar];
	[searchBar release];
	//------------------------------------------------------------------------------------------------------------
	UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel 
																			target:self 
																			action:@selector(cancelSearch:)];
	[[self navigationItem] setLeftBarButtonItem:cancel];
	[cancel release];
	//------------------------------------------------------------------------------------------------------------	
	[[[self navigationController] navigationBar] setTintColor:[PropertiesGetter getColorForKey:@"bar_color"]];
	[[self view] setAutoresizesSubviews:YES];
	[[self view] setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight ];
	//------------------------------------------------------------------------------------------------------------	
	[super viewDidLoad];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [search count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"c";
	
	section *s = [search objectAtIndex:indexPath.row];
	SecondLevelCell *cell = (SecondLevelCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		UIViewController *c = [[UIViewController alloc] initWithNibName:@"SecondLevelCell" bundle:nil];
		cell = (SecondLevelCell *)c.view;
		[c release];
	}	
		
	UIView* backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
	if(indexPath.row %2 == 1) {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_even_color"]];
	} else {
		[backgroundView setBackgroundColor:[PropertiesGetter getColorForKey:@"second_level_color"]];
	}
	[cell setBackgroundView:backgroundView];
	[backgroundView release];
	
	[cell setSection:s];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	section *s = [search objectAtIndex:indexPath.row];	
	NSInteger prev = [[[self navigationController] viewControllers] count] - 2;
	[[[[self navigationController] viewControllers] objectAtIndex:prev] setSectionToGo:s];
	[[[[self navigationController] viewControllers] objectAtIndex:prev] setFirstToGo:YES];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.navigationController.view cache:YES];
	[[self navigationController] popViewControllerAnimated:NO];
	[UIView commitAnimations];
}

/* ---------- search --------------*/

- (void) searchTableView:(NSString *)searchText; {
	for (int i=0; i < [sections count]; i++) {
		[search addObjectsFromArray:[[sections objectAtIndex:i] matchingSections:searchText]];
	}
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
	[search removeAllObjects];
	if([searchText length] > 2) {		
		[self searchTableView:searchText];
	}
	[[self tableView] reloadData];
}

- (void) cancelSearch:(id)sender {
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.75];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:[[self navigationController] view] cache:YES];
	[[self navigationController] popViewControllerAnimated:NO];
	[UIView commitAnimations];
}

@end

