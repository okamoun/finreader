#import "InfosView.h"

@implementation InfosView

@synthesize titleLabel;
@synthesize versionLabel;
@synthesize contentView;

- (void)dealloc {
	[titleLabel release];
	[versionLabel release];
	[contentView release];
    [super dealloc];
}

- (void)drawRect:(CGRect)rect {
	[titleLabel setText:[PropertiesGetter getStringForKey:@"title"]];
	[versionLabel setText:[NSString stringWithFormat:@"%@ %@", [PropertiesGetter getStringForKey:@"version"], [PropertiesGetter getReleaseDate]]];
	[contentView loadData:[NSData dataWithContentsOfFile:[FileManager getInfosFile]] 
				 MIMEType:@"application/xhtml+xml" 
		 textEncodingName:@"UTF-8" 
				  baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
}

@end
